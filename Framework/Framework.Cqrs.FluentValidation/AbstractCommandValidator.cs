﻿using FluentValidation;
using FluentValidation.Results;
using Framework.Cqrs.Commands;

namespace Framework.Cqrs.FluentValidation
{
    public abstract class AbstractCommandValidator<T> : AbstractValidator<T>, ICommandValidator<T>
        where T : class, ICommand
    {
        INotification ICommandValidator<T>.Validate(T instance)
        {
            ValidationResult result = Validate(instance);

            var notification = new Notification();
            foreach (ValidationFailure validationFailure in result.Errors)
                notification.Add(validationFailure.ErrorMessage);

            return notification;
        }
    }
}