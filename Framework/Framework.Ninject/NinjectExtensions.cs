﻿using System.Linq;
using Ninject.Activation;
using Ninject.Syntax;
using Ninject.Web.Common;

namespace Framework.IoC.Ninject
{
    /// <summary>
    /// Defines extension method that specifies InAmbientOrRequestScope.
    /// </summary>
    public static class AmbientOrRequestScopeExtensionMethod
    {
        /// <summary>
        /// Sets the scope to ambient or request scope.
        /// </summary>
        /// <typeparam name="T">The type of the service.</typeparam>
        /// <param name="syntax">The syntax.</param>
        /// <returns>The syntax to define more information.</returns>
        public static IBindingNamedWithOrOnSyntax<T> InAmbientOrRequestScope<T>(this IBindingInSyntax<T> syntax)
        {
            return syntax.InScope(GetScope);
        }

        /// <summary>
        /// Gets the scope.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>The scope.</returns>
        private static object GetScope(IContext context)
        {
            var scope = NinjectAmbientScope.Current;
            if (scope != null)
                return scope;

            //// Note: Copied from Ninject (https://github.com/ninject/Ninject.Web.Common/blob/master/src/Ninject.Web.Common/RequestScopeExtensionMethod.cs) subject to changes (out of sync with Ninject)
            return context.Kernel.Components.GetAll<INinjectHttpApplicationPlugin>().Select(c => c.GetRequestScope(context)).FirstOrDefault(s => s != null);
        }
    }
}
