﻿using System;
using Ninject;

namespace Framework.IoC.Ninject
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        protected readonly IKernel Kernel;

        public NinjectDependencyResolver(IKernel kernel)
        {
            Kernel = kernel ?? throw new ArgumentNullException(nameof(kernel));
        }

        public T Get<T>()
        {
            return Kernel.Get<T>();
        }

        public T TryGet<T>()
        {
            return Kernel.TryGet<T>();
        }

        public object Get(Type type)
        {
            return Kernel.Get(type);
        }

        public object TryGet(Type type)
        {
            return Kernel.TryGet(type);
        }
    }
}