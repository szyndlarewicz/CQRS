﻿using System;
using System.Runtime.Remoting.Messaging;
using Framework.Core.Collections;

namespace Framework.IoC.Ninject
{
    /// <summary>
    /// Ninject ambient scope used to create a scope directly in code.
    /// </summary>
    /// <remarks>Nested scopes are supported, but multi-threading in nested scopes is not supported.</remarks>
    public class NinjectAmbientScope : AmbientScope
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NinjectAmbientScope" /> class.
        /// </summary>
        public NinjectAmbientScope()
            : this(Guid.NewGuid().ToString())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NinjectAmbientScope" /> class.
        /// </summary>
        /// <param name="id">The scope identifier.</param>
        public NinjectAmbientScope(string id)
        {
            Id = id;

            ScopeStack = ScopeStack == null 
                ? ImmutableStack.Create(id)
                : ScopeStack.Push(id);
        }

        /// <summary>
        /// Gets the current ambient scope.
        /// </summary>
        /// <value>The current ambient scope.</value>
        public static object Current
        {
            get
            {
                if (ScopeStack == null || ScopeStack.IsEmpty)
                    return null;

                return ScopeStack.Peek();
            }
        }

        private static Core.Collections.ImmutableStack<string> ScopeStack
        {
            get => CallContext.LogicalGetData("scopes") as Core.Collections.ImmutableStack<string>;
            set => CallContext.LogicalSetData("scopes", value);
        }

        /// <summary>
        /// Gets or sets the scope identifier.
        /// </summary>
        /// <value>The scope identifier.</value>
        public string Id { get; set; }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            if (!ScopeStack.IsEmpty)
                ScopeStack = ScopeStack.Pop();

            if (ScopeStack.IsEmpty)
                CallContext.FreeNamedDataSlot("scopes");
        }
    }
}