﻿namespace Framework.IoC.Ninject
{
    public sealed class NinjectAmbientScopeFactory : IAmbientScopeFactory
    {
        public AmbientScope Create()
        {
            return new NinjectAmbientScope();
        }
    }
}