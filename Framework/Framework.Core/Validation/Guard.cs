﻿using System;
using System.Linq.Expressions;

namespace Framework.Core.Validation
{
    public static class Guard
    {
        public static void NotNull<T>(Expression<Func<T>> reference, T value)
        {
            if (value == null)
                throw new ArgumentNullException(GetParameterName(reference), "Parameter cannot be null.");
        }

        public static void NotNullOrEmpty(Expression<Func<string>> reference, string value)
        {
            NotNull(reference, value);
            if (value.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", GetParameterName(reference));
        }

        public static void NotNullOrEmpty(Expression<Func<Guid?>> reference, Guid? value)
        {
            NotNull(reference, value);
            if (value == Guid.Empty)
                throw new ArgumentException("Parameter cannot be empty.", GetParameterName(reference));
        }

        public static void NotEmpty(Expression<Func<Guid>> reference, Guid value)
        {
            if (value == Guid.Empty)
                throw new ArgumentException("Parameter cannot be empty.", GetParameterName(reference));
        }

        public static void NotEmpty(Expression<Func<string>> reference, string value)
        {
            if (value == string.Empty)
                throw new ArgumentException("Parameter cannot be empty.", GetParameterName(reference));
        }

        public static void IsValid<T>(Expression<Func<T>> reference, T value, Func<T, bool> validate, string message)
        {
            if (!validate(value))
                throw new ArgumentException(message, GetParameterName(reference));
        }

        public static void IsValid<T>(Expression<Func<T>> reference, T value, Func<T, bool> validate, string format, params object[] args)
        {
            if (!validate(value))
                throw new ArgumentException(string.Format(format, args), GetParameterName(reference));
        }

        private static string GetParameterName(Expression reference)
        {
            var lambda = reference as LambdaExpression;
            var member = lambda.Body as MemberExpression;

            return member.Member.Name;
        }
    }
}