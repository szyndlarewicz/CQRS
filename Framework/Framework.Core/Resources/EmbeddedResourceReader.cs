﻿using System;
using System.IO;
using System.Reflection;

namespace Framework.Core.Resources
{
    public class ResourceReader : IResourceReader
    {
        public string Read(string resourceName)
        {
            if (resourceName == null)
                throw new ArgumentNullException(nameof(resourceName));

            if (resourceName.Length == 0)
                throw new ArgumentException($"{nameof(resourceName)} cannot be empty");

            var assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream == null)
                    throw new InvalidOperationException($"Cannot read required resource '{resourceName}'");

                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
    }
}