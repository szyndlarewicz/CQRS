﻿namespace Framework.Core.Resources
{
    public interface IResourceReader
    {
        string Read(string resourceName);
    }
}