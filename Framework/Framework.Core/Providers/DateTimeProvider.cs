﻿using System;

namespace Framework.Core.Providers
{
    public abstract class DateTimeProvider
    {
        private static DateTimeProvider _current = new GmtDateTimeProvider();

        public static DateTimeProvider Current
        {
            get => _current;
            set => _current = value ?? throw new ArgumentNullException(nameof(value));
        }

        public abstract DateTime Now { get; }

        public static void Reset()
        {
            _current = new GmtDateTimeProvider();
        }
    }
}