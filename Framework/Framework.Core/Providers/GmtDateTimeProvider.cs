﻿using System;

namespace Framework.Core.Providers
{
    public sealed class GmtDateTimeProvider : DateTimeProvider
    {
        public override DateTime Now => DateTime.Now;
    }
}