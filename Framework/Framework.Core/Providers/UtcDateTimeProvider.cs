using System;

namespace Framework.Core.Providers
{
    public sealed class UtcDateTimeProvider : DateTimeProvider
    {
        public override DateTime Now => DateTime.UtcNow;
    }
}