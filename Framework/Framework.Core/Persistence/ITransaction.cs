﻿using System;

namespace Framework.Core.Persistence
{
    public interface ITransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}