﻿using System;

namespace Framework.Core.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        ITransaction BeginTransaction();

        int Save();
    }
}