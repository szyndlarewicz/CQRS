﻿namespace Framework.Core.Persistence
{
    public abstract class Poco<TKey>
    {
        public TKey Id { get; set; }
    }
}