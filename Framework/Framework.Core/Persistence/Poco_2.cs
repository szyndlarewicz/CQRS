﻿namespace Framework.Core.Persistence
{
    public abstract class Poco<TKey, TVersion> : Poco<TKey>
    {
        public TVersion Version { get; set; }
    }
}