﻿using System;
using System.Runtime.Serialization;

namespace Framework.Core.Logging
{
    [Serializable]
    public class UnrecognizedLogLevelException : ArgumentException
    {
        public UnrecognizedLogLevelException(string logLevel)
            : base("Unrecognized log level")
        {
            LogLevel = logLevel;
        }

        public string LogLevel { get; }
        
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("LogLevel", LogLevel);
            base.GetObjectData(info, context);
        }
    }
}