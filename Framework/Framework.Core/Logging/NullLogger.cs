﻿using System;

namespace Framework.Core.Logging
{
    public class NullLogger : ILogger
    {
        public void Log(LogLevel logLevel, string message)
        {
        }

        public void Log(LogLevel logLevel, Exception exception)
        {
        }

        public void Log(LogLevel logLevel, Exception exception, string message)
        {
        }
    }
}