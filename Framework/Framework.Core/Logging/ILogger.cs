﻿using System;

namespace Framework.Core.Logging
{
    public interface ILogger
    {
        void Log(LogLevel logLevel, string message);

        void Log(LogLevel logLevel, Exception exception);

        void Log(LogLevel logLevel, Exception exception, string message);
    }
}