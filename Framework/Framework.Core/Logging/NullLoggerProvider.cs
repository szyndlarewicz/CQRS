﻿namespace Framework.Core.Logging
{
    public class NullLoggerProvider : LoggerProvider
    {
        public override ILogger GetLogger<T>()
        {
            return new NullLogger();
        }

        public override ILogger GetLogger(string name)
        {
            return new NullLogger();
        }
    }
}
