﻿using System;
using System.Linq;

namespace Framework.Core.Logging
{
    public sealed class LogLevel : IEquatable<LogLevel>
    {
        private readonly string _value;

        public static LogLevel Trace => new LogLevel("Trace");
        public static LogLevel Debug => new LogLevel("Debug");
        public static LogLevel Info => new LogLevel("Info");
        public static LogLevel Warn => new LogLevel("Warn");
        public static LogLevel Error => new LogLevel("Error");
        public static LogLevel Fatal => new LogLevel("Fatal");

        private static readonly string[] AvailableLevels = { "Trace", "Debug", "Info", "Warn", "Error", "Fatal" };

        public LogLevel(string logLevel)
        {
            if (logLevel == null)
                throw new ArgumentNullException(nameof(logLevel));

            if (logLevel.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", nameof(logLevel));

            if (!AvailableLevels.Contains(logLevel))
                throw new UnrecognizedLogLevelException(logLevel);

            _value = logLevel;
        }

        public static explicit operator LogLevel(string instance)
        {
            return instance == null ? null : new LogLevel(instance);
        }

        public static implicit operator string(LogLevel instance)
        {
            return instance?._value;
        }

        public static bool operator ==(LogLevel x, LogLevel y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(LogLevel x, LogLevel y)
        {
            return !(x == y);
        }
        
        public override bool Equals(object value)
        {
            return Equals(value as LogLevel);
        }

        public bool Equals(LogLevel value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}