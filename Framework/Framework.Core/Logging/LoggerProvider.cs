﻿using System;

namespace Framework.Core.Logging
{
    public abstract class LoggerProvider
    {
        private static LoggerProvider _current = new NullLoggerProvider();

        public static LoggerProvider Current
        {
            get => _current;
            set => _current = value ?? throw new ArgumentNullException(nameof(value));
        }

        public static void Reset()
        {
            _current = new NullLoggerProvider();
        }

        public abstract ILogger GetLogger<T>();

        public abstract ILogger GetLogger(string name);
    }
}