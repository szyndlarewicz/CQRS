﻿using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace Framework.Core.Authentication
{
    public static class Rfc2898DeriveBytesCrypto
    {
        public const int DefaultSaltSize = 16;
        public const int DefaultHashIterations = 5000;

        public static string GenerateSalt()
        {
            return GenerateSalt(DefaultSaltSize, DefaultHashIterations);
        }

        public static string GenerateSalt(int saltSize, int hashIterations)
        {
            RandomNumberGenerator random = RandomNumberGenerator.Create();
            byte[] data = new byte[saltSize];
            random.GetBytes(data);

            return hashIterations + "." + Convert.ToBase64String(data);
        }

        public static bool Verify(string textToHash, string textToHashSalt, string hashedText)
        {
            if (textToHash == null)
                throw new ArgumentNullException(nameof(textToHash));

            if (textToHash.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", nameof(textToHash));

            if (textToHashSalt == null)
                throw new ArgumentNullException(nameof(textToHashSalt));

            if (textToHashSalt.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", nameof(textToHashSalt));

            if (hashedText == null)
                throw new ArgumentNullException(nameof(hashedText));

            if (hashedText.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", nameof(textToHashSalt));

            int hashIterations = ExpandHashIterations(textToHashSalt);
            return CalculateHash(textToHash, textToHashSalt, hashIterations) == hashedText;
        }

        public static string CalculateHash(string textToHash, string textToHashSalt, int hashIterations)
        {
            if (textToHash == null)
                throw new ArgumentNullException(nameof(textToHash));

            if (textToHash.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", nameof(textToHash));

            if (textToHashSalt == null)
                throw new ArgumentNullException(nameof(textToHashSalt));

            if (textToHashSalt.Length == 0)
                throw new ArgumentException("Parameter cannot be empty.", nameof(textToHashSalt));

            if (hashIterations <= 1)
                throw new ArgumentNullException($"Cannot use a salt with iterations {hashIterations}, use a value greater than 1, recommended: 5000.");

            byte[] saltBytes = Encoding.UTF8.GetBytes(textToHashSalt);
            using (Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(textToHash, saltBytes, hashIterations))
            {
                byte[] key = rfc2898DeriveBytes.GetBytes(64);
                return Convert.ToBase64String(key);
            }
        }

        public static int ExpandHashIterations(string salt)
        {
            int index = salt.IndexOf('.');

            int hashIterations;
            if (!int.TryParse(salt.Substring(0, index), NumberStyles.Number, CultureInfo.InvariantCulture, out hashIterations))
                throw new FormatException("The salt was not in an expected format of {int}.{string}.");

            return hashIterations;
        }
    }
}