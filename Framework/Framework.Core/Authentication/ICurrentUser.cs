﻿using System;

namespace Framework.Core.Authentication
{
    public interface ICurrentUser
    {
        Guid UserId { get; }

        bool IsInRole(string roleName);
    }
}