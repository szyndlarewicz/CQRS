﻿using System.Collections.Generic;

namespace Framework.Domain
{
    public interface IEventsProviderAggregateRoot : IAggregateRoot
    {
        IEnumerable<IEvent> Events { get; }

        void Flush();
    }
}