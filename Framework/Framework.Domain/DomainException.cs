﻿using System;
using System.Runtime.Serialization;

namespace Framework.Domain
{
    [Serializable]
    public abstract class DomainException : Exception
    {
        protected DomainException()
            : base("Something went wrong")
        {
        }

        protected DomainException(string message) 
            : base(message)
        {
        }

        protected DomainException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected DomainException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
#if DEBUG
            base.GetObjectData(info, context);
#else
            info.AddValue("ClassName", GetType().FullName);
            info.AddValue("Message", Message);
#endif
        }
    }
}