﻿namespace Framework.Domain
{
    public interface IEventsProviderAggregateRoot<out TIdentity, out TVersion> : IEventsProviderAggregateRoot<TIdentity>, IAggregateRoot<TIdentity, TVersion>
        where TIdentity : EntityIdentity
        where TVersion : AggregateVersion
    {
    }
}