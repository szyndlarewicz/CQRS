﻿using System;

namespace Framework.Domain
{
    public class AggregateVersion : IEquatable<AggregateVersion>, IValueObject
    {
        protected readonly long Version;

        public AggregateVersion()
        {
            Version = 1;
        }

        public AggregateVersion(long version)
        {
            if(version == 0)
                throw new ArgumentException("Aggregate version cannot be lower than 1");

            Version = version;
        }

        public static AggregateVersion First => new AggregateVersion();

        public AggregateVersion Next => new AggregateVersion(Version + 1);

        public static explicit operator AggregateVersion(long instance)
        {
            return instance == 0 ? null : new AggregateVersion(instance);
        }

        public static explicit operator AggregateVersion(long? instance)
        {
            return (instance == null || instance == 0) ? null : new AggregateVersion(instance.Value);
        }

        public static implicit operator long?(AggregateVersion instance)
        {
            return instance?.Version;
        }

        public static implicit operator long(AggregateVersion instance)
        {
            return instance?.Version ?? 0;
        }

        public static bool operator ==(AggregateVersion x, AggregateVersion y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(AggregateVersion x, AggregateVersion y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as AggregateVersion);
        }

        public bool Equals(AggregateVersion value)
        {
            return !ReferenceEquals(null, value) && string.Equals(Version, value.Version);
        }

        public override int GetHashCode()
        {
            return Version.GetHashCode();
        }

        public override string ToString()
        {
            return Version.ToString();
        }
    }
}