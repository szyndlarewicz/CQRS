﻿namespace Framework.Domain
{
    public interface IAggregateRoot<out TIdentity, out TVersion> : IEntity<TIdentity>, IAggregateRoot
        where TIdentity : EntityIdentity
        where TVersion : AggregateVersion
    {
        TVersion Version { get; }
    }
}