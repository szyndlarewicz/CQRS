﻿using System;

namespace Framework.Domain
{
    public abstract class EntityWithState<T> : IEntity
        where T : class
    {
        protected T State { get; set; }

        protected EntityWithState(T state)
        {
            State = state ?? throw new ArgumentNullException(nameof(state));
        }

        public static implicit operator T(EntityWithState<T> instance)
        {
            return instance?.State;
        }
    }
}