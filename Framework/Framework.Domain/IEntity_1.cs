﻿namespace Framework.Domain
{
    public interface IEntity<out TIdentity> : IEntity
        where TIdentity : EntityIdentity
    {
        TIdentity Id { get; }
    }
}