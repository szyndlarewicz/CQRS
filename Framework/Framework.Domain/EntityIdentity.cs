﻿using System;

namespace Framework.Domain
{
    public class EntityIdentity : IEquatable<EntityIdentity>, IValueObject
    {
        protected readonly Guid Identity;

        public EntityIdentity(Guid identity)
        {
            if (identity == Guid.Empty)
                throw new ArgumentException("Identity cannot be empty");

            Identity = identity;
        }

        public EntityIdentity(Guid? identity)
        {
            if (identity == null)
                throw new ArgumentNullException("Identity cannot be null");

            if (identity == Guid.Empty)
                throw new ArgumentException("Identity cannot be empty");

            Identity = identity.Value;
        }

        public EntityIdentity(string identity)
            : this(Guid.Parse(identity))
        {
        }

        public EntityIdentity()
            : this(Guid.NewGuid())
        {
        }

        public static EntityIdentity New => new EntityIdentity();

        public static explicit operator EntityIdentity(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new EntityIdentity(instance);
        }

        public static explicit operator EntityIdentity(Guid instance)
        {
            return instance == Guid.Empty ? null : new EntityIdentity(instance);
        }

        public static explicit operator EntityIdentity(Guid? instance)
        {
            return (instance == null || instance == Guid.Empty) ? null : new EntityIdentity(instance.Value);
        }
        
        public static implicit operator Guid?(EntityIdentity instance)
        {
            return instance?.Identity;
        }

        public static implicit operator Guid(EntityIdentity instance)
        {
            return instance?.Identity ?? Guid.Empty;
        }

        public static bool operator ==(EntityIdentity x, EntityIdentity y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(EntityIdentity x, EntityIdentity y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as EntityIdentity);
        }

        public bool Equals(EntityIdentity value)
        {
            return !ReferenceEquals(null, value) && string.Equals(Identity, value.Identity);
        }

        public override int GetHashCode()
        {
            return Identity.GetHashCode();
        }

        public override string ToString()
        {
            return Identity.ToString();
        }
    }
}