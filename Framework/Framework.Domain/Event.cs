﻿using System;

namespace Framework.Domain
{
    public abstract class Event : IEvent
    {
        protected Event(Guid aggregateId)
        {
            AggregateId = aggregateId;
        }

        public Guid AggregateId { get; }
    }
}