﻿namespace Framework.Domain
{
    public interface IAggregateRoot<out TIdentity> : IEntity<TIdentity>, IAggregateRoot
        where TIdentity : EntityIdentity
    {
    }
}