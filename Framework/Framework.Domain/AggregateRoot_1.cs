﻿using System.Collections.Generic;

namespace Framework.Domain
{
    public abstract class AggregateRoot<T> : EntityWithState<T>, IEventsProviderAggregateRoot, IAggregateRoot
        where T : class, new()
    {
        private readonly Queue<IEvent> _events = new Queue<IEvent>();

        protected AggregateRoot(T state) 
            : base(state)
        {
        }

        public IEnumerable<IEvent> Events => _events;

        public void Flush()
        {
            _events.Clear();
        }

        protected void Enqueue(IEvent @event)
        {
            _events.Enqueue(@event);
        }
    }
}