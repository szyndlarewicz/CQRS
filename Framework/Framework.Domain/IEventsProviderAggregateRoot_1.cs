﻿namespace Framework.Domain
{
    public interface IEventsProviderAggregateRoot<out TIdentity> : IEventsProviderAggregateRoot, IAggregateRoot<TIdentity>
        where TIdentity : EntityIdentity
    {
    }
}