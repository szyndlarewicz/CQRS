﻿using System;
using System.Linq;
using Framework.Cqrs.Events;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class EventHandlerBinding : BindingStrategy
    {
        private readonly IEventHandlerRegistrar _eventHandlerRegistrar;

        public EventHandlerBinding(IDependencyRegister dependencyRegister, IEventHandlerRegistrar eventHandlerRegistrar) 
            : base(dependencyRegister)
        {
            _eventHandlerRegistrar = eventHandlerRegistrar;
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(IEventHandler)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);

            Type @event = definition.GetGenericArguments().First();
            Type eventHandler = implementation;
            _eventHandlerRegistrar.Register(@event, eventHandler);
        }
    }
}