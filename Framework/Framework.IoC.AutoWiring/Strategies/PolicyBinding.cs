﻿using System;
using System.Linq;
using Framework.Domain;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class PolicyBinding : BindingStrategy
    {
        public PolicyBinding(IDependencyRegister dependencyRegister)
            : base(dependencyRegister)
        {
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(IStrategy)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);
        }
    }
}