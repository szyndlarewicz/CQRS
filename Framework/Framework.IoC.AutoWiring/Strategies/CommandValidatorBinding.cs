﻿using System;
using System.Linq;
using Framework.Cqrs.Commands;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class CommandValidatorBinding : BindingStrategy
    {
        public CommandValidatorBinding(IDependencyRegister dependencyRegister) 
            : base(dependencyRegister)
        {
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(ICommandValidator)));
            DependencyRegister.RegisterTransient(definition, implementation);
        }
    }
}