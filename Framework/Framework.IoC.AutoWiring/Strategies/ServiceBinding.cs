﻿using System;
using System.Linq;
using Framework.Domain;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class ServiceBinding : BindingStrategy
    {
        public ServiceBinding(IDependencyRegister dependencyRegister) 
            : base(dependencyRegister)
        {
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(IService)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);
        }
    }
}