﻿using System;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class BindingUnknownStrategy : BindingStrategy
    {
        public BindingUnknownStrategy(IDependencyRegister dependencyRegister) 
            : base(dependencyRegister)
        {
        }

        public override void Bind(Type implementation)
        {
            DependencyRegister.RegisterTransient(implementation);
        }
    }
}