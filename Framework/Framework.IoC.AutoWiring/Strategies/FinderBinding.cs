﻿using System;
using System.Linq;
using Framework.Search.Infrastructure;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class FinderBinding : BindingStrategy
    {
        public FinderBinding(IDependencyRegister dependencyRegister) 
            : base(dependencyRegister)
        {
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(IFinder)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);
        }
    }
}