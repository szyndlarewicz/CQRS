﻿using System;
using System.Linq;
using Framework.Domain;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class StrategyBinding : BindingStrategy
    {
        public StrategyBinding(IDependencyRegister dependencyRegister)
            : base(dependencyRegister)
        {
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(IPolicy)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);
        }
    }
}