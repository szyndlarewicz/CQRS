﻿using System;
using System.Linq;
using Framework.Cqrs.Queries;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class QueryHandlerBinding : BindingStrategy
    {
        private readonly IQueryHandlerRegistrar _queryHandlerRegistrar;

        public QueryHandlerBinding(IDependencyRegister dependencyRegister, IQueryHandlerRegistrar queryHandlerRegistrar) 
            : base(dependencyRegister)
        {
            _queryHandlerRegistrar = queryHandlerRegistrar;
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(IQueryHandler)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);

            Type query = definition.GetGenericArguments().Single(c => c.GetInterfaces().Contains(typeof(IQuery)));
            Type view = definition.GetGenericArguments().Single(c => !c.GetInterfaces().Contains(typeof(IQuery)));

            Type queryHandler = implementation;
            _queryHandlerRegistrar.Register(query, view, queryHandler);
        }
    }
}