﻿using System;
using System.Linq;
using Framework.Cqrs.Commands;

namespace Framework.IoC.AutoWiring.Strategies
{
    public sealed class CommandHandlerBinding : BindingStrategy
    {
        private readonly ICommandHandlerRegistrar _commandHandlerRegistrar;

        public CommandHandlerBinding(IDependencyRegister dependencyRegister, ICommandHandlerRegistrar commandHandlerRegistrar) 
            : base(dependencyRegister)
        {
            _commandHandlerRegistrar = commandHandlerRegistrar;
        }

        public override void Bind(Type implementation)
        {
            Type definition = implementation.GetInterfaces().Single(c => c.GetInterfaces().Contains(typeof(ICommandHandler)));
            DependencyRegister.RegisterAmbientScope(definition, implementation);

            Type command = definition.GetGenericArguments().First();
            Type commandHandler = implementation;
            _commandHandlerRegistrar.Register(command, commandHandler);
        }
    }
}