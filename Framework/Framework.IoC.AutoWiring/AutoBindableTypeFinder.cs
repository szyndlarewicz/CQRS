using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Framework.IoC.AutoWiring
{
    public sealed class AutoBindableTypeFinder : IAutoBindableTypeFinder
    {
        public IEnumerable<Type> Find(IEnumerable<Assembly> assemblies)
        {
            if (assemblies == null)
                throw new ArgumentNullException(nameof(assemblies));

            return assemblies
                .SelectMany(assembly => assembly.GetTypes())
                .Where(type => type.GetCustomAttribute<AutoBindAttribute>() != null)
                .ToList();
        }
    }
}