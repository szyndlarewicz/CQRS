﻿using System;
using System.Runtime.Serialization;

namespace Framework.IoC.AutoWiring
{
    [Serializable]
    public sealed class BindingStrategyNotFoundException : Exception
    {
        public BindingStrategyNotFoundException(Type keyType)
            : base($"Binding strategy not found for given type '{keyType.FullName}'")
        {
            KeyType = keyType;
        }

        public Type KeyType { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("KeyType", KeyType);
            base.GetObjectData(info, context);
        }
    }
}