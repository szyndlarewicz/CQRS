﻿using System;
using System.Collections.Generic;

namespace Framework.IoC.AutoWiring
{
    public interface IBindingStrategyMapping
    {
        IBindingStrategyMapping Register<TKey, TBindingStrategy>()
            where TKey : class
            where TBindingStrategy : BindingStrategy;

        IBindingStrategyMapping Register(Type keyType, Type bindingStrategy);

        Type FindStrategy(IEnumerable<Type> keyTypes);

        Type GetStrategy(Type keyType);
    }
}