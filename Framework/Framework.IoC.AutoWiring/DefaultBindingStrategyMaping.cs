﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Cqrs.Commands;
using Framework.Cqrs.Events;
using Framework.Cqrs.Queries;
using Framework.Domain;
using Framework.IoC.AutoWiring.Strategies;
using Framework.Search.Infrastructure;

namespace Framework.IoC.AutoWiring
{
    public class DefaultBindingStrategyMaping : IBindingStrategyMapping
    {
        protected readonly IDictionary<Type, Type> Mapping;

        public DefaultBindingStrategyMaping()
        {
            Mapping = new Dictionary<Type, Type>
            {
                { typeof(ICommandValidator), typeof(CommandValidatorBinding) },
                { typeof(ICommandHandler), typeof(CommandHandlerBinding) },
                { typeof(IEventHandler), typeof(EventHandlerBinding) },
                { typeof(IQueryHandler), typeof(QueryHandlerBinding) },
                { typeof(IService), typeof(ServiceBinding) },
                { typeof(IFactory), typeof(FactoryBinding) },
                { typeof(IRepository), typeof(RepositoryBinding) },
                { typeof(IFinder), typeof(FinderBinding) },
                { typeof(IStrategy), typeof(PolicyBinding) },
                { typeof(IPolicy), typeof(StrategyBinding) }
            };
        }

        public IBindingStrategyMapping Register<TKey, TBindingStrategy>()
            where TKey : class
            where TBindingStrategy : BindingStrategy
        {
            Type keyType = typeof(TKey);
            if (Mapping.ContainsKey(keyType))
                throw new BindingStrategyRegisteredException(keyType);

            Mapping.Add(keyType, typeof(TBindingStrategy));
            return this;
        }

        public IBindingStrategyMapping Register(Type keyType, Type bindingStrategy)
        {
            if (keyType == null)
                throw new ArgumentNullException(nameof(keyType));

            if (Mapping.ContainsKey(keyType))
                throw new BindingStrategyRegisteredException(keyType);

            if (bindingStrategy == null)
                throw new ArgumentNullException(nameof(bindingStrategy));

            if (bindingStrategy.BaseType == typeof(BindingStrategy))
                throw new ArgumentException($"Binding strategy '{bindingStrategy.FullName}' does not implement class BindingStrategy");

            Mapping.Add(keyType, bindingStrategy);
            return this;
        }

        public Type FindStrategy(IEnumerable<Type> keyTypes)
        {
            if (keyTypes == null)
                throw new ArgumentNullException(nameof(keyTypes));

            return keyTypes.Where(Mapping.ContainsKey).Select(c => Mapping[c]).SingleOrDefault();
        }

        public Type GetStrategy(Type keyType)
        {
            if (keyType == null)
                throw new ArgumentNullException(nameof(keyType));
            
            if (!Mapping.ContainsKey(keyType))
                throw new BindingStrategyNotFoundException(keyType);

            return Mapping[keyType];
        }
    }
}