﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Framework.Core.Logging;

namespace Framework.IoC.AutoWiring
{
    public sealed class AutoBindProcessor
    {
        private readonly IAssemblyProvider _assemblyProvider;
        private readonly IAutoBindableTypeFinder _autoBindableTypeFinder;
        private readonly IBindingStrategyFactory _bindingStrategyFactory;
        private readonly ILogger _logger;

        public AutoBindProcessor(
            IAssemblyProvider assemblyProvider,
            IAutoBindableTypeFinder autoBindableTypeFinder,
            IBindingStrategyFactory bindingStrategyFactory,
            ILogger logger)
        {
            _assemblyProvider = assemblyProvider ?? throw new ArgumentNullException(nameof(assemblyProvider));
            _autoBindableTypeFinder = autoBindableTypeFinder ?? throw new ArgumentNullException(nameof(autoBindableTypeFinder));
            _bindingStrategyFactory = bindingStrategyFactory ?? throw new ArgumentNullException(nameof(bindingStrategyFactory));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void Execute()
        {
            _logger.Log((LogLevel)LogLevel.Info, "Autowiring process start");

            IEnumerable<Assembly> assemblies = _assemblyProvider.GetProjectAssemblies();
            IEnumerable<Type> types = _autoBindableTypeFinder.Find(assemblies.ToArray());

            foreach (Type type in types)
            {
                _logger.Log((LogLevel) LogLevel.Info, $"Binding '{type}'");

                try
                {
                    BindingStrategy bindingStrategy = _bindingStrategyFactory.Create(type);
                    bindingStrategy.Bind(type);
                }
                catch (Exception exception)
                {
                    _logger.Log((LogLevel)LogLevel.Error, exception, $"Error occurred during bind the type '{type}'");
                }
            }

            _logger.Log((LogLevel)LogLevel.Info, "Autowiring process end");
        }
    }
}