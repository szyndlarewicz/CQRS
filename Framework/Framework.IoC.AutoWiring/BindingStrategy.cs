﻿using System;

namespace Framework.IoC.AutoWiring
{
    public abstract class BindingStrategy
    {
        protected IDependencyRegister DependencyRegister { get; }

        protected BindingStrategy(IDependencyRegister dependencyRegister)
        {
            DependencyRegister = dependencyRegister ?? throw new ArgumentNullException(nameof(dependencyRegister));
        }

        public abstract void Bind(Type implementation);
    }
}