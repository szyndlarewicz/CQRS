﻿using System;
using System.Linq;
using Framework.IoC.AutoWiring.Strategies;

namespace Framework.IoC.AutoWiring
{
    public sealed class BindingStrategyFactory : IBindingStrategyFactory
    {
        private readonly IDependencyResolver _dependencyResolver;

        private readonly IBindingStrategyMapping _bindingStrategyMapping;

        public BindingStrategyFactory(
            IDependencyResolver dependencyResolver,
            IBindingStrategyMapping bindingStrategyMapping)
        {
            _dependencyResolver = dependencyResolver ?? throw new ArgumentNullException(nameof(dependencyResolver));
            _bindingStrategyMapping = bindingStrategyMapping ?? throw new ArgumentNullException(nameof(bindingStrategyMapping));
        }

        public BindingStrategy Create(Type bindingType)
        {
            if (bindingType == null)
                throw new ArgumentNullException(nameof(bindingType));

            var possibleKeys = bindingType.GetInterfaces().Union(new [] { bindingType.BaseType });
            Type bindingStrategyType = _bindingStrategyMapping.FindStrategy(possibleKeys)
                ?? typeof(BindingUnknownStrategy);

            return (BindingStrategy) _dependencyResolver.Get(bindingStrategyType);
        }
    }
}