using System;
using System.Collections.Generic;
using System.Reflection;

namespace Framework.IoC.AutoWiring
{
    public interface IAutoBindableTypeFinder
    {
        IEnumerable<Type> Find(IEnumerable<Assembly> assemblies);
    }
}