﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Framework.IoC.AutoWiring
{
    public sealed class CurrentAppDomainAssemblyProvider : IAssemblyProvider
    {
        public IEnumerable<Assembly> GetProjectAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies();
        }
    }
}