﻿using System.Collections.Generic;
using System.Reflection;

namespace Framework.IoC.AutoWiring
{
    public interface IAssemblyProvider
    {
        IEnumerable<Assembly> GetProjectAssemblies();
    }
}