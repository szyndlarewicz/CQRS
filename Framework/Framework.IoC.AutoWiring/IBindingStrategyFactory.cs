﻿using System;

namespace Framework.IoC.AutoWiring
{
    public interface IBindingStrategyFactory
    {
        BindingStrategy Create(Type bindingType);
    }
}