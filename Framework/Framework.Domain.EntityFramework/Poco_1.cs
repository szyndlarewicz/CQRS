﻿namespace Framework.Domain.EntityFramework
{
    /// <summary>
    /// The POCO class with id. Use to mark all classes that will be stored in database.
    /// </summary>
    /// <typeparam name="TKey">The Id type</typeparam>
    public abstract class Poco<TKey> : Poco
    {
        public TKey Id { get; set; }
    }
}