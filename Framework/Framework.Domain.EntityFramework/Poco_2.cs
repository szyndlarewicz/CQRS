﻿namespace Framework.Domain.EntityFramework
{
    /// <summary>
    /// The POCO class with id and version. Use to mark all classes that will be stored in database.
    /// </summary>
    /// <typeparam name="TKey">The Id type</typeparam>
    /// <typeparam name="TVersion">The Version type</typeparam>
    public abstract class Poco<TKey, TVersion> : Poco<TKey>
    {
        public TVersion Version { get; set; }
    }
}