﻿using System;

namespace Framework.Domain.ValueObjects
{
    public sealed class ExchangeRate : IValueObject
    {
        public ExchangeRate(DateTime date, CurrencyCode source, CurrencyCode result, decimal rate)
        {
            Day = date;
            Source = source;
            Result = result;
            Rate = rate;
        }

        public DateTime Day { get; }

        public CurrencyCode Source { get; }

        public CurrencyCode Result { get; }

        public decimal Rate { get; }

        public static implicit operator string(ExchangeRate instance)
        {
            return instance?.ToString();
        }

        public static bool operator ==(ExchangeRate x, ExchangeRate y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(ExchangeRate x, ExchangeRate y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as ExchangeRate);
        }

        public bool Equals(ExchangeRate value)
        {
            return !ReferenceEquals(null, value) 
                && DateTime.Equals(Day, value.Day)
                && CurrencyCode.Equals(Source, value.Source)
                && CurrencyCode.Equals(Result, value.Result)
                && decimal.Equals(Rate, value.Rate);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ Day.GetHashCode();
                hash = (hash * HashingMultiplier) ^ Source.GetHashCode();
                hash = (hash * HashingMultiplier) ^ Result.GetHashCode();
                hash = (hash * HashingMultiplier) ^ Rate.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"{Source}/{Result} {Rate} {Day.ToShortDateString()}";
        }
    }
}