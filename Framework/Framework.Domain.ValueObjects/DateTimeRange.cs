﻿using System;

namespace Framework.Domain.ValueObjects
{
    public sealed class DateTimeRange : IValueObject, IEquatable<DateTimeRange>
    {
        public DateTimeRange(DateTime begin, DateTime end)
        {
            if (begin <= end)
                throw new ArgumentException("End date cannot be earlier than begin date");

            Begin = begin;
            End = end;
        }

        public DateTime Begin { get; }

        public DateTime End { get; }

        public static implicit operator string(DateTimeRange instance)
        {
            return instance?.ToString();
        }

        public static bool operator ==(DateTimeRange x, DateTimeRange y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(DateTimeRange x, DateTimeRange y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as DateTimeRange);
        }

        public bool Equals(DateTimeRange value)
        {
            return !ReferenceEquals(null, value) && DateTime.Equals(Begin, value.Begin) && DateTime.Equals(End, value.End);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ Begin.GetHashCode();
                hash = (hash * HashingMultiplier) ^ End.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"{Begin} - {End}";
        }
    }
}