﻿namespace Framework.Domain.ValueObjects
{
    public sealed class Money : IValueObject
    {
        public Money(CurrencyCode currencyCode, decimal amount)
        {
            CurrencyCode = currencyCode;
            Amount = amount;
        }

        public CurrencyCode CurrencyCode { get; }

        public decimal Amount { get; }

        public static implicit operator string(Money instance)
        {
            return instance?.ToString();
        }

        public static bool operator ==(Money x, Money y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(Money x, Money y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as Money);
        }

        public bool Equals(Money value)
        {
            return !ReferenceEquals(null, value) && CurrencyCode.Equals(CurrencyCode, value.CurrencyCode) && decimal.Equals(Amount, value.Amount);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ CurrencyCode.GetHashCode();
                hash = (hash * HashingMultiplier) ^ Amount.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"{Amount} {CurrencyCode}";
        }
    }
}