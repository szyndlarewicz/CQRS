﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Framework.Domain.ValueObjects
{
    [SuppressMessage("Microsoft.StyleCop.CSharp.LayoutRules", "SA1516:ElementsMustBeSeparatedByBlankLine", Justification = "Too many similar properties")]
    public sealed class CountryCode : IValueObject, IEquatable<CountryCode>
    {
        private static readonly string[] Codes =
        {
            "ABW",
            "AFG",
            "AGO",
            "AIA",
            "ALA",
            "ALB",
            "AND",
            "ANT",
            "ARE",
            "ARG",
            "ARM",
            "ASM",
            "ATA",
            "ATF",
            "ATG",
            "AUS",
            "AUT",
            "AZE",
            "BDI",
            "BEL",
            "BEN",
            "BFA",
            "BGD",
            "BGR",
            "BHR",
            "BHS",
            "BIH",
            "BLM",
            "BLR",
            "BLZ",
            "BMU",
            "BOL",
            "BRA",
            "BRB",
            "BRN",
            "BTN",
            "BVT",
            "BWA",
            "CAF",
            "CAN",
            "CCK",
            "CHE",
            "CHL",
            "CHN",
            "CIV",
            "CMR",
            "COD",
            "COG",
            "COK",
            "COL",
            "COM",
            "CPV",
            "CRI",
            "CUB",
            "CXR",
            "CYM",
            "CYP",
            "CZE",
            "DEU",
            "DJI",
            "DMA",
            "DNK",
            "DOM",
            "DZA",
            "ECU",
            "EGY",
            "ERI",
            "ESH",
            "ESP",
            "EST",
            "ETH",
            "FIN",
            "FJI",
            "FLK",
            "FRA",
            "FRO",
            "FSM",
            "GAB",
            "GBR",
            "GEO",
            "GGY",
            "GHA",
            "GIB",
            "GIN",
            "GLP",
            "GMB",
            "GNB",
            "GNQ",
            "GRC",
            "GRD",
            "GRL",
            "GTM",
            "GUF",
            "GUM",
            "GUY",
            "HKG",
            "HMD",
            "HND",
            "HRV",
            "HTI",
            "HUN",
            "IDN",
            "IMN",
            "IND",
            "IOT",
            "IRL",
            "IRN",
            "IRQ",
            "ISL",
            "ISR",
            "ITA",
            "JAM",
            "JEY",
            "JOR",
            "JPN",
            "KAZ",
            "KEN",
            "KGZ",
            "KHM",
            "KIR",
            "KNA",
            "KOR",
            "KWT",
            "LAO",
            "LBN",
            "LBR",
            "LBY",
            "LCA",
            "LIE",
            "LKA",
            "LSO",
            "LTU",
            "LUX",
            "LVA",
            "MAC",
            "MAF",
            "MAR",
            "MCO",
            "MDA",
            "MDG",
            "MDV",
            "MEX",
            "MHL",
            "MKD",
            "MLI",
            "MLT",
            "MMR",
            "MNE",
            "MNG",
            "MNP",
            "MOZ",
            "MRT",
            "MSR",
            "MTQ",
            "MUS",
            "MWI",
            "MYS",
            "MYT",
            "NAM",
            "NCL",
            "NER",
            "NFK",
            "NGA",
            "NIC",
            "NIU",
            "NLD",
            "NOR",
            "NPL",
            "NRU",
            "NZL",
            "OMN",
            "PAK",
            "PAN",
            "PCN",
            "PER",
            "PHL",
            "PLW",
            "PNG",
            "POL",
            "PRI",
            "PRK",
            "PRT",
            "PRY",
            "PSE",
            "PYF",
            "QAT",
            "REU",
            "ROU",
            "RUS",
            "RWA",
            "SAU",
            "SDN",
            "SEN",
            "SGP",
            "SGS",
            "SHN",
            "SJM",
            "SLB",
            "SLE",
            "SLV",
            "SMR",
            "SOM",
            "SPM",
            "SRB",
            "STP",
            "SUR",
            "SVK",
            "SVN",
            "SWE",
            "SWZ",
            "SYC",
            "SYR",
            "TCA",
            "TCD",
            "TGO",
            "THA",
            "TJK",
            "TKL",
            "TKM",
            "TLS",
            "TON",
            "TTO",
            "TUN",
            "TUR",
            "TUV",
            "TWN",
            "TZA",
            "UGA",
            "UKR",
            "UMI",
            "URY",
            "USA",
            "UZB",
            "VAT",
            "VCT",
            "VEN",
            "VGB",
            "VIR",
            "VNM",
            "VUT",
            "WLF",
            "WSM",
            "YEM",
            "ZAF",
            "ZMB",
            "ZWE"
        };

        private readonly string _value;

        public CountryCode(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!Codes.Contains(value))
                throw new ArgumentException("Unrecognized country code");

            _value = value;
        }

        public static CountryCode Abw => new CountryCode("ABW");
        public static CountryCode Afg => new CountryCode("AFG");
        public static CountryCode Ago => new CountryCode("AGO");
        public static CountryCode Aia => new CountryCode("AIA");
        public static CountryCode Ala => new CountryCode("ALA");
        public static CountryCode Alb => new CountryCode("ALB");
        public static CountryCode And => new CountryCode("AND");
        public static CountryCode Ant => new CountryCode("ANT");
        public static CountryCode Are => new CountryCode("ARE");
        public static CountryCode Arg => new CountryCode("ARG");
        public static CountryCode Arm => new CountryCode("ARM");
        public static CountryCode Asm => new CountryCode("ASM");
        public static CountryCode Ata => new CountryCode("ATA");
        public static CountryCode Atf => new CountryCode("ATF");
        public static CountryCode Atg => new CountryCode("ATG");
        public static CountryCode Aus => new CountryCode("AUS");
        public static CountryCode Aut => new CountryCode("AUT");
        public static CountryCode Aze => new CountryCode("AZE");
        public static CountryCode Bdi => new CountryCode("BDI");
        public static CountryCode Bel => new CountryCode("BEL");
        public static CountryCode Ben => new CountryCode("BEN");
        public static CountryCode Bfa => new CountryCode("BFA");
        public static CountryCode Bgd => new CountryCode("BGD");
        public static CountryCode Bgr => new CountryCode("BGR");
        public static CountryCode Bhr => new CountryCode("BHR");
        public static CountryCode Bhs => new CountryCode("BHS");
        public static CountryCode Bih => new CountryCode("BIH");
        public static CountryCode Blm => new CountryCode("BLM");
        public static CountryCode Blr => new CountryCode("BLR");
        public static CountryCode Blz => new CountryCode("BLZ");
        public static CountryCode Bmu => new CountryCode("BMU");
        public static CountryCode Bol => new CountryCode("BOL");
        public static CountryCode Bra => new CountryCode("BRA");
        public static CountryCode Brb => new CountryCode("BRB");
        public static CountryCode Brn => new CountryCode("BRN");
        public static CountryCode Btn => new CountryCode("BTN");
        public static CountryCode Bvt => new CountryCode("BVT");
        public static CountryCode Bwa => new CountryCode("BWA");
        public static CountryCode Caf => new CountryCode("CAF");
        public static CountryCode Can => new CountryCode("CAN");
        public static CountryCode Cck => new CountryCode("CCK");
        public static CountryCode Che => new CountryCode("CHE");
        public static CountryCode Chl => new CountryCode("CHL");
        public static CountryCode Chn => new CountryCode("CHN");
        public static CountryCode Civ => new CountryCode("CIV");
        public static CountryCode Cmr => new CountryCode("CMR");
        public static CountryCode Cod => new CountryCode("COD");
        public static CountryCode Cog => new CountryCode("COG");
        public static CountryCode Cok => new CountryCode("COK");
        public static CountryCode Col => new CountryCode("COL");
        public static CountryCode Com => new CountryCode("COM");
        public static CountryCode Cpv => new CountryCode("CPV");
        public static CountryCode Cri => new CountryCode("CRI");
        public static CountryCode Cub => new CountryCode("CUB");
        public static CountryCode Cxr => new CountryCode("CXR");
        public static CountryCode Cym => new CountryCode("CYM");
        public static CountryCode Cyp => new CountryCode("CYP");
        public static CountryCode Cze => new CountryCode("CZE");
        public static CountryCode Deu => new CountryCode("DEU");
        public static CountryCode Dji => new CountryCode("DJI");
        public static CountryCode Dma => new CountryCode("DMA");
        public static CountryCode Dnk => new CountryCode("DNK");
        public static CountryCode Dom => new CountryCode("DOM");
        public static CountryCode Dza => new CountryCode("DZA");
        public static CountryCode Ecu => new CountryCode("ECU");
        public static CountryCode Egy => new CountryCode("EGY");
        public static CountryCode Eri => new CountryCode("ERI");
        public static CountryCode Esh => new CountryCode("ESH");
        public static CountryCode Esp => new CountryCode("ESP");
        public static CountryCode Est => new CountryCode("EST");
        public static CountryCode Eth => new CountryCode("ETH");
        public static CountryCode Fin => new CountryCode("FIN");
        public static CountryCode Fji => new CountryCode("FJI");
        public static CountryCode Flk => new CountryCode("FLK");
        public static CountryCode Fra => new CountryCode("FRA");
        public static CountryCode Fro => new CountryCode("FRO");
        public static CountryCode Fsm => new CountryCode("FSM");
        public static CountryCode Gab => new CountryCode("GAB");
        public static CountryCode Gbr => new CountryCode("GBR");
        public static CountryCode Geo => new CountryCode("GEO");
        public static CountryCode Ggy => new CountryCode("GGY");
        public static CountryCode Gha => new CountryCode("GHA");
        public static CountryCode Gib => new CountryCode("GIB");
        public static CountryCode Gin => new CountryCode("GIN");
        public static CountryCode Glp => new CountryCode("GLP");
        public static CountryCode Gmb => new CountryCode("GMB");
        public static CountryCode Gnb => new CountryCode("GNB");
        public static CountryCode Gnq => new CountryCode("GNQ");
        public static CountryCode Grc => new CountryCode("GRC");
        public static CountryCode Grd => new CountryCode("GRD");
        public static CountryCode Grl => new CountryCode("GRL");
        public static CountryCode Gtm => new CountryCode("GTM");
        public static CountryCode Guf => new CountryCode("GUF");
        public static CountryCode Gum => new CountryCode("GUM");
        public static CountryCode Guy => new CountryCode("GUY");
        public static CountryCode Hkg => new CountryCode("HKG");
        public static CountryCode Hmd => new CountryCode("HMD");
        public static CountryCode Hnd => new CountryCode("HND");
        public static CountryCode Hrv => new CountryCode("HRV");
        public static CountryCode Hti => new CountryCode("HTI");
        public static CountryCode Hun => new CountryCode("HUN");
        public static CountryCode Idn => new CountryCode("IDN");
        public static CountryCode Imn => new CountryCode("IMN");
        public static CountryCode Ind => new CountryCode("IND");
        public static CountryCode Iot => new CountryCode("IOT");
        public static CountryCode Irl => new CountryCode("IRL");
        public static CountryCode Irn => new CountryCode("IRN");
        public static CountryCode Irq => new CountryCode("IRQ");
        public static CountryCode Isl => new CountryCode("ISL");
        public static CountryCode Isr => new CountryCode("ISR");
        public static CountryCode Ita => new CountryCode("ITA");
        public static CountryCode Jam => new CountryCode("JAM");
        public static CountryCode Jey => new CountryCode("JEY");
        public static CountryCode Jor => new CountryCode("JOR");
        public static CountryCode Jpn => new CountryCode("JPN");
        public static CountryCode Kaz => new CountryCode("KAZ");
        public static CountryCode Ken => new CountryCode("KEN");
        public static CountryCode Kgz => new CountryCode("KGZ");
        public static CountryCode Khm => new CountryCode("KHM");
        public static CountryCode Kir => new CountryCode("KIR");
        public static CountryCode Kna => new CountryCode("KNA");
        public static CountryCode Kor => new CountryCode("KOR");
        public static CountryCode Kwt => new CountryCode("KWT");
        public static CountryCode Lao => new CountryCode("LAO");
        public static CountryCode Lbn => new CountryCode("LBN");
        public static CountryCode Lbr => new CountryCode("LBR");
        public static CountryCode Lby => new CountryCode("LBY");
        public static CountryCode Lca => new CountryCode("LCA");
        public static CountryCode Lie => new CountryCode("LIE");
        public static CountryCode Lka => new CountryCode("LKA");
        public static CountryCode Lso => new CountryCode("LSO");
        public static CountryCode Ltu => new CountryCode("LTU");
        public static CountryCode Lux => new CountryCode("LUX");
        public static CountryCode Lva => new CountryCode("LVA");
        public static CountryCode Mac => new CountryCode("MAC");
        public static CountryCode Maf => new CountryCode("MAF");
        public static CountryCode Mar => new CountryCode("MAR");
        public static CountryCode Mco => new CountryCode("MCO");
        public static CountryCode Mda => new CountryCode("MDA");
        public static CountryCode Mdg => new CountryCode("MDG");
        public static CountryCode Mdv => new CountryCode("MDV");
        public static CountryCode Mex => new CountryCode("MEX");
        public static CountryCode Mhl => new CountryCode("MHL");
        public static CountryCode Mkd => new CountryCode("MKD");
        public static CountryCode Mli => new CountryCode("MLI");
        public static CountryCode Mlt => new CountryCode("MLT");
        public static CountryCode Mmr => new CountryCode("MMR");
        public static CountryCode Mne => new CountryCode("MNE");
        public static CountryCode Mng => new CountryCode("MNG");
        public static CountryCode Mnp => new CountryCode("MNP");
        public static CountryCode Moz => new CountryCode("MOZ");
        public static CountryCode Mrt => new CountryCode("MRT");
        public static CountryCode Msr => new CountryCode("MSR");
        public static CountryCode Mtq => new CountryCode("MTQ");
        public static CountryCode Mus => new CountryCode("MUS");
        public static CountryCode Mwi => new CountryCode("MWI");
        public static CountryCode Mys => new CountryCode("MYS");
        public static CountryCode Myt => new CountryCode("MYT");
        public static CountryCode Nam => new CountryCode("NAM");
        public static CountryCode Ncl => new CountryCode("NCL");
        public static CountryCode Ner => new CountryCode("NER");
        public static CountryCode Nfk => new CountryCode("NFK");
        public static CountryCode Nga => new CountryCode("NGA");
        public static CountryCode Nic => new CountryCode("NIC");
        public static CountryCode Niu => new CountryCode("NIU");
        public static CountryCode Nld => new CountryCode("NLD");
        public static CountryCode Nor => new CountryCode("NOR");
        public static CountryCode Npl => new CountryCode("NPL");
        public static CountryCode Nru => new CountryCode("NRU");
        public static CountryCode Nzl => new CountryCode("NZL");
        public static CountryCode Omn => new CountryCode("OMN");
        public static CountryCode Pak => new CountryCode("PAK");
        public static CountryCode Pan => new CountryCode("PAN");
        public static CountryCode Pcn => new CountryCode("PCN");
        public static CountryCode Per => new CountryCode("PER");
        public static CountryCode Phl => new CountryCode("PHL");
        public static CountryCode Plw => new CountryCode("PLW");
        public static CountryCode Png => new CountryCode("PNG");
        public static CountryCode Pol => new CountryCode("POL");
        public static CountryCode Pri => new CountryCode("PRI");
        public static CountryCode Prk => new CountryCode("PRK");
        public static CountryCode Prt => new CountryCode("PRT");
        public static CountryCode Pry => new CountryCode("PRY");
        public static CountryCode Pse => new CountryCode("PSE");
        public static CountryCode Pyf => new CountryCode("PYF");
        public static CountryCode Qat => new CountryCode("QAT");
        public static CountryCode Reu => new CountryCode("REU");
        public static CountryCode Rou => new CountryCode("ROU");
        public static CountryCode Rus => new CountryCode("RUS");
        public static CountryCode Rwa => new CountryCode("RWA");
        public static CountryCode Sau => new CountryCode("SAU");
        public static CountryCode Sdn => new CountryCode("SDN");
        public static CountryCode Sen => new CountryCode("SEN");
        public static CountryCode Sgp => new CountryCode("SGP");
        public static CountryCode Sgs => new CountryCode("SGS");
        public static CountryCode Shn => new CountryCode("SHN");
        public static CountryCode Sjm => new CountryCode("SJM");
        public static CountryCode Slb => new CountryCode("SLB");
        public static CountryCode Sle => new CountryCode("SLE");
        public static CountryCode Slv => new CountryCode("SLV");
        public static CountryCode Smr => new CountryCode("SMR");
        public static CountryCode Som => new CountryCode("SOM");
        public static CountryCode Spm => new CountryCode("SPM");
        public static CountryCode Srb => new CountryCode("SRB");
        public static CountryCode Stp => new CountryCode("STP");
        public static CountryCode Sur => new CountryCode("SUR");
        public static CountryCode Svk => new CountryCode("SVK");
        public static CountryCode Svn => new CountryCode("SVN");
        public static CountryCode Swe => new CountryCode("SWE");
        public static CountryCode Swz => new CountryCode("SWZ");
        public static CountryCode Syc => new CountryCode("SYC");
        public static CountryCode Syr => new CountryCode("SYR");
        public static CountryCode Tca => new CountryCode("TCA");
        public static CountryCode Tcd => new CountryCode("TCD");
        public static CountryCode Tgo => new CountryCode("TGO");
        public static CountryCode Tha => new CountryCode("THA");
        public static CountryCode Tjk => new CountryCode("TJK");
        public static CountryCode Tkl => new CountryCode("TKL");
        public static CountryCode Tkm => new CountryCode("TKM");
        public static CountryCode Tls => new CountryCode("TLS");
        public static CountryCode Ton => new CountryCode("TON");
        public static CountryCode Tto => new CountryCode("TTO");
        public static CountryCode Tun => new CountryCode("TUN");
        public static CountryCode Tur => new CountryCode("TUR");
        public static CountryCode Tuv => new CountryCode("TUV");
        public static CountryCode Twn => new CountryCode("TWN");
        public static CountryCode Tza => new CountryCode("TZA");
        public static CountryCode Uga => new CountryCode("UGA");
        public static CountryCode Ukr => new CountryCode("UKR");
        public static CountryCode Umi => new CountryCode("UMI");
        public static CountryCode Ury => new CountryCode("URY");
        public static CountryCode Usa => new CountryCode("USA");
        public static CountryCode Uzb => new CountryCode("UZB");
        public static CountryCode Vat => new CountryCode("VAT");
        public static CountryCode Vct => new CountryCode("VCT");
        public static CountryCode Ven => new CountryCode("VEN");
        public static CountryCode Vgb => new CountryCode("VGB");
        public static CountryCode Vir => new CountryCode("VIR");
        public static CountryCode Vnm => new CountryCode("VNM");
        public static CountryCode Vut => new CountryCode("VUT");
        public static CountryCode Wlf => new CountryCode("WLF");
        public static CountryCode Wsm => new CountryCode("WSM");
        public static CountryCode Yem => new CountryCode("YEM");
        public static CountryCode Zaf => new CountryCode("ZAF");
        public static CountryCode Zmb => new CountryCode("ZMB");
        public static CountryCode Zwe => new CountryCode("ZWE");

        public static IReadOnlyCollection<CountryCode> GetAll() => new List<CountryCode>
        {
            Abw,
            Afg,
            Ago,
            Aia,
            Ala,
            Alb,
            And,
            Ant,
            Are,
            Arg,
            Arm,
            Asm,
            Ata,
            Atf,
            Atg,
            Aus,
            Aut,
            Aze,
            Bdi,
            Bel,
            Ben,
            Bfa,
            Bgd,
            Bgr,
            Bhr,
            Bhs,
            Bih,
            Blm,
            Blr,
            Blz,
            Bmu,
            Bol,
            Bra,
            Brb,
            Brn,
            Btn,
            Bvt,
            Bwa,
            Caf,
            Can,
            Cck,
            Che,
            Chl,
            Chn,
            Civ,
            Cmr,
            Cod,
            Cog,
            Cok,
            Col,
            Com,
            Cpv,
            Cri,
            Cub,
            Cxr,
            Cym,
            Cyp,
            Cze,
            Deu,
            Dji,
            Dma,
            Dnk,
            Dom,
            Dza,
            Ecu,
            Egy,
            Eri,
            Esh,
            Esp,
            Est,
            Eth,
            Fin,
            Fji,
            Flk,
            Fra,
            Fro,
            Fsm,
            Gab,
            Gbr,
            Geo,
            Ggy,
            Gha,
            Gib,
            Gin,
            Glp,
            Gmb,
            Gnb,
            Gnq,
            Grc,
            Grd,
            Grl,
            Gtm,
            Guf,
            Gum,
            Guy,
            Hkg,
            Hmd,
            Hnd,
            Hrv,
            Hti,
            Hun,
            Idn,
            Imn,
            Ind,
            Iot,
            Irl,
            Irn,
            Irq,
            Isl,
            Isr,
            Ita,
            Jam,
            Jey,
            Jor,
            Jpn,
            Kaz,
            Ken,
            Kgz,
            Khm,
            Kir,
            Kna,
            Kor,
            Kwt,
            Lao,
            Lbn,
            Lbr,
            Lby,
            Lca,
            Lie,
            Lka,
            Lso,
            Ltu,
            Lux,
            Lva,
            Mac,
            Maf,
            Mar,
            Mco,
            Mda,
            Mdg,
            Mdv,
            Mex,
            Mhl,
            Mkd,
            Mli,
            Mlt,
            Mmr,
            Mne,
            Mng,
            Mnp,
            Moz,
            Mrt,
            Msr,
            Mtq,
            Mus,
            Mwi,
            Mys,
            Myt,
            Nam,
            Ncl,
            Ner,
            Nfk,
            Nga,
            Nic,
            Niu,
            Nld,
            Nor,
            Npl,
            Nru,
            Nzl,
            Omn,
            Pak,
            Pan,
            Pcn,
            Per,
            Phl,
            Plw,
            Png,
            Pol,
            Pri,
            Prk,
            Prt,
            Pry,
            Pse,
            Pyf,
            Qat,
            Reu,
            Rou,
            Rus,
            Rwa,
            Sau,
            Sdn,
            Sen,
            Sgp,
            Sgs,
            Shn,
            Sjm,
            Slb,
            Sle,
            Slv,
            Smr,
            Som,
            Spm,
            Srb,
            Stp,
            Sur,
            Svk,
            Svn,
            Swe,
            Swz,
            Syc,
            Syr,
            Tca,
            Tcd,
            Tgo,
            Tha,
            Tjk,
            Tkl,
            Tkm,
            Tls,
            Ton,
            Tto,
            Tun,
            Tur,
            Tuv,
            Twn,
            Tza,
            Uga,
            Ukr,
            Umi,
            Ury,
            Usa,
            Uzb,
            Vat,
            Vct,
            Ven,
            Vgb,
            Vir,
            Vnm,
            Vut,
            Wlf,
            Wsm,
            Yem,
            Zaf,
            Zmb,
            Zwe
        };

        public static explicit operator CountryCode(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new CountryCode(instance);
        }

        public static implicit operator string(CountryCode instance)
        {
            return instance?._value;
        }

        public static bool operator ==(CountryCode x, CountryCode y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(CountryCode x, CountryCode y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as CountryCode);
        }

        public bool Equals(CountryCode value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}