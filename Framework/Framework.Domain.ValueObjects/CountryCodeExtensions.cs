﻿namespace Framework.Domain.ValueObjects
{
    public static class CountryCodeExtensions
    {
        public static CountryName ToCountryName(this CountryCode countryCode)
        {
            var converter = new CountryCodeToNameConverter();
            return converter.Convert(countryCode);
        }
    }
}