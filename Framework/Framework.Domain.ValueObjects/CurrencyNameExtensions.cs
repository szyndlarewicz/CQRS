﻿namespace Framework.Domain.ValueObjects
{
    public static class CurrencyNameExtensions
    {
        public static CurrencyCode ToCurrencyCode(this CurrencyName currencyName)
        {
            var converter = new CurrencyNameToCodeConverter();
            return converter.Convert(currencyName);
        }
    }
}