﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Framework.Domain.ValueObjects
{
    [SuppressMessage("Microsoft.StyleCop.CSharp.LayoutRules", "SA1516:ElementsMustBeSeparatedByBlankLine", Justification = "Too many similar properties")]
    public sealed class CurrencyName : IValueObject
    {
        private static readonly string[] Currencies =
        {
            "AFN",
            "EUR",
            "ALL",
            "DZD",
            "USD",
            "AOA",
            "XCD",
            "ARS",
            "AMD",
            "AWG",
            "AUD",
            "AZN",
            "BSD",
            "BHD",
            "BDT",
            "BBD",
            "BYN",
            "BZD",
            "XOF",
            "BMD",
            "INR",
            "BTN",
            "BOB",
            "BOV",
            "BAM",
            "BWP",
            "NOK",
            "BRL",
            "BND",
            "BGN",
            "BIF",
            "CVE",
            "KHR",
            "XAF",
            "CAD",
            "KYD",
            "CLP",
            "CLF",
            "CNY",
            "COP",
            "COU",
            "KMF",
            "CDF",
            "NZD",
            "CRC",
            "HRK",
            "CUP",
            "CUC",
            "ANG",
            "CZK",
            "DKK",
            "DJF",
            "DOP",
            "EGP",
            "SVC",
            "ERN",
            "ETB",
            "FKP",
            "FJD",
            "XPF",
            "GMD",
            "GEL",
            "GHS",
            "GIP",
            "GTQ",
            "GBP",
            "GNF",
            "GYD",
            "HTG",
            "HNL",
            "HKD",
            "HUF",
            "ISK",
            "XDR",
            "IRR",
            "IQD",
            "ILS",
            "JMD",
            "JPY",
            "JOD",
            "KZT",
            "KES",
            "KPW",
            "KRW",
            "KWD",
            "KGS",
            "LAK",
            "LBP",
            "LSL",
            "ZAR",
            "LRD",
            "LYD",
            "CHF",
            "MOP",
            "MKD",
            "MGA",
            "MWK",
            "MYR",
            "MVR",
            "MRO",
            "MUR",
            "XUA",
            "MXN",
            "MXV",
            "MDL",
            "MNT",
            "MAD",
            "MZN",
            "MMK",
            "NAD",
            "NPR",
            "NIO",
            "NGN",
            "OMR",
            "PKR",
            "PAB",
            "PGK",
            "PYG",
            "PEN",
            "PHP",
            "PLN",
            "QAR",
            "RON",
            "RUB",
            "RWF",
            "SHP",
            "WST",
            "STD",
            "SAR",
            "RSD",
            "SCR",
            "SLL",
            "SGD",
            "XSU",
            "SBD",
            "SOS",
            "SSP",
            "LKR",
            "SDG",
            "SRD",
            "SZL",
            "SEK",
            "CHW",
            "SYP",
            "TWD",
            "TJS",
            "TZS",
            "THB",
            "TOP",
            "TTD",
            "TND",
            "TRY",
            "TMT",
            "UGX",
            "UAH",
            "AED",
            "USN",
            "UYU",
            "UYI",
            "UZS",
            "VUV",
            "VEF",
            "VND",
            "YER",
            "ZMW",
            "ZWL",
            "XBA",
            "XBB",
            "XBC",
            "XBD",
            "XTS"
        };

        private readonly string _value;

        public CurrencyName(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!Currencies.Contains(value))
                throw new ArgumentException("Unrecognized currency name");

            _value = value;
        }

        public static CurrencyName Afghani => new CurrencyName("Afghani");
        public static CurrencyName Euro => new CurrencyName("Euro");
        public static CurrencyName Lek => new CurrencyName("Lek");
        public static CurrencyName AlgerianDinar => new CurrencyName("Algerian Dinar");
        public static CurrencyName UsDollar => new CurrencyName("US Dollar");
        public static CurrencyName Kwanza => new CurrencyName("Kwanza");
        public static CurrencyName EastCaribbeanDollar => new CurrencyName("East Caribbean Dollar");
        public static CurrencyName ArgentinePeso => new CurrencyName("Argentine Peso");
        public static CurrencyName ArmenianDram => new CurrencyName("Armenian Dram");
        public static CurrencyName ArubanFlorin => new CurrencyName("Aruban Florin");
        public static CurrencyName AustralianDollar => new CurrencyName("Australian Dollar");
        public static CurrencyName AzerbaijanManat => new CurrencyName("Azerbaijan Manat");
        public static CurrencyName BahamianDollar => new CurrencyName("Bahamian Dollar");
        public static CurrencyName BahrainiDinar => new CurrencyName("Bahraini Dinar");
        public static CurrencyName Taka => new CurrencyName("Taka");
        public static CurrencyName BarbadosDollar => new CurrencyName("Barbados Dollar");
        public static CurrencyName BelarusianRuble => new CurrencyName("Belarusian Ruble");
        public static CurrencyName BelizeDollar => new CurrencyName("Belize Dollar");
        public static CurrencyName CfaFrancBceao => new CurrencyName("CFA Franc BCEAO");
        public static CurrencyName BermudianDollar => new CurrencyName("Bermudian Dollar");
        public static CurrencyName IndianRupee => new CurrencyName("Indian Rupee");
        public static CurrencyName Ngultrum => new CurrencyName("Ngultrum");
        public static CurrencyName Boliviano => new CurrencyName("Boliviano");
        public static CurrencyName Mvdol => new CurrencyName("Mvdol");
        public static CurrencyName ConvertibleMark => new CurrencyName("Convertible Mark");
        public static CurrencyName Pula => new CurrencyName("Pula");
        public static CurrencyName NorwegianKrone => new CurrencyName("Norwegian Krone");
        public static CurrencyName BrazilianReal => new CurrencyName("Brazilian Real");
        public static CurrencyName BruneiDollar => new CurrencyName("Brunei Dollar");
        public static CurrencyName BulgarianLev => new CurrencyName("Bulgarian Lev");
        public static CurrencyName BurundiFranc => new CurrencyName("Burundi Franc");
        public static CurrencyName CaboVerdeEscudo => new CurrencyName("Cabo Verde Escudo");
        public static CurrencyName Riel => new CurrencyName("Riel");
        public static CurrencyName CfaFrancBeac => new CurrencyName("CFA Franc BEAC");
        public static CurrencyName CanadianDollar => new CurrencyName("Canadian Dollar");
        public static CurrencyName CaymanIslandsDollar => new CurrencyName("Cayman Islands Dollar");
        public static CurrencyName ChileanPeso => new CurrencyName("Chilean Peso");
        public static CurrencyName UnidadDeFomento => new CurrencyName("Unidad de Fomento");
        public static CurrencyName YuanRenminbi => new CurrencyName("Yuan Renminbi");
        public static CurrencyName ColombianPeso => new CurrencyName("Colombian Peso");
        public static CurrencyName UnidadDeValorReal => new CurrencyName("Unidad de Valor Real");
        public static CurrencyName ComorianFranc => new CurrencyName("Comorian Franc");
        public static CurrencyName CongoleseFranc => new CurrencyName("Congolese Franc");
        public static CurrencyName NewZealandDollar => new CurrencyName("New Zealand Dollar");
        public static CurrencyName CostaRicanColon => new CurrencyName("Costa Rican Colon");
        public static CurrencyName Kuna => new CurrencyName("Kuna");
        public static CurrencyName CubanPeso => new CurrencyName("Cuban Peso");
        public static CurrencyName PesoConvertible => new CurrencyName("Peso Convertible");
        public static CurrencyName NetherlandsAntilleanGuilder => new CurrencyName("Netherlands Antillean Guilder");
        public static CurrencyName CzechKoruna => new CurrencyName("Czech Koruna");
        public static CurrencyName DanishKrone => new CurrencyName("Danish Krone");
        public static CurrencyName DjiboutiFranc => new CurrencyName("Djibouti Franc");
        public static CurrencyName DominicanPeso => new CurrencyName("Dominican Peso");
        public static CurrencyName EgyptianPound => new CurrencyName("Egyptian Pound");
        public static CurrencyName ElSalvadorColon => new CurrencyName("El Salvador Colon");
        public static CurrencyName Nakfa => new CurrencyName("Nakfa");
        public static CurrencyName EthiopianBirr => new CurrencyName("Ethiopian Birr");
        public static CurrencyName FalklandIslandsPound => new CurrencyName("Falkland Islands Pound");
        public static CurrencyName FijiDollar => new CurrencyName("Fiji Dollar");
        public static CurrencyName CfpFranc => new CurrencyName("CFP Franc");
        public static CurrencyName Dalasi => new CurrencyName("Dalasi");
        public static CurrencyName Lari => new CurrencyName("Lari");
        public static CurrencyName GhanaCedi => new CurrencyName("Ghana Cedi");
        public static CurrencyName GibraltarPound => new CurrencyName("Gibraltar Pound");
        public static CurrencyName Quetzal => new CurrencyName("Quetzal");
        public static CurrencyName PoundSterling => new CurrencyName("Pound Sterling");
        public static CurrencyName GuineanFranc => new CurrencyName("Guinean Franc");
        public static CurrencyName GuyanaDollar => new CurrencyName("Guyana Dollar");
        public static CurrencyName Gourde => new CurrencyName("Gourde");
        public static CurrencyName Lempira => new CurrencyName("Lempira");
        public static CurrencyName HongKongDollar => new CurrencyName("Hong Kong Dollar");
        public static CurrencyName Forint => new CurrencyName("Forint");
        public static CurrencyName IcelandKrona => new CurrencyName("Iceland Krona");
        public static CurrencyName SpecialDrawingRight => new CurrencyName("SDR (Special Drawing Right)");
        public static CurrencyName IranianRial => new CurrencyName("Iranian Rial");
        public static CurrencyName IraqiDinar => new CurrencyName("Iraqi Dinar");
        public static CurrencyName NewIsraeliSheqel => new CurrencyName("New Israeli Sheqel");
        public static CurrencyName JamaicanDollar => new CurrencyName("Jamaican Dollar");
        public static CurrencyName Yen => new CurrencyName("Yen");
        public static CurrencyName JordanianDinar => new CurrencyName("Jordanian Dinar");
        public static CurrencyName Tenge => new CurrencyName("Tenge");
        public static CurrencyName KenyanShilling => new CurrencyName("Kenyan Shilling");
        public static CurrencyName NorthKoreanWon => new CurrencyName("North Korean Won");
        public static CurrencyName Won => new CurrencyName("Won");
        public static CurrencyName KuwaitiDinar => new CurrencyName("Kuwaiti Dinar");
        public static CurrencyName Som => new CurrencyName("Som");
        public static CurrencyName LaoKip => new CurrencyName("Lao Kip");
        public static CurrencyName LebanesePound => new CurrencyName("Lebanese Pound");
        public static CurrencyName Loti => new CurrencyName("Loti");
        public static CurrencyName Rand => new CurrencyName("Rand");
        public static CurrencyName LiberianDollar => new CurrencyName("Liberian Dollar");
        public static CurrencyName LibyanDinar => new CurrencyName("Libyan Dinar");
        public static CurrencyName SwissFranc => new CurrencyName("Swiss Franc");
        public static CurrencyName Pataca => new CurrencyName("Pataca");
        public static CurrencyName Denar => new CurrencyName("Denar");
        public static CurrencyName MalagasyAriary => new CurrencyName("Malagasy Ariary");
        public static CurrencyName MalawiKwacha => new CurrencyName("Malawi Kwacha");
        public static CurrencyName MalaysianRinggit => new CurrencyName("Malaysian Ringgit");
        public static CurrencyName Rufiyaa => new CurrencyName("Rufiyaa");
        public static CurrencyName Ouguiya => new CurrencyName("Ouguiya");
        public static CurrencyName MauritiusRupee => new CurrencyName("Mauritius Rupee");
        public static CurrencyName AdbUnitOfAccount => new CurrencyName("ADB Unit of Account");
        public static CurrencyName MexicanPeso => new CurrencyName("Mexican Peso");
        public static CurrencyName MexicanUnidadDeInversion => new CurrencyName("Mexican Unidad de Inversion (UDI)");
        public static CurrencyName MoldovanLeu => new CurrencyName("Moldovan Leu");
        public static CurrencyName Tugrik => new CurrencyName("Tugrik");
        public static CurrencyName MoroccanDirham => new CurrencyName("Moroccan Dirham");
        public static CurrencyName MozambiqueMetical => new CurrencyName("Mozambique Metical");
        public static CurrencyName Kyat => new CurrencyName("Kyat");
        public static CurrencyName NamibiaDollar => new CurrencyName("Namibia Dollar");
        public static CurrencyName NepaleseRupee => new CurrencyName("Nepalese Rupee");
        public static CurrencyName CordobaOro => new CurrencyName("Cordoba Oro");
        public static CurrencyName Naira => new CurrencyName("Naira");
        public static CurrencyName RialOmani => new CurrencyName("Rial Omani");
        public static CurrencyName PakistanRupee => new CurrencyName("Pakistan Rupee");
        public static CurrencyName Balboa => new CurrencyName("Balboa");
        public static CurrencyName Kina => new CurrencyName("Kina");
        public static CurrencyName Guarani => new CurrencyName("Guarani");
        public static CurrencyName Sol => new CurrencyName("Sol");
        public static CurrencyName PhilippinePeso => new CurrencyName("Philippine Peso");
        public static CurrencyName Zloty => new CurrencyName("Zloty");
        public static CurrencyName QatariRial => new CurrencyName("Qatari Rial");
        public static CurrencyName RomanianLeu => new CurrencyName("Romanian Leu");
        public static CurrencyName RussianRuble => new CurrencyName("Russian Ruble");
        public static CurrencyName RwandaFranc => new CurrencyName("Rwanda Franc");
        public static CurrencyName SaintHelenaPound => new CurrencyName("Saint Helena Pound");
        public static CurrencyName Tala => new CurrencyName("Tala");
        public static CurrencyName Dobra => new CurrencyName("Dobra");
        public static CurrencyName SaudiRiyal => new CurrencyName("Saudi Riyal");
        public static CurrencyName SerbianDinar => new CurrencyName("Serbian Dinar");
        public static CurrencyName SeychellesRupee => new CurrencyName("Seychelles Rupee");
        public static CurrencyName Leone => new CurrencyName("Leone");
        public static CurrencyName SingaporeDollar => new CurrencyName("Singapore Dollar");
        public static CurrencyName Sucre => new CurrencyName("Sucre");
        public static CurrencyName SolomonIslandsDollar => new CurrencyName("Solomon Islands Dollar");
        public static CurrencyName SomaliShilling => new CurrencyName("Somali Shilling");
        public static CurrencyName SouthSudanesePound => new CurrencyName("South Sudanese Pound");
        public static CurrencyName SriLankaRupee => new CurrencyName("Sri Lanka Rupee");
        public static CurrencyName SudanesePound => new CurrencyName("Sudanese Pound");
        public static CurrencyName SurinamDollar => new CurrencyName("Surinam Dollar");
        public static CurrencyName Lilangeni => new CurrencyName("Lilangeni");
        public static CurrencyName SwedishKrona => new CurrencyName("Swedish Krona");
        public static CurrencyName WirFranc => new CurrencyName("WIR Franc");
        public static CurrencyName SyrianPound => new CurrencyName("Syrian Pound");
        public static CurrencyName NewTaiwanDollar => new CurrencyName("New Taiwan Dollar");
        public static CurrencyName Somoni => new CurrencyName("Somoni");
        public static CurrencyName TanzanianShilling => new CurrencyName("Tanzanian Shilling");
        public static CurrencyName Baht => new CurrencyName("Baht");
        public static CurrencyName Paanga => new CurrencyName("Pa’anga");
        public static CurrencyName TrinidadAndTobagoDolla => new CurrencyName("Trinidad and Tobago Dolla");
        public static CurrencyName TunisianDinar => new CurrencyName("Tunisian Dinar");
        public static CurrencyName TurkishLira => new CurrencyName("Turkish Lira");
        public static CurrencyName TurkmenistanNewManat => new CurrencyName("Turkmenistan New Manat");
        public static CurrencyName UgandaShilling => new CurrencyName("Uganda Shilling");
        public static CurrencyName Hryvnia => new CurrencyName("Hryvnia");
        public static CurrencyName UaeDirham => new CurrencyName("UAE Dirham");
        public static CurrencyName UsDollarNextDay => new CurrencyName("US Dollar (Next day)");
        public static CurrencyName PesoUruguayo => new CurrencyName("Peso Uruguayo");
        public static CurrencyName UruguayPesoEnUnidadesIndexadas => new CurrencyName("Uruguay Peso en Unidades Indexadas (URUIURUI)");
        public static CurrencyName UzbekistanSum => new CurrencyName("Uzbekistan Sum");
        public static CurrencyName Vatu => new CurrencyName("Vatu");
        public static CurrencyName Bolivar => new CurrencyName("Bolívar");
        public static CurrencyName Dong => new CurrencyName("Dong");
        public static CurrencyName YemeniRial => new CurrencyName("Yemeni Rial");
        public static CurrencyName ZambianKwacha => new CurrencyName("Zambian Kwacha");
        public static CurrencyName ZimbabweDollar => new CurrencyName("Zimbabwe Dollar");

        public static IReadOnlyCollection<CurrencyName> GetAll() => new List<CurrencyName>
        {
            Afghani,
            Euro,
            Lek,
            AlgerianDinar,
            UsDollar,
            Kwanza,
            EastCaribbeanDollar,
            ArgentinePeso,
            ArmenianDram,
            ArubanFlorin,
            AustralianDollar,
            AzerbaijanManat,
            BahamianDollar,
            BahrainiDinar,
            Taka,
            BarbadosDollar,
            BelarusianRuble,
            BelizeDollar,
            CfaFrancBceao,
            BermudianDollar,
            IndianRupee,
            Ngultrum,
            Boliviano,
            Mvdol,
            ConvertibleMark,
            Pula,
            NorwegianKrone,
            BrazilianReal,
            BruneiDollar,
            BulgarianLev,
            BurundiFranc,
            CaboVerdeEscudo,
            Riel,
            CfaFrancBeac,
            CanadianDollar,
            CaymanIslandsDollar,
            ChileanPeso,
            UnidadDeFomento,
            YuanRenminbi,
            ColombianPeso,
            UnidadDeValorReal,
            ComorianFranc,
            CongoleseFranc,
            NewZealandDollar,
            CostaRicanColon,
            Kuna,
            CubanPeso,
            PesoConvertible,
            NetherlandsAntilleanGuilder,
            CzechKoruna,
            DanishKrone,
            DjiboutiFranc,
            DominicanPeso,
            EgyptianPound,
            ElSalvadorColon,
            Nakfa,
            EthiopianBirr,
            FalklandIslandsPound,
            FijiDollar,
            CfpFranc,
            Dalasi,
            Lari,
            GhanaCedi,
            GibraltarPound,
            Quetzal,
            PoundSterling,
            GuineanFranc,
            GuyanaDollar,
            Gourde,
            Lempira,
            HongKongDollar,
            Forint,
            IcelandKrona,
            SpecialDrawingRight,
            IranianRial,
            IraqiDinar,
            NewIsraeliSheqel,
            JamaicanDollar,
            Yen,
            JordanianDinar,
            Tenge,
            KenyanShilling,
            NorthKoreanWon,
            Won,
            KuwaitiDinar,
            Som,
            LaoKip,
            LebanesePound,
            Loti,
            Rand,
            LiberianDollar,
            LibyanDinar,
            SwissFranc,
            Pataca,
            Denar,
            MalagasyAriary,
            MalawiKwacha,
            MalaysianRinggit,
            Rufiyaa,
            Ouguiya,
            MauritiusRupee,
            AdbUnitOfAccount,
            MexicanPeso,
            MexicanUnidadDeInversion,
            MoldovanLeu,
            Tugrik,
            MoroccanDirham,
            MozambiqueMetical,
            Kyat,
            NamibiaDollar,
            NepaleseRupee,
            CordobaOro,
            Naira,
            RialOmani,
            PakistanRupee,
            Balboa,
            Kina,
            Guarani,
            Sol,
            PhilippinePeso,
            Zloty,
            QatariRial,
            RomanianLeu,
            RussianRuble,
            RwandaFranc,
            SaintHelenaPound,
            Tala,
            Dobra,
            SaudiRiyal,
            SerbianDinar,
            SeychellesRupee,
            Leone,
            SingaporeDollar,
            Sucre,
            SolomonIslandsDollar,
            SomaliShilling,
            SouthSudanesePound,
            SriLankaRupee,
            SudanesePound,
            SurinamDollar,
            Lilangeni,
            SwedishKrona,
            WirFranc,
            SyrianPound,
            NewTaiwanDollar,
            Somoni,
            TanzanianShilling,
            Baht,
            Paanga,
            TrinidadAndTobagoDolla,
            TunisianDinar,
            TurkishLira,
            TurkmenistanNewManat,
            UgandaShilling,
            Hryvnia,
            UaeDirham,
            UsDollarNextDay,
            PesoUruguayo,
            UruguayPesoEnUnidadesIndexadas,
            UzbekistanSum,
            Vatu,
            Bolivar,
            Dong,
            YemeniRial,
            ZambianKwacha,
            ZimbabweDollar
        };

        public static explicit operator CurrencyName(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new CurrencyName(instance);
        }

        public static implicit operator string(CurrencyName instance)
        {
            return instance?._value;
        }

        public static bool operator ==(CurrencyName x, CurrencyName y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(CurrencyName x, CurrencyName y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as CurrencyName);
        }

        public bool Equals(CurrencyName value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}