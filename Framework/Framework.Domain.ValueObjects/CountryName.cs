﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Framework.Domain.ValueObjects
{
    [SuppressMessage("Microsoft.StyleCop.CSharp.LayoutRules", "SA1516:ElementsMustBeSeparatedByBlankLine", Justification = "Too many similar properties")]
    public sealed class CountryName : IValueObject, IEquatable<CountryName>
    {
        private static readonly string[] Countries =
        {
            "Aruba",
            "Afghanistan",
            "Angola",
            "Anguilla",
            "Åland Islands",
            "Albania",
            "Andorra",
            "Netherlands Antilles",
            "United Arab Emirates",
            "Argentina",
            "Armenia",
            "American Samoa",
            "Antarctica",
            "French Southern Territories",
            "Antigua and Barbuda",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Burundi",
            "Belgium",
            "Benin",
            "Burkina Faso",
            "Bangladesh",
            "Bulgaria",
            "Bahrain",
            "Bahamas",
            "Bosnia and Herzegovina",
            "Saint Barthélemy",
            "Belarus",
            "Belize",
            "Bermuda",
            "Bolivia, Plurinational State of",
            "Brazil",
            "Barbados",
            "Brunei Darussalam",
            "Bhutan",
            "Bouvet Island",
            "Botswana",
            "Central African Republic",
            "Canada",
            "Cocos (Keeling) Islands",
            "Switzerland",
            "Chile",
            "China",
            "Côte d'Ivoire",
            "Cameroon",
            "Congo, the Democratic Republic of the",
            "Congo",
            "Cook Islands",
            "Colombia",
            "Comoros",
            "Cape Verde",
            "Costa Rica",
            "Cuba",
            "Christmas Island",
            "Cayman Islands",
            "Cyprus",
            "Czech Republic",
            "Germany",
            "Djibouti",
            "Dominica",
            "Denmark",
            "Dominican Republic",
            "Algeria",
            "Ecuador",
            "Egypt",
            "Eritrea",
            "Western Sahara",
            "Spain",
            "Estonia",
            "Ethiopia",
            "Finland",
            "Fiji",
            "Falkland Islands (Malvinas)",
            "France",
            "Faroe Islands",
            "Micronesia, Federated States of",
            "Gabon",
            "United Kingdom",
            "Georgia",
            "Guernsey",
            "Ghana",
            "Gibraltar",
            "Guinea",
            "Guadeloupe",
            "Gambia",
            "Guinea-Bissau",
            "Equatorial Guinea",
            "Greece",
            "Grenada",
            "Greenland",
            "Guatemala",
            "French Guiana",
            "Guam",
            "Guyana",
            "Hong Kong",
            "Heard Island and McDonald Islands",
            "Honduras",
            "Croatia",
            "Haiti",
            "Hungary",
            "Indonesia",
            "Isle of Man",
            "India",
            "British Indian Ocean Territory",
            "Ireland",
            "Iran, Islamic Republic of",
            "Iraq",
            "Iceland",
            "Israel",
            "Italy",
            "Jamaica",
            "Jersey",
            "Jordan",
            "Japan",
            "Kazakhstan",
            "Kenya",
            "Kyrgyzstan",
            "Cambodia",
            "Kiribati",
            "Saint Kitts and Nevis",
            "Korea, Republic of",
            "Kuwait",
            "Lao People's Democratic Republic",
            "Lebanon",
            "Liberia",
            "Libyan Arab Jamahiriya",
            "Saint Lucia",
            "Liechtenstein",
            "Sri Lanka",
            "Lesotho",
            "Lithuania",
            "Luxembourg",
            "Latvia",
            "Macao",
            "Saint Martin (French part)",
            "Morocco",
            "Monaco",
            "Moldova, Republic of",
            "Madagascar",
            "Maldives",
            "Mexico",
            "Marshall Islands",
            "Macedonia, the former Yugoslav Republic of",
            "Mali",
            "Malta",
            "Myanmar",
            "Montenegro",
            "Mongolia",
            "Northern Mariana Islands",
            "Mozambique",
            "Mauritania",
            "Montserrat",
            "Martinique",
            "Mauritius",
            "Malawi",
            "Malaysia",
            "Mayotte",
            "Namibia",
            "New Caledonia",
            "Niger",
            "Norfolk Island",
            "Nigeria",
            "Nicaragua",
            "Niue",
            "Netherlands",
            "Norway",
            "Nepal",
            "Nauru",
            "New Zealand",
            "Oman",
            "Pakistan",
            "Panama",
            "Pitcairn",
            "Peru",
            "Philippines",
            "Palau",
            "Papua New Guinea",
            "Poland",
            "Puerto Rico",
            "Korea, Democratic People's Republic of",
            "Portugal",
            "Paraguay",
            "Palestinian Territory, Occupied",
            "French Polynesia",
            "Qatar",
            "Réunion",
            "Romania",
            "Russian Federation",
            "Rwanda",
            "Saudi Arabia",
            "Sudan",
            "Senegal",
            "Singapore",
            "South Georgia and the South Sandwich Islands",
            "Saint Helena, Ascension and Tristan da Cunha",
            "Svalbard and Jan Mayen",
            "Solomon Islands",
            "Sierra Leone",
            "El Salvador",
            "San Marino",
            "Somalia",
            "Saint Pierre and Miquelon",
            "Serbia",
            "Sao Tome and Principe",
            "Suriname",
            "Slovakia",
            "Slovenia",
            "Sweden",
            "Swaziland",
            "Seychelles",
            "Syrian Arab Republic",
            "Turks and Caicos Islands",
            "Chad",
            "Togo",
            "Thailand",
            "Tajikistan",
            "Tokelau",
            "Turkmenistan",
            "Timor-Leste",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turkey",
            "Tuvalu",
            "Taiwan, Province of China",
            "Tanzania, United Republic of",
            "Uganda",
            "Ukraine",
            "United States Minor Outlying Islands",
            "Uruguay",
            "United States",
            "Uzbekistan",
            "Holy See (Vatican City State)",
            "Saint Vincent and the Grenadines",
            "Venezuela, Bolivarian Republic of",
            "Virgin Islands, British",
            "Virgin Islands, U.S.",
            "Vietnam",
            "Vanuatu",
            "Wallis and Futuna",
            "Samoa",
            "Yemen",
            "South Africa",
            "Zambia",
            "Zimbabwe"
        };

        private readonly string _value;

        public CountryName(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!Countries.Contains(value))
                throw new ArgumentException("Unrecognized country name");

            _value = value;
        }

        public static CountryName Aruba => new CountryName("Aruba");
        public static CountryName Afghanistan => new CountryName("Afghanistan");
        public static CountryName Angola => new CountryName("Angola");
        public static CountryName Anguilla => new CountryName("Anguilla");
        public static CountryName AlandIslands => new CountryName("Åland Islands");
        public static CountryName Albania => new CountryName("Albania");
        public static CountryName Andorra => new CountryName("Andorra");
        public static CountryName NetherlandsAntilles => new CountryName("Netherlands Antilles");
        public static CountryName UnitedArabEmirates => new CountryName("United Arab Emirates");
        public static CountryName Argentina => new CountryName("Argentina");
        public static CountryName Armenia => new CountryName("Armenia");
        public static CountryName AmericanSamoa => new CountryName("American Samoa");
        public static CountryName Antarctica => new CountryName("Antarctica");
        public static CountryName FrenchSouthernTerritories => new CountryName("French Southern Territories");
        public static CountryName AntiguaAndBarbuda => new CountryName("Antigua and Barbuda");
        public static CountryName Australia => new CountryName("Australia");
        public static CountryName Austria => new CountryName("Austria");
        public static CountryName Azerbaijan => new CountryName("Azerbaijan");
        public static CountryName Burundi => new CountryName("Burundi");
        public static CountryName Belgium => new CountryName("Belgium");
        public static CountryName Benin => new CountryName("Benin");
        public static CountryName BurkinaFaso => new CountryName("Burkina Faso");
        public static CountryName Bangladesh => new CountryName("Bangladesh");
        public static CountryName Bulgaria => new CountryName("Bulgaria");
        public static CountryName Bahrain => new CountryName("Bahrain");
        public static CountryName Bahamas => new CountryName("Bahamas");
        public static CountryName BosniaAndHerzegovina => new CountryName("Bosnia and Herzegovina");
        public static CountryName SaintBarthelemy => new CountryName("Saint Barthélemy");
        public static CountryName Belarus => new CountryName("Belarus");
        public static CountryName Belize => new CountryName("Belize");
        public static CountryName Bermuda => new CountryName("Bermuda");
        public static CountryName Bolivia => new CountryName("Bolivia, Plurinational State of");
        public static CountryName Brazil => new CountryName("Brazil");
        public static CountryName Barbados => new CountryName("Barbados");
        public static CountryName BruneiDarussalam => new CountryName("Brunei Darussalam");
        public static CountryName Bhutan => new CountryName("Bhutan");
        public static CountryName BouvetIsland => new CountryName("Bouvet Island");
        public static CountryName Botswana => new CountryName("Botswana");
        public static CountryName CentralAfricanRepublic => new CountryName("Central African Republic");
        public static CountryName Canada => new CountryName("Canada");
        public static CountryName CocosIslands => new CountryName("Cocos (Keeling) Islands");
        public static CountryName Switzerland => new CountryName("Switzerland");
        public static CountryName Chile => new CountryName("Chile");
        public static CountryName China => new CountryName("China");
        public static CountryName IvoryCoast => new CountryName("Côte d'Ivoire");
        public static CountryName Cameroon => new CountryName("Cameroon");
        public static CountryName DemocraticRepublicOfTheCongo => new CountryName("Congo, the Democratic Republic of the");
        public static CountryName Congo => new CountryName("Congo");
        public static CountryName CookIslands => new CountryName("Cook Islands");
        public static CountryName Colombia => new CountryName("Colombia");
        public static CountryName Comoros => new CountryName("Comoros");
        public static CountryName CapeVerde => new CountryName("Cape Verde");
        public static CountryName CostaRica => new CountryName("Costa Rica");
        public static CountryName Cuba => new CountryName("Cuba");
        public static CountryName ChristmasIsland => new CountryName("Christmas Island");
        public static CountryName CaymanIslands => new CountryName("Cayman Islands");
        public static CountryName Cyprus => new CountryName("Cyprus");
        public static CountryName CzechRepublic => new CountryName("Czech Republic");
        public static CountryName Germany => new CountryName("Germany");
        public static CountryName Djibouti => new CountryName("Djibouti");
        public static CountryName Dominica => new CountryName("Dominica");
        public static CountryName Denmark => new CountryName("Denmark");
        public static CountryName DominicanRepublic => new CountryName("Dominican Republic");
        public static CountryName Algeria => new CountryName("Algeria");
        public static CountryName Ecuador => new CountryName("Ecuador");
        public static CountryName Egypt => new CountryName("Egypt");
        public static CountryName Eritrea => new CountryName("Eritrea");
        public static CountryName WesternSahara => new CountryName("Western Sahara");
        public static CountryName Spain => new CountryName("Spain");
        public static CountryName Estonia => new CountryName("Estonia");
        public static CountryName Ethiopia => new CountryName("Ethiopia");
        public static CountryName Finland => new CountryName("Finland");
        public static CountryName Fiji => new CountryName("Fiji");
        public static CountryName FalklandIslands => new CountryName("Falkland Islands (Malvinas)");
        public static CountryName France => new CountryName("France");
        public static CountryName FaroeIslands => new CountryName("Faroe Islands");
        public static CountryName FederatedStatesOfMicronesia => new CountryName("Micronesia, Federated States of");
        public static CountryName Gabon => new CountryName("Gabon");
        public static CountryName UnitedKingdom => new CountryName("United Kingdom");
        public static CountryName Georgia => new CountryName("Georgia");
        public static CountryName Guernsey => new CountryName("Guernsey");
        public static CountryName Ghana => new CountryName("Ghana");
        public static CountryName Gibraltar => new CountryName("Gibraltar");
        public static CountryName Guinea => new CountryName("Guinea");
        public static CountryName Guadeloupe => new CountryName("Guadeloupe");
        public static CountryName Gambia => new CountryName("Gambia");
        public static CountryName GuineaBissau => new CountryName("Guinea-Bissau");
        public static CountryName EquatorialGuinea => new CountryName("Equatorial Guinea");
        public static CountryName Greece => new CountryName("Greece");
        public static CountryName Grenada => new CountryName("Grenada");
        public static CountryName Greenland => new CountryName("Greenland");
        public static CountryName Guatemala => new CountryName("Guatemala");
        public static CountryName FrenchGuiana => new CountryName("French Guiana");
        public static CountryName Guam => new CountryName("Guam");
        public static CountryName Guyana => new CountryName("Guyana");
        public static CountryName HongKong => new CountryName("Hong Kong");
        public static CountryName HeardIslandAndMcDonaldIslands => new CountryName("Heard Island and McDonald Islands");
        public static CountryName Honduras => new CountryName("Honduras");
        public static CountryName Croatia => new CountryName("Croatia");
        public static CountryName Haiti => new CountryName("Haiti");
        public static CountryName Hungary => new CountryName("Hungary");
        public static CountryName Indonesia => new CountryName("Indonesia");
        public static CountryName IsleOfMan => new CountryName("Isle of Man");
        public static CountryName India => new CountryName("India");
        public static CountryName BritishIndianOceanTerritory => new CountryName("British Indian Ocean Territory");
        public static CountryName Ireland => new CountryName("Ireland");
        public static CountryName Iran => new CountryName("Iran, Islamic Republic of");
        public static CountryName Iraq => new CountryName("Iraq");
        public static CountryName Iceland => new CountryName("Iceland");
        public static CountryName Israel => new CountryName("Israel");
        public static CountryName Italy => new CountryName("Italy");
        public static CountryName Jamaica => new CountryName("Jamaica");
        public static CountryName Jersey => new CountryName("Jersey");
        public static CountryName Jordan => new CountryName("Jordan");
        public static CountryName Japan => new CountryName("Japan");
        public static CountryName Kazakhstan => new CountryName("Kazakhstan");
        public static CountryName Kenya => new CountryName("Kenya");
        public static CountryName Kyrgyzstan => new CountryName("Kyrgyzstan");
        public static CountryName Cambodia => new CountryName("Cambodia");
        public static CountryName Kiribati => new CountryName("Kiribati");
        public static CountryName SaintKittsAndNevis => new CountryName("Saint Kitts and Nevis");
        public static CountryName SouthKorea => new CountryName("Korea, Republic of");
        public static CountryName Kuwait => new CountryName("Kuwait");
        public static CountryName Laos => new CountryName("Lao People's Democratic Republic");
        public static CountryName Lebanon => new CountryName("Lebanon");
        public static CountryName Liberia => new CountryName("Liberia");
        public static CountryName LibyanArabJamahiriya => new CountryName("Libyan Arab Jamahiriya");
        public static CountryName SaintLucia => new CountryName("Saint Lucia");
        public static CountryName Liechtenstein => new CountryName("Liechtenstein");
        public static CountryName SriLanka => new CountryName("Sri Lanka");
        public static CountryName Lesotho => new CountryName("Lesotho");
        public static CountryName Lithuania => new CountryName("Lithuania");
        public static CountryName Luxembourg => new CountryName("Luxembourg");
        public static CountryName Latvia => new CountryName("Latvia");
        public static CountryName Macao => new CountryName("Macao");
        public static CountryName SaintMartin => new CountryName("Saint Martin (French part)");
        public static CountryName Morocco => new CountryName("Morocco");
        public static CountryName Monaco => new CountryName("Monaco");
        public static CountryName Moldova => new CountryName("Moldova, Republic of");
        public static CountryName Madagascar => new CountryName("Madagascar");
        public static CountryName Maldives => new CountryName("Maldives");
        public static CountryName Mexico => new CountryName("Mexico");
        public static CountryName MarshallIslands => new CountryName("Marshall Islands");
        public static CountryName Macedonia => new CountryName("Macedonia, the former Yugoslav Republic of");
        public static CountryName Mali => new CountryName("Mali");
        public static CountryName Malta => new CountryName("Malta");
        public static CountryName Myanmar => new CountryName("Myanmar");
        public static CountryName Montenegro => new CountryName("Montenegro");
        public static CountryName Mongolia => new CountryName("Mongolia");
        public static CountryName NorthernMarianaIslands => new CountryName("Northern Mariana Islands");
        public static CountryName Mozambique => new CountryName("Mozambique");
        public static CountryName Mauritania => new CountryName("Mauritania");
        public static CountryName Montserrat => new CountryName("Montserrat");
        public static CountryName Martinique => new CountryName("Martinique");
        public static CountryName Mauritius => new CountryName("Mauritius");
        public static CountryName Malawi => new CountryName("Malawi");
        public static CountryName Malaysia => new CountryName("Malaysia");
        public static CountryName Mayotte => new CountryName("Mayotte");
        public static CountryName Namibia => new CountryName("Namibia");
        public static CountryName NewCaledonia => new CountryName("New Caledonia");
        public static CountryName Niger => new CountryName("Niger");
        public static CountryName NorfolkIsland => new CountryName("Norfolk Island");
        public static CountryName Nigeria => new CountryName("Nigeria");
        public static CountryName Nicaragua => new CountryName("Nicaragua");
        public static CountryName Niue => new CountryName("Niue");
        public static CountryName Netherlands => new CountryName("Netherlands");
        public static CountryName Norway => new CountryName("Norway");
        public static CountryName Nepal => new CountryName("Nepal");
        public static CountryName Nauru => new CountryName("Nauru");
        public static CountryName NewZealand => new CountryName("New Zealand");
        public static CountryName Oman => new CountryName("Oman");
        public static CountryName Pakistan => new CountryName("Pakistan");
        public static CountryName Panama => new CountryName("Panama");
        public static CountryName Pitcairn => new CountryName("Pitcairn");
        public static CountryName Peru => new CountryName("Peru");
        public static CountryName Philippines => new CountryName("Philippines");
        public static CountryName Palau => new CountryName("Palau");
        public static CountryName PapuaNewGuinea => new CountryName("Papua New Guinea");
        public static CountryName Poland => new CountryName("Poland");
        public static CountryName PuertoRico => new CountryName("Puerto Rico");
        public static CountryName NorthKorea => new CountryName("Korea, Democratic People's Republic of");
        public static CountryName Portugal => new CountryName("Portugal");
        public static CountryName Paraguay => new CountryName("Paraguay");
        public static CountryName PalestinianTerritories => new CountryName("Palestinian Territory, Occupied");
        public static CountryName FrenchPolynesia => new CountryName("French Polynesia");
        public static CountryName Qatar => new CountryName("Qatar");
        public static CountryName Reunion => new CountryName("Réunion");
        public static CountryName Romania => new CountryName("Romania");
        public static CountryName RussianFederation => new CountryName("Russian Federation");
        public static CountryName Rwanda => new CountryName("Rwanda");
        public static CountryName SaudiArabia => new CountryName("Saudi Arabia");
        public static CountryName Sudan => new CountryName("Sudan");
        public static CountryName Senegal => new CountryName("Senegal");
        public static CountryName Singapore => new CountryName("Singapore");
        public static CountryName SouthGeorgiaAndTheSouthSandwichIslands => new CountryName("South Georgia and the South Sandwich Islands");
        public static CountryName SaintHelenaAscensionAndTristanDaCunha => new CountryName("Saint Helena, Ascension and Tristan da Cunha");
        public static CountryName SvalbardAndJanMayen => new CountryName("Svalbard and Jan Mayen");
        public static CountryName SolomonIslands => new CountryName("Solomon Islands");
        public static CountryName SierraLeone => new CountryName("Sierra Leone");
        public static CountryName ElSalvador => new CountryName("El Salvador");
        public static CountryName SanMarino => new CountryName("San Marino");
        public static CountryName Somalia => new CountryName("Somalia");
        public static CountryName SaintPierreAndMiquelon => new CountryName("Saint Pierre and Miquelon");
        public static CountryName Serbia => new CountryName("Serbia");
        public static CountryName SaoTomeAndPrincipe => new CountryName("Sao Tome and Principe");
        public static CountryName Suriname => new CountryName("Suriname");
        public static CountryName Slovakia => new CountryName("Slovakia");
        public static CountryName Slovenia => new CountryName("Slovenia");
        public static CountryName Sweden => new CountryName("Sweden");
        public static CountryName Swaziland => new CountryName("Swaziland");
        public static CountryName Seychelles => new CountryName("Seychelles");
        public static CountryName SyrianArabRepublic => new CountryName("Syrian Arab Republic");
        public static CountryName TurksAndCaicosIslands => new CountryName("Turks and Caicos Islands");
        public static CountryName Chad => new CountryName("Chad");
        public static CountryName Togo => new CountryName("Togo");
        public static CountryName Thailand => new CountryName("Thailand");
        public static CountryName Tajikistan => new CountryName("Tajikistan");
        public static CountryName Tokelau => new CountryName("Tokelau");
        public static CountryName Turkmenistan => new CountryName("Turkmenistan");
        public static CountryName TimorLeste => new CountryName("Timor-Leste");
        public static CountryName Tonga => new CountryName("Tonga");
        public static CountryName TrinidadAndTobago => new CountryName("Trinidad and Tobago");
        public static CountryName Tunisia => new CountryName("Tunisia");
        public static CountryName Turkey => new CountryName("Turkey");
        public static CountryName Tuvalu => new CountryName("Tuvalu");
        public static CountryName TaiwanProvince => new CountryName("Taiwan, Province of China");
        public static CountryName Tanzania => new CountryName("Tanzania, United Republic of");
        public static CountryName Uganda => new CountryName("Uganda");
        public static CountryName Ukraine => new CountryName("Ukraine");
        public static CountryName UnitedStatesMinorOutlyingIslands => new CountryName("United States Minor Outlying Islands");
        public static CountryName Uruguay => new CountryName("Uruguay");
        public static CountryName UnitedStates => new CountryName("United States");
        public static CountryName Uzbekistan => new CountryName("Uzbekistan");
        public static CountryName HolySee => new CountryName("Holy See (Vatican City State)");
        public static CountryName SaintVincentAndTheGrenadines => new CountryName("Saint Vincent and the Grenadines");
        public static CountryName Venezuela => new CountryName("Venezuela, Bolivarian Republic of");
        public static CountryName BritishVirginIslands => new CountryName("Virgin Islands, British");
        public static CountryName UnitedStatesVirginIslands => new CountryName("Virgin Islands, U.S.");
        public static CountryName Vietnam => new CountryName("Vietnam");
        public static CountryName Vanuatu => new CountryName("Vanuatu");
        public static CountryName WallisAndFutuna => new CountryName("Wallis and Futuna");
        public static CountryName Samoa => new CountryName("Samoa");
        public static CountryName Yemen => new CountryName("Yemen");
        public static CountryName SouthAfrica => new CountryName("South Africa");
        public static CountryName Zambia => new CountryName("Zambia");
        public static CountryName Zimbabwe => new CountryName("Zimbabwe");

        public static IReadOnlyCollection<CountryName> GetAll() => new List<CountryName>
        {
            Aruba,
            Afghanistan,
            Angola,
            Anguilla,
            AlandIslands,
            Albania,
            Andorra,
            NetherlandsAntilles,
            UnitedArabEmirates,
            Argentina,
            Armenia,
            AmericanSamoa,
            Antarctica,
            FrenchSouthernTerritories,
            AntiguaAndBarbuda,
            Australia,
            Austria,
            Azerbaijan,
            Burundi,
            Belgium,
            Benin,
            BurkinaFaso,
            Bangladesh,
            Bulgaria,
            Bahrain,
            Bahamas,
            BosniaAndHerzegovina,
            SaintBarthelemy,
            Belarus,
            Belize,
            Bermuda,
            Bolivia,
            Brazil,
            Barbados,
            BruneiDarussalam,
            Bhutan,
            BouvetIsland,
            Botswana,
            CentralAfricanRepublic,
            Canada,
            CocosIslands,
            Switzerland,
            Chile,
            China,
            IvoryCoast,
            Cameroon,
            DemocraticRepublicOfTheCongo,
            Congo,
            CookIslands,
            Colombia,
            Comoros,
            CapeVerde,
            CostaRica,
            Cuba,
            ChristmasIsland,
            CaymanIslands,
            Cyprus,
            CzechRepublic,
            Germany,
            Djibouti,
            Dominica,
            Denmark,
            DominicanRepublic,
            Algeria,
            Ecuador,
            Egypt,
            Eritrea,
            WesternSahara,
            Spain,
            Estonia,
            Ethiopia,
            Finland,
            Fiji,
            FalklandIslands,
            France,
            FaroeIslands,
            FederatedStatesOfMicronesia,
            Gabon,
            UnitedKingdom,
            Georgia,
            Guernsey,
            Ghana,
            Gibraltar,
            Guinea,
            Guadeloupe,
            Gambia,
            GuineaBissau,
            EquatorialGuinea,
            Greece,
            Grenada,
            Greenland,
            Guatemala,
            FrenchGuiana,
            Guam,
            Guyana,
            HongKong,
            HeardIslandAndMcDonaldIslands,
            Honduras,
            Croatia,
            Haiti,
            Hungary,
            Indonesia,
            IsleOfMan,
            India,
            BritishIndianOceanTerritory,
            Ireland,
            Iran,
            Iraq,
            Iceland,
            Israel,
            Italy,
            Jamaica,
            Jersey,
            Jordan,
            Japan,
            Kazakhstan,
            Kenya,
            Kyrgyzstan,
            Cambodia,
            Kiribati,
            SaintKittsAndNevis,
            SouthKorea,
            Kuwait,
            Laos,
            Lebanon,
            Liberia,
            LibyanArabJamahiriya,
            SaintLucia,
            Liechtenstein,
            SriLanka,
            Lesotho,
            Lithuania,
            Luxembourg,
            Latvia,
            Macao,
            SaintMartin,
            Morocco,
            Monaco,
            Moldova,
            Madagascar,
            Maldives,
            Mexico,
            MarshallIslands,
            Macedonia,
            Mali,
            Malta,
            Myanmar,
            Montenegro,
            Mongolia,
            NorthernMarianaIslands,
            Mozambique,
            Mauritania,
            Montserrat,
            Martinique,
            Mauritius,
            Malawi,
            Malaysia,
            Mayotte,
            Namibia,
            NewCaledonia,
            Niger,
            NorfolkIsland,
            Nigeria,
            Nicaragua,
            Niue,
            Netherlands,
            Norway,
            Nepal,
            Nauru,
            NewZealand,
            Oman,
            Pakistan,
            Panama,
            Pitcairn,
            Peru,
            Philippines,
            Palau,
            PapuaNewGuinea,
            Poland,
            PuertoRico,
            NorthKorea,
            Portugal,
            Paraguay,
            PalestinianTerritories,
            FrenchPolynesia,
            Qatar,
            Reunion,
            Romania,
            RussianFederation,
            Rwanda,
            SaudiArabia,
            Sudan,
            Senegal,
            Singapore,
            SouthGeorgiaAndTheSouthSandwichIslands,
            SaintHelenaAscensionAndTristanDaCunha,
            SvalbardAndJanMayen,
            SolomonIslands,
            SierraLeone,
            ElSalvador,
            SanMarino,
            Somalia,
            SaintPierreAndMiquelon,
            Serbia,
            SaoTomeAndPrincipe,
            Suriname,
            Slovakia,
            Slovenia,
            Sweden,
            Swaziland,
            Seychelles,
            SyrianArabRepublic,
            TurksAndCaicosIslands,
            Chad,
            Togo,
            Thailand,
            Tajikistan,
            Tokelau,
            Turkmenistan,
            TimorLeste,
            Tonga,
            TrinidadAndTobago,
            Tunisia,
            Turkey,
            Tuvalu,
            TaiwanProvince,
            Tanzania,
            Uganda,
            Ukraine,
            UnitedStatesMinorOutlyingIslands,
            Uruguay,
            UnitedStates,
            Uzbekistan,
            HolySee,
            SaintVincentAndTheGrenadines,
            Venezuela,
            BritishVirginIslands,
            UnitedStatesVirginIslands,
            Vietnam,
            Vanuatu,
            WallisAndFutuna,
            Samoa,
            Yemen,
            SouthAfrica,
            Zambia,
            Zimbabwe
        };
        
        public static explicit operator CountryName(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new CountryName(instance);
        }

        public static implicit operator string(CountryName instance)
        {
            return instance?._value;
        }

        public static bool operator ==(CountryName x, CountryName y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(CountryName x, CountryName y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as CountryName);
        }

        public bool Equals(CountryName value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}