﻿namespace Framework.Domain.ValueObjects
{
    public static class CountryNameExtensions
    {
        public static CountryCode ToCountryCode(this CountryName countryName)
        {
            var converter = new CountryNameToCodeConverter();
            return converter.Convert(countryName);
        }
    }
}