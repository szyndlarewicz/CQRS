﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Framework.Domain.ValueObjects
{
    [SuppressMessage("Microsoft.StyleCop.CSharp.LayoutRules", "SA1516:ElementsMustBeSeparatedByBlankLine", Justification = "Too many similar properties")]
    public sealed class CurrencyCode : IValueObject
    {
        private static readonly string[] Codes =
        {
            "AFN",
            "EUR",
            "ALL",
            "DZD",
            "USD",
            "AOA",
            "XCD",
            "ARS",
            "AMD",
            "AWG",
            "AUD",
            "AZN",
            "BSD",
            "BHD",
            "BDT",
            "BBD",
            "BYN",
            "BZD",
            "XOF",
            "BMD",
            "INR",
            "BTN",
            "BOB",
            "BOV",
            "BAM",
            "BWP",
            "NOK",
            "BRL",
            "BND",
            "BGN",
            "BIF",
            "CVE",
            "KHR",
            "XAF",
            "CAD",
            "KYD",
            "CLP",
            "CLF",
            "CNY",
            "COP",
            "COU",
            "KMF",
            "CDF",
            "NZD",
            "CRC",
            "HRK",
            "CUP",
            "CUC",
            "ANG",
            "CZK",
            "DKK",
            "DJF",
            "DOP",
            "EGP",
            "SVC",
            "ERN",
            "ETB",
            "FKP",
            "FJD",
            "XPF",
            "GMD",
            "GEL",
            "GHS",
            "GIP",
            "GTQ",
            "GBP",
            "GNF",
            "GYD",
            "HTG",
            "HNL",
            "HKD",
            "HUF",
            "ISK",
            "XDR",
            "IRR",
            "IQD",
            "ILS",
            "JMD",
            "JPY",
            "JOD",
            "KZT",
            "KES",
            "KPW",
            "KRW",
            "KWD",
            "KGS",
            "LAK",
            "LBP",
            "LSL",
            "ZAR",
            "LRD",
            "LYD",
            "CHF",
            "MOP",
            "MKD",
            "MGA",
            "MWK",
            "MYR",
            "MVR",
            "MRO",
            "MUR",
            "XUA",
            "MXN",
            "MXV",
            "MDL",
            "MNT",
            "MAD",
            "MZN",
            "MMK",
            "NAD",
            "NPR",
            "NIO",
            "NGN",
            "OMR",
            "PKR",
            "PAB",
            "PGK",
            "PYG",
            "PEN",
            "PHP",
            "PLN",
            "QAR",
            "RON",
            "RUB",
            "RWF",
            "SHP",
            "WST",
            "STD",
            "SAR",
            "RSD",
            "SCR",
            "SLL",
            "SGD",
            "XSU",
            "SBD",
            "SOS",
            "SSP",
            "LKR",
            "SDG",
            "SRD",
            "SZL",
            "SEK",
            "CHW",
            "SYP",
            "TWD",
            "TJS",
            "TZS",
            "THB",
            "TOP",
            "TTD",
            "TND",
            "TRY",
            "TMT",
            "UGX",
            "UAH",
            "AED",
            "USN",
            "UYU",
            "UYI",
            "UZS",
            "VUV",
            "VEF",
            "VND",
            "YER",
            "ZMW",
            "ZWL",
            "XBA",
            "XBB",
            "XBC",
            "XBD",
            "XTS"
        };

        private readonly string _value;

        public CurrencyCode(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));

            if (!Codes.Contains(value))
                throw new ArgumentException("Unrecognized currency code");

            _value = value;
        }

        public static CurrencyCode Afn => new CurrencyCode("AFN");
        public static CurrencyCode Eur => new CurrencyCode("EUR");
        public static CurrencyCode All => new CurrencyCode("ALL");
        public static CurrencyCode Dzd => new CurrencyCode("DZD");
        public static CurrencyCode Usd => new CurrencyCode("USD");
        public static CurrencyCode Aoa => new CurrencyCode("AOA");
        public static CurrencyCode Xcd => new CurrencyCode("XCD");
        public static CurrencyCode Ars => new CurrencyCode("ARS");
        public static CurrencyCode Amd => new CurrencyCode("AMD");
        public static CurrencyCode Awg => new CurrencyCode("AWG");
        public static CurrencyCode Aud => new CurrencyCode("AUD");
        public static CurrencyCode Azn => new CurrencyCode("AZN");
        public static CurrencyCode Bsd => new CurrencyCode("BSD");
        public static CurrencyCode Bhd => new CurrencyCode("BHD");
        public static CurrencyCode Bdt => new CurrencyCode("BDT");
        public static CurrencyCode Bbd => new CurrencyCode("BBD");
        public static CurrencyCode Byn => new CurrencyCode("BYN");
        public static CurrencyCode Bzd => new CurrencyCode("BZD");
        public static CurrencyCode Xof => new CurrencyCode("XOF");
        public static CurrencyCode Bmd => new CurrencyCode("BMD");
        public static CurrencyCode Inr => new CurrencyCode("INR");
        public static CurrencyCode Btn => new CurrencyCode("BTN");
        public static CurrencyCode Bob => new CurrencyCode("BOB");
        public static CurrencyCode Bov => new CurrencyCode("BOV");
        public static CurrencyCode Bam => new CurrencyCode("BAM");
        public static CurrencyCode Bwp => new CurrencyCode("BWP");
        public static CurrencyCode Nok => new CurrencyCode("NOK");
        public static CurrencyCode Brl => new CurrencyCode("BRL");
        public static CurrencyCode Bnd => new CurrencyCode("BND");
        public static CurrencyCode Bgn => new CurrencyCode("BGN");
        public static CurrencyCode Bif => new CurrencyCode("BIF");
        public static CurrencyCode Cve => new CurrencyCode("CVE");
        public static CurrencyCode Khr => new CurrencyCode("KHR");
        public static CurrencyCode Xaf => new CurrencyCode("XAF");
        public static CurrencyCode Cad => new CurrencyCode("CAD");
        public static CurrencyCode Kyd => new CurrencyCode("KYD");
        public static CurrencyCode Clp => new CurrencyCode("CLP");
        public static CurrencyCode Clf => new CurrencyCode("CLF");
        public static CurrencyCode Cny => new CurrencyCode("CNY");
        public static CurrencyCode Cop => new CurrencyCode("COP");
        public static CurrencyCode Cou => new CurrencyCode("COU");
        public static CurrencyCode Kmf => new CurrencyCode("KMF");
        public static CurrencyCode Cdf => new CurrencyCode("CDF");
        public static CurrencyCode Nzd => new CurrencyCode("NZD");
        public static CurrencyCode Crc => new CurrencyCode("CRC");
        public static CurrencyCode Hrk => new CurrencyCode("HRK");
        public static CurrencyCode Cup => new CurrencyCode("CUP");
        public static CurrencyCode Cuc => new CurrencyCode("CUC");
        public static CurrencyCode Ang => new CurrencyCode("ANG");
        public static CurrencyCode Czk => new CurrencyCode("CZK");
        public static CurrencyCode Dkk => new CurrencyCode("DKK");
        public static CurrencyCode Djf => new CurrencyCode("DJF");
        public static CurrencyCode Dop => new CurrencyCode("DOP");
        public static CurrencyCode Egp => new CurrencyCode("EGP");
        public static CurrencyCode Svc => new CurrencyCode("SVC");
        public static CurrencyCode Ern => new CurrencyCode("ERN");
        public static CurrencyCode Etb => new CurrencyCode("ETB");
        public static CurrencyCode Fkp => new CurrencyCode("FKP");
        public static CurrencyCode Fjd => new CurrencyCode("FJD");
        public static CurrencyCode Xpf => new CurrencyCode("XPF");
        public static CurrencyCode Gmd => new CurrencyCode("GMD");
        public static CurrencyCode Gel => new CurrencyCode("GEL");
        public static CurrencyCode Ghs => new CurrencyCode("GHS");
        public static CurrencyCode Gip => new CurrencyCode("GIP");
        public static CurrencyCode Gtq => new CurrencyCode("GTQ");
        public static CurrencyCode Gbp => new CurrencyCode("GBP");
        public static CurrencyCode Gnf => new CurrencyCode("GNF");
        public static CurrencyCode Gyd => new CurrencyCode("GYD");
        public static CurrencyCode Htg => new CurrencyCode("HTG");
        public static CurrencyCode Hnl => new CurrencyCode("HNL");
        public static CurrencyCode Hkd => new CurrencyCode("HKD");
        public static CurrencyCode Huf => new CurrencyCode("HUF");
        public static CurrencyCode Isk => new CurrencyCode("ISK");
        public static CurrencyCode Xdr => new CurrencyCode("XDR");
        public static CurrencyCode Irr => new CurrencyCode("IRR");
        public static CurrencyCode Iqd => new CurrencyCode("IQD");
        public static CurrencyCode Ils => new CurrencyCode("ILS");
        public static CurrencyCode Jmd => new CurrencyCode("JMD");
        public static CurrencyCode Jpy => new CurrencyCode("JPY");
        public static CurrencyCode Jod => new CurrencyCode("JOD");
        public static CurrencyCode Kzt => new CurrencyCode("KZT");
        public static CurrencyCode Kes => new CurrencyCode("KES");
        public static CurrencyCode Kpw => new CurrencyCode("KPW");
        public static CurrencyCode Krw => new CurrencyCode("KRW");
        public static CurrencyCode Kwd => new CurrencyCode("KWD");
        public static CurrencyCode Kgs => new CurrencyCode("KGS");
        public static CurrencyCode Lak => new CurrencyCode("LAK");
        public static CurrencyCode Lbp => new CurrencyCode("LBP");
        public static CurrencyCode Lsl => new CurrencyCode("LSL");
        public static CurrencyCode Zar => new CurrencyCode("ZAR");
        public static CurrencyCode Lrd => new CurrencyCode("LRD");
        public static CurrencyCode Lyd => new CurrencyCode("LYD");
        public static CurrencyCode Chf => new CurrencyCode("CHF");
        public static CurrencyCode Mop => new CurrencyCode("MOP");
        public static CurrencyCode Mkd => new CurrencyCode("MKD");
        public static CurrencyCode Mga => new CurrencyCode("MGA");
        public static CurrencyCode Mwk => new CurrencyCode("MWK");
        public static CurrencyCode Myr => new CurrencyCode("MYR");
        public static CurrencyCode Mvr => new CurrencyCode("MVR");
        public static CurrencyCode Mro => new CurrencyCode("MRO");
        public static CurrencyCode Mur => new CurrencyCode("MUR");
        public static CurrencyCode Xua => new CurrencyCode("XUA");
        public static CurrencyCode Mxn => new CurrencyCode("MXN");
        public static CurrencyCode Mxv => new CurrencyCode("MXV");
        public static CurrencyCode Mdl => new CurrencyCode("MDL");
        public static CurrencyCode Mnt => new CurrencyCode("MNT");
        public static CurrencyCode Mad => new CurrencyCode("MAD");
        public static CurrencyCode Mzn => new CurrencyCode("MZN");
        public static CurrencyCode Mmk => new CurrencyCode("MMK");
        public static CurrencyCode Nad => new CurrencyCode("NAD");
        public static CurrencyCode Npr => new CurrencyCode("NPR");
        public static CurrencyCode Nio => new CurrencyCode("NIO");
        public static CurrencyCode Ngn => new CurrencyCode("NGN");
        public static CurrencyCode Omr => new CurrencyCode("OMR");
        public static CurrencyCode Pkr => new CurrencyCode("PKR");
        public static CurrencyCode Pab => new CurrencyCode("PAB");
        public static CurrencyCode Pgk => new CurrencyCode("PGK");
        public static CurrencyCode Pyg => new CurrencyCode("PYG");
        public static CurrencyCode Pen => new CurrencyCode("PEN");
        public static CurrencyCode Php => new CurrencyCode("PHP");
        public static CurrencyCode Pln => new CurrencyCode("PLN");
        public static CurrencyCode Qar => new CurrencyCode("QAR");
        public static CurrencyCode Ron => new CurrencyCode("RON");
        public static CurrencyCode Rub => new CurrencyCode("RUB");
        public static CurrencyCode Rwf => new CurrencyCode("RWF");
        public static CurrencyCode Shp => new CurrencyCode("SHP");
        public static CurrencyCode Wst => new CurrencyCode("WST");
        public static CurrencyCode Std => new CurrencyCode("STD");
        public static CurrencyCode Sar => new CurrencyCode("SAR");
        public static CurrencyCode Rsd => new CurrencyCode("RSD");
        public static CurrencyCode Scr => new CurrencyCode("SCR");
        public static CurrencyCode Sll => new CurrencyCode("SLL");
        public static CurrencyCode Sgd => new CurrencyCode("SGD");
        public static CurrencyCode Xsu => new CurrencyCode("XSU");
        public static CurrencyCode Sbd => new CurrencyCode("SBD");
        public static CurrencyCode Sos => new CurrencyCode("SOS");
        public static CurrencyCode Ssp => new CurrencyCode("SSP");
        public static CurrencyCode Lkr => new CurrencyCode("LKR");
        public static CurrencyCode Sdg => new CurrencyCode("SDG");
        public static CurrencyCode Srd => new CurrencyCode("SRD");
        public static CurrencyCode Szl => new CurrencyCode("SZL");
        public static CurrencyCode Sek => new CurrencyCode("SEK");
        public static CurrencyCode Chw => new CurrencyCode("CHW");
        public static CurrencyCode Syp => new CurrencyCode("SYP");
        public static CurrencyCode Twd => new CurrencyCode("TWD");
        public static CurrencyCode Tjs => new CurrencyCode("TJS");
        public static CurrencyCode Tzs => new CurrencyCode("TZS");
        public static CurrencyCode Thb => new CurrencyCode("THB");
        public static CurrencyCode Top => new CurrencyCode("TOP");
        public static CurrencyCode Ttd => new CurrencyCode("TTD");
        public static CurrencyCode Tnd => new CurrencyCode("TND");
        public static CurrencyCode Try => new CurrencyCode("TRY");
        public static CurrencyCode Tmt => new CurrencyCode("TMT");
        public static CurrencyCode Ugx => new CurrencyCode("UGX");
        public static CurrencyCode Uah => new CurrencyCode("UAH");
        public static CurrencyCode Aed => new CurrencyCode("AED");
        public static CurrencyCode Usn => new CurrencyCode("USN");
        public static CurrencyCode Uyu => new CurrencyCode("UYU");
        public static CurrencyCode Uyi => new CurrencyCode("UYI");
        public static CurrencyCode Uzs => new CurrencyCode("UZS");
        public static CurrencyCode Vuv => new CurrencyCode("VUV");
        public static CurrencyCode Vef => new CurrencyCode("VEF");
        public static CurrencyCode Vnd => new CurrencyCode("VND");
        public static CurrencyCode Yer => new CurrencyCode("YER");
        public static CurrencyCode Zmw => new CurrencyCode("ZMW");
        public static CurrencyCode Zwl => new CurrencyCode("ZWL");

        public static IReadOnlyCollection<CurrencyCode> GetAll() => new List<CurrencyCode>
        {
            Afn,
            Eur,
            All,
            Dzd,
            Usd,
            Aoa,
            Xcd,
            Ars,
            Amd,
            Awg,
            Aud,
            Azn,
            Bsd,
            Bhd,
            Bdt,
            Bbd,
            Byn,
            Bzd,
            Xof,
            Bmd,
            Inr,
            Btn,
            Bob,
            Bov,
            Bam,
            Bwp,
            Nok,
            Brl,
            Bnd,
            Bgn,
            Bif,
            Cve,
            Khr,
            Xaf,
            Cad,
            Kyd,
            Clp,
            Clf,
            Cny,
            Cop,
            Cou,
            Kmf,
            Cdf,
            Nzd,
            Crc,
            Hrk,
            Cup,
            Cuc,
            Ang,
            Czk,
            Dkk,
            Djf,
            Dop,
            Egp,
            Svc,
            Ern,
            Etb,
            Fkp,
            Fjd,
            Xpf,
            Gmd,
            Gel,
            Ghs,
            Gip,
            Gtq,
            Gbp,
            Gnf,
            Gyd,
            Htg,
            Hnl,
            Hkd,
            Huf,
            Isk,
            Xdr,
            Irr,
            Iqd,
            Ils,
            Jmd,
            Jpy,
            Jod,
            Kzt,
            Kes,
            Kpw,
            Krw,
            Kwd,
            Kgs,
            Lak,
            Lbp,
            Lsl,
            Zar,
            Lrd,
            Lyd,
            Chf,
            Mop,
            Mkd,
            Mga,
            Mwk,
            Myr,
            Mvr,
            Mro,
            Mur,
            Xua,
            Mxn,
            Mxv,
            Mdl,
            Mnt,
            Mad,
            Mzn,
            Mmk,
            Nad,
            Npr,
            Nio,
            Ngn,
            Omr,
            Pkr,
            Pab,
            Pgk,
            Pyg,
            Pen,
            Php,
            Pln,
            Qar,
            Ron,
            Rub,
            Rwf,
            Shp,
            Wst,
            Std,
            Sar,
            Rsd,
            Scr,
            Sll,
            Sgd,
            Xsu,
            Sbd,
            Sos,
            Ssp,
            Lkr,
            Sdg,
            Srd,
            Szl,
            Sek,
            Chw,
            Syp,
            Twd,
            Tjs,
            Tzs,
            Thb,
            Top,
            Ttd,
            Tnd,
            Try,
            Tmt,
            Ugx,
            Uah,
            Aed,
            Usn,
            Uyu,
            Uyi,
            Uzs,
            Vuv,
            Vef,
            Vnd,
            Yer,
            Zmw,
            Zwl
        };

        public static explicit operator CurrencyCode(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new CurrencyCode(instance);
        }

        public static implicit operator string(CurrencyCode instance)
        {
            return instance?._value;
        }

        public static bool operator ==(CurrencyCode x, CurrencyCode y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(CurrencyCode x, CurrencyCode y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as CurrencyCode);
        }

        public bool Equals(CurrencyCode value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}