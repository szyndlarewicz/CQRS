﻿namespace Framework.Domain.ValueObjects
{
    public static class CurrencyCodeExtensions
    {
        public static CurrencyName ToCurrencyName(this CurrencyCode currencyCode)
        {
            var converter = new CurrencyCodeToNameConverter();
            return converter.Convert(currencyCode);
        }
    }
}