﻿namespace Framework.Domain.ValueObjects
{
    public interface IValueObjectConverter<in TSource, out TResult> : IValueObjectConverter
        where TSource : IValueObject
        where TResult : IValueObject
    {
        TResult Convert(TSource source);
    }
}
