﻿using System;
using Framework.Search.Contracts.Types;
using Framework.Search.Infrastructure;

namespace Framework.Search.Dapper
{
    public sealed class SqlServer2008QueryPagination : ISqlQueryPagination
    {
        private const string PagedQuery = "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0} {1}) rn, n.* FROM ({2}) AS n) AS m WHERE rn >= {3} AND rn < {4}";
        private const string CountQuery = "SELECT count(*) AS rc FROM ({0}) AS n";

        public string Build(SqlQuery sqlQuery, PageIndex pageIndex, PageSize pageSize, SortOrder sortOrder)
        {
            if (sqlQuery == null)
                throw new ArgumentNullException(nameof(sqlQuery));

            if (pageIndex == null)
                throw new ArgumentNullException(nameof(pageIndex));

            if (pageSize == null)
                throw new ArgumentNullException(nameof(pageSize));

            if (sortOrder == null)
                throw new ArgumentNullException(nameof(sortOrder));

            string pagedSql = CreatePagedQuery(sqlQuery, pageIndex, pageSize, sortOrder);
            string countSql = CreateCountQuery(sqlQuery);

            return pagedSql + ";" + countSql;
        }

        private string CreatePagedQuery(SqlQuery sqlQuery, PageIndex pageIndex, PageSize pageSize, SortOrder sortOrder)
        {
            return string.Format(
                PagedQuery, 
                sortOrder.OrderExpression,
                sortOrder.OrderDirection == OrderDirection.Ascending ? "ASC" : "DESC", 
                sqlQuery.Value,
                (pageIndex * pageSize) + 1,
                (pageIndex * pageSize) + pageSize + 1);
        }

        private string CreateCountQuery(SqlQuery sqlQuery)
        {
            return string.Format(CountQuery, sqlQuery.Value);
        }
    }
}