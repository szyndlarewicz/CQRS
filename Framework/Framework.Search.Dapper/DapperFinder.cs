﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Framework.Search.Contracts.Types;
using Framework.Search.Contracts.Views;
using Framework.Search.Infrastructure;

namespace Framework.Search.Dapper
{
    public abstract class DapperFinder : IFinder
    {
        private ISqlQueryPagination _pagination;

        protected DapperFinder(ReadDbConnection connection)
        {
            Connection = connection ?? throw new ArgumentNullException(nameof(connection));
            Pagination = new SqlServer2008QueryPagination();
        }

        public ISqlQueryPagination Pagination 
        {
            get => _pagination;
            set => _pagination = value ?? throw new ArgumentNullException(nameof(value));
        }

        protected ReadDbConnection Connection { get; }

        protected IEnumerable<T> Query<T>(SqlQuery sqlQuery, object param = null) where T : View
        {
            if (sqlQuery == null)
                throw new ArgumentNullException(nameof(sqlQuery));

            return Connection.Connection.Query<T>(sqlQuery.Value, param);
        }

        protected PageCollection<T> Query<T>(SqlQuery sqlQuery, PageIndex pageIndex, PageSize pageSize, SortOrder sortOrder, object param = null) where T : View
        {
            string sql = _pagination.Build(sqlQuery, pageIndex, pageSize, sortOrder);

            var result = Connection.Connection.QueryMultiple(sql, param);
            var queryResult = result.Read<T>().AsList();
            var rowsCount = result.Read<long>().Single();
            return new PageCollection<T>(queryResult, rowsCount);
        }
    }
}