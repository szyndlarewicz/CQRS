﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Framework.Search.Dapper
{
    public class ReadDbConnection
    {
        public const string DefaultDbName = "name=database";

        public ReadDbConnection(string nameOrConnectionString = DefaultDbName)
        {
            if (nameOrConnectionString == null)
                throw new ArgumentNullException(nameof(nameOrConnectionString));

            if (nameOrConnectionString.Length == 0)
                throw new ArgumentException($"{nameof(nameOrConnectionString)} cannot be Empty");

            Connection = new SqlConnection(RetriveConnectionString(nameOrConnectionString));
        }

        public IDbConnection Connection { get; protected set; }

        protected string RetriveConnectionString(string nameOrConnectionString)
        {
            return nameOrConnectionString.StartsWith("name=")
                ? GetConnectionStringFromConfig(nameOrConnectionString.Substring(5)) 
                : nameOrConnectionString;
        }

        protected string GetConnectionStringFromConfig(string connectionStringName)
        {
            return ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }
    }
}