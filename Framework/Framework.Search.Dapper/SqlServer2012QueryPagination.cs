﻿using System;
using Framework.Search.Contracts.Types;
using Framework.Search.Infrastructure;

namespace Framework.Search.Dapper
{
    public sealed class SqlServer2012QueryPagination : ISqlQueryPagination
    {
        private const string PagedQuery = "({0}) ORDER BY {1} {2} OFFSET {3} ROWS FETCH NEXT {4} ROWS ONLY";
        private const string CountQuery = "SELECT count(*) AS rc FROM ({0}) AS n";

        public string Build(SqlQuery sqlQuery, PageIndex pageIndex, PageSize pageSize, SortOrder sortOrder)
        {
            if (sqlQuery == null)
                throw new ArgumentNullException(nameof(sqlQuery));

            if (pageIndex == null)
                throw new ArgumentNullException(nameof(pageIndex));
            
            if (pageSize == null)
                throw new ArgumentNullException(nameof(pageSize));
            
            if (sortOrder == null)
                throw new ArgumentNullException(nameof(sortOrder));

            string pagedSql = CreatePagedQuery(sqlQuery, pageIndex, pageSize, sortOrder);
            string countSql = CreateCountQuery(sqlQuery);

            return pagedSql + ";" + countSql;
        }

        private string CreatePagedQuery(SqlQuery sqlQuery, PageIndex pageIndex, PageSize pageSize, SortOrder sortOrder)
        {
            return string.Format(
                PagedQuery,
                sqlQuery.Value,
                sortOrder.OrderExpression,
                sortOrder.OrderDirection == OrderDirection.Ascending ? "ASC" : "DESC",
                pageIndex * pageSize,
                pageSize);
        }

        private string CreateCountQuery(SqlQuery sqlQuery)
        {
            return string.Format(CountQuery, sqlQuery.Value);
        }
    }
}