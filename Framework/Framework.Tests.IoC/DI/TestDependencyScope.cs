﻿using Framework.IoC;
using Framework.IoC.Ninject;
using Framework.Testing.DI;

namespace Framework.Tests.IoC.DI
{
    public sealed class TestDependencyScope : NinjectDependencyScope<TestDependencyScope>
    {
        public TestDependencyScope RegisterDependencyResolver()
        {
            RegisterSingleton<IDependencyResolver, NinjectDependencyResolver>();
            return this;
        }

        public TestDependencyScope RegisterAmbientScopeFactory()
        {
            RegisterSingleton<IAmbientScopeFactory, NinjectAmbientScopeFactory>();
            return this;
        }
    }
}