﻿using Framework.IoC;
using Framework.Tests.IoC.DI;
using Framework.IoC.Ninject;
using Framework.Tests.IoC.Mocks;
using Ninject;
using NUnit.Framework;

namespace Framework.Tests.IoC
{
    [TestFixture]
    public sealed class Tests_AmbientScope
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterAmbientScopeFactory()
                .Done();
        }

        [TestCase]
        public void It_should_inject_the_same_instance_inside_ambient_scope()
        {
            IKernel kernel = _resolver.Get<IKernel>();
            kernel.Bind<Model>().ToSelf().InAmbientOrRequestScope();

            var ambientScopeFactory = _resolver.Get<IAmbientScopeFactory>();
            using (ambientScopeFactory.Create())
            {
                Model modelA = _resolver.Get<Model>();
                Model modelB = _resolver.Get<Model>();
                Assert.AreSame(modelA, modelB);
            }
        }

        [TestCase]
        public void It_should_inject_different_instances_outside_ambient_scope()
        {
            IKernel kernel = _resolver.Get<IKernel>();
            kernel.Bind<Model>().ToSelf().InTransientScope();

            Model modelA, modelB;

            var ambientScopeFactory = _resolver.Get<IAmbientScopeFactory>();

            using (ambientScopeFactory.Create())
            {
                modelA = _resolver.Get<Model>();
            }

            using (ambientScopeFactory.Create())
            {
                modelB = _resolver.Get<Model>();
            }

            Assert.AreNotSame(modelA, modelB);
        }
    }
}