﻿using System;
using Framework.IoC;
using Framework.IoC.Ninject;
using Ninject;
using Ninject.Web.Common;

namespace Framework.Testing.DI
{
    public class NinjectDependencyScope<T> : IDependencyScope
        where T : NinjectDependencyScope<T>, new()
    {
        protected readonly IKernel Kernel;

        public NinjectDependencyScope()
        {
            Kernel = new StandardKernel();
        }

        public static T Instance => new T();

        #region Transient
        public IDependencyRegister RegisterTransient(Type definition, Type implementation)
        {
            Kernel.Bind(definition).To(implementation).InTransientScope();
            return this;
        }

        public IDependencyRegister RegisterTransient<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().To<TImplementation>().InTransientScope();
            return this;
        }

        public IDependencyRegister RegisterTransient(Type implementation)
        {
            Kernel.Bind(implementation).ToSelf().InTransientScope();
            return this;
        }

        public IDependencyRegister RegisterTransient<TImplementation>()
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToSelf().InTransientScope();
            return this;
        }
        #endregion

        #region AmbientScope
        public IDependencyRegister RegisterAmbientScope(Type definition, Type implementation)
        {
            Kernel.Bind(definition).To(implementation).InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().To<TImplementation>().InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope(Type implementation)
        {
            Kernel.Bind(implementation).ToSelf().InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope<TImplementation>()
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToSelf().InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope(Type implementation, object value)
        {
            Kernel.Bind(implementation).ToConstant(value).InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope<TImplementation>(TImplementation value)
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToConstant(value).InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope(Type definition, Type implementation, object value)
        {
            Kernel.Bind(definition).ToConstant(value).InAmbientOrRequestScope();
            return this;
        }

        public IDependencyRegister RegisterAmbientScope<TDefinition, TImplementation>(TImplementation value)
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().ToConstant(value).InAmbientOrRequestScope();
            return this;
        }
        #endregion

        #region RequestScope
        public IDependencyRegister RegisterRequestScope(Type definition, Type implementation)
        {
            Kernel.Bind(definition).To(implementation).InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().To<TImplementation>().InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope(Type implementation)
        {
            Kernel.Bind(implementation).ToSelf().InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope<TImplementation>()
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToSelf().InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope(Type implementation, object value)
        {
            Kernel.Bind(implementation).ToConstant(value).InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope<TImplementation>(TImplementation value)
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToConstant(value).InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope(Type definition, Type implementation, object value)
        {
            Kernel.Bind(definition).ToConstant(value).InRequestScope();
            return this;
        }

        public IDependencyRegister RegisterRequestScope<TDefinition, TImplementation>(TImplementation value)
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().ToConstant(value).InRequestScope();
            return this;
        }
        #endregion

        #region Singleton
        public IDependencyRegister RegisterSingleton(Type definition, Type implementation)
        {
            Kernel.Bind(definition).To(implementation).InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().To<TImplementation>().InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton(Type implementation)
        {
            Kernel.Bind(implementation).ToSelf().InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton<TImplementation>()
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToSelf().InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton(Type implementation, object value)
        {
            Kernel.Bind(implementation).ToConstant(value).InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton<TImplementation>(TImplementation value)
            where TImplementation : class
        {
            Kernel.Bind<TImplementation>().ToConstant(value).InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton(Type definition, Type implementation, object value)
        {
            Kernel.Bind(definition).ToConstant(value).InSingletonScope();
            return this;
        }

        public IDependencyRegister RegisterSingleton<TDefinition, TImplementation>(TImplementation value)
            where TDefinition : class
            where TImplementation : class, TDefinition
        {
            Kernel.Bind<TDefinition>().ToConstant(value).InSingletonScope();
            return this;
        }
        #endregion

        public IDependencyResolver Done()
        {
            return this;
        }

        public TInstance Get<TInstance>()
        {
            return Kernel.Get<TInstance>();
        }

        public TInstance TryGet<TInstance>()
        {
            return Kernel.TryGet<TInstance>();
        }

        public object Get(Type type)
        {
            return Kernel.Get(type);
        }

        public object TryGet(Type type)
        {
            return Kernel.TryGet(type);
        }
    }
}