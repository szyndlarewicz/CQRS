﻿using Framework.IoC;

namespace Framework.Testing.DI
{
    public interface IDependencyScope : IDependencyRegister, IDependencyResolver
    {
    }
}