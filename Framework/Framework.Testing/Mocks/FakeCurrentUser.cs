﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Core.Authentication;

namespace Framework.Testing.Mocks
{
    public sealed class FakeCurrentUser : ICurrentUser
    {
        private Guid _userId = Guid.Empty;
        private IList<string> _roles = new List<string>();

        public FakeCurrentUser()
        {
        }

        public FakeCurrentUser(Guid userId) 
        {
            _userId = userId;
        }

        public FakeCurrentUser(Guid userId, params string[] roles)
        {
            if (roles.Length == 0)
                throw new ArgumentException($"{nameof(roles)} cannot be Empty");

            _userId = userId;
            _roles = roles.ToList();
        }

        public Guid UserId => _userId;

        public bool IsInRole(string roleName)
        {
            return _roles.Any(role => role == roleName);
        }

        public void Set(Guid userId)
        {
            _userId = userId;
            _roles = new List<string>();
        }

        public void Set(Guid userId, params string[] roles)
        {
            if (roles.Length == 0)
                throw new ArgumentException($"{nameof(roles)} cannot be Empty");

            _userId = userId;
            _roles = roles.ToList();
        }
    }
}