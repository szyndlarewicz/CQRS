﻿using System;
using Framework.Core.Providers;

namespace Framework.Testing.Providers
{
    public sealed class StaticDateTimeProvider : DateTimeProvider
    {
        private DateTime _nowDateTime = DateTime.Now;

        public override DateTime Now => _nowDateTime;

        public void Change(DateTime now)
        {
            _nowDateTime = now;
        }

        public void Change(string now)
        {
            _nowDateTime = DateTime.Parse(now);
        }
    }
}