﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Framework.Testing
{
    public class WriteDbConnection
    {
        public const string DefaultDbName = "name=database";

        public WriteDbConnection(string nameOrConnectionString = DefaultDbName)
        {
            if (nameOrConnectionString == null)
                throw new ArgumentNullException(nameof(nameOrConnectionString));

            if (nameOrConnectionString.Length == 0)
                throw new ArgumentException($"{nameof(nameOrConnectionString)} cannot be Empty");

            Connection = new SqlConnection(RetriveConnectionString(nameOrConnectionString));
        }

        public IDbConnection Connection { get; protected set; }

        private string RetriveConnectionString(string nameOrConnectionString)
        {
            return nameOrConnectionString.StartsWith("name=") 
                ? GetConnectionStringFromConfig(nameOrConnectionString.Substring(5)) 
                : nameOrConnectionString;
        }

        private string GetConnectionStringFromConfig(string connectionStringName)
        {
            return ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
        }
    }
}