﻿using System;

namespace Framework.Testing.Data.Builders
{
    public abstract class DataBuilder<TContextBuilder, TContextRouter> : DataBuilder<TContextBuilder>
        where TContextBuilder : class, new()
        where TContextRouter : class
    {
        protected DataBuilder(WriteDbConnection dbConnection) 
            : base(dbConnection)
        {
            InContext = (TContextRouter)Activator.CreateInstance(typeof(TContextRouter), Actions, this);
        }

        public TContextRouter InContext { get; }
    }
}