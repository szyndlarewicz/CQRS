﻿using System.Collections.Generic;

namespace Framework.Testing.Data.Builders
{
    public abstract class ContextRouter<T>
    {
        protected ContextRouter(Queue<BuilderCommand> actions, T builder)
        {
            Database = builder;
        }

        public T Database { get; }
    }
}