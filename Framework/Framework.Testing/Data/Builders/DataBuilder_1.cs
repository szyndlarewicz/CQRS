﻿using System.Collections.Generic;
using Dapper;

namespace Framework.Testing.Data.Builders
{
    public abstract class DataBuilder<TContextBuilder> 
        where TContextBuilder : class, new()
    {
        protected DataBuilder(WriteDbConnection dbConnection)
        {
            DbConnection = dbConnection;
            Actions = new Queue<BuilderCommand>();
        }

        public static TContextBuilder Instance => new TContextBuilder();

        protected Queue<BuilderCommand> Actions { get; }

        private WriteDbConnection DbConnection { get; }

        public abstract TContextBuilder CleanUp();

        public void Build()
        {
            while (Actions.Count > 0)
            {
                BuilderCommand command = Actions.Dequeue();
                DbConnection.Connection.Execute(command.SqlCommand, command.Parameters);
            }
        }
    }
}