﻿namespace Framework.Testing.Data.Builders
{
    public sealed class BuilderCommand
    {
        public BuilderCommand(string sqlCommand, object parameters = null)
        {
            SqlCommand = sqlCommand;
            Parameters = parameters;
        }

        public string SqlCommand { get; }

        public object Parameters { get; }
    }
}