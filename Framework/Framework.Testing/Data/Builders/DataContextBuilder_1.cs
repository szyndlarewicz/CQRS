﻿using System.Collections.Generic;

namespace Framework.Testing.Data.Builders
{
    public abstract class DataContextBuilder<TContextRouter>
        where TContextRouter : class
    {
        protected DataContextBuilder(Queue<BuilderCommand> actions, TContextRouter router)
        {
            Actions = actions;
            InContext = router;
        }

        public TContextRouter InContext { get; }

        protected Queue<BuilderCommand> Actions { get; }
    }
}