﻿using System;

namespace Framework.Testing.Data.Readers
{
    public abstract class DataReader
    {
        protected DataReader()
            : this(new WriteDbConnection("name=database"))
        {
        }

        protected DataReader(WriteDbConnection dbConnection)
        {
            DbConnection = dbConnection ?? throw new ArgumentNullException(nameof(dbConnection));
        }

        protected WriteDbConnection DbConnection { get; }
    }
}