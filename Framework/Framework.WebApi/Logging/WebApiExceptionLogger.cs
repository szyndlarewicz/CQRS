﻿using System.Web.Http.ExceptionHandling;
using Framework.Core.Logging;

namespace Framework.WebApi.Logging
{
    public sealed class WebApiExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            string name = context.ExceptionContext?.ControllerContext.Controller.GetType().Name;
            ILogger logger = LoggerProvider.Current.GetLogger(name);
            logger.Log((LogLevel)LogLevel.Error, context.Exception);
        }
    }
}