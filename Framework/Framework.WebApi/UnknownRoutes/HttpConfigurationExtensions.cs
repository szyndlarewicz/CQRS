﻿using System.Web.Http;

namespace Framework.WebApi.UnknownRoutes
{
    public static class HttpConfigurationExtensions
    {
        public static void MapUnknownRoutes(this HttpConfiguration config)
        {
            config.Routes.MapHttpRoute(
                name: "CatchAllRoute",
                routeTemplate: "{*catchall}",
                defaults: new { controller = "UnknownRouteApi", action = "Catch" });
        }
    }
}