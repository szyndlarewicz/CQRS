﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Framework.WebApi.UnknownRoutes
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public sealed class UnknownRouteApiController : ApiController
    {
        [HttpDelete, HttpGet, HttpHead, HttpOptions, HttpPost, HttpPatch, HttpPut]
        public HttpResponseMessage Catch()
        {
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}