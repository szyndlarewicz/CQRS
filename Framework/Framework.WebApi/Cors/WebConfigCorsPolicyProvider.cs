﻿using System.Configuration;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http.Cors;

namespace Framework.WebApi.Cors
{
    public sealed class WebConfigCorsPolicyProvider : ICorsPolicyProvider
    {
        public const string DefaultCorsSectionName = "corsPolicy";

        public async Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var section = (CorsPolicyConfigurationSection) ConfigurationManager.GetSection(DefaultCorsSectionName);
            var corsPolicy = new CorsPolicy
            {
                AllowAnyHeader = section.AllowAnyHeader,
                AllowAnyMethod = section.AllowAnyMethod,
                AllowAnyOrigin = section.AllowAnyOrigin,
                SupportsCredentials = section.SupportsCredentials,
                PreflightMaxAge = section.PreflightMaxAge
            };

            foreach (ExposedHeaderConfigurationElement exposedHeader in section.ExposedHeaders)
                corsPolicy.ExposedHeaders.Add(exposedHeader.Value);

            foreach (HeaderConfigurationElement header in section.Headers)
                corsPolicy.ExposedHeaders.Add(header.Url);

            foreach (MethodConfigurationElement method in section.Methods)
                corsPolicy.ExposedHeaders.Add(method.Value);

            foreach (OriginConfigurationElement origin in section.Origins)
                corsPolicy.ExposedHeaders.Add(origin.Value);

            return await Task.FromResult(corsPolicy);
        }
    }
}