﻿using System.Configuration;

namespace Framework.WebApi.Cors
{
    public sealed class CorsPolicyConfigurationSection : ConfigurationSection
    {
        private const string AllowAnyHeaderAttributeName = "allowAnyHeader";
        private const string AllowAnyMethodAttributeName = "allowAnyMethod";
        private const string AllowAnyOriginAttributeName = "allowAnyOrigin";
        private const string SupportsCredentialsAttributeName = "supportsCredentials";
        private const string PreflightMaxAgeAttributeName = "preflightMaxAge";

        private const string ExposedHeadersTagName = "exposedHeaders";
        private const string HeadersTagName = "headers";
        private const string MethodsTagName = "methods";
        private const string OriginsTagName = "origins";

        [ConfigurationProperty(AllowAnyHeaderAttributeName, DefaultValue = "true", IsRequired = true)]
        public bool AllowAnyHeader
        {
            get => (bool)this[AllowAnyHeaderAttributeName];
            set => this[AllowAnyHeaderAttributeName] = value;
        }

        [ConfigurationProperty(AllowAnyMethodAttributeName, DefaultValue = "true", IsRequired = true)]
        public bool AllowAnyMethod
        {
            get => (bool)this[AllowAnyMethodAttributeName];
            set => this[AllowAnyMethodAttributeName] = value;
        }

        [ConfigurationProperty(AllowAnyOriginAttributeName, DefaultValue = "true", IsRequired = true)]
        public bool AllowAnyOrigin
        {
            get => (bool)this[AllowAnyOriginAttributeName];
            set => this[AllowAnyOriginAttributeName] = value;
        }

        [ConfigurationProperty(SupportsCredentialsAttributeName, DefaultValue = "true", IsRequired = true)]
        public bool SupportsCredentials
        {
            get => (bool)this[SupportsCredentialsAttributeName];
            set => this[SupportsCredentialsAttributeName] = value;
        }

        [ConfigurationProperty(PreflightMaxAgeAttributeName, IsRequired = false)]
        public long? PreflightMaxAge
        {
            get => (long?) this[PreflightMaxAgeAttributeName];
            set => this[PreflightMaxAgeAttributeName] = value;
        }

        [ConfigurationProperty(ExposedHeadersTagName)]
        public ExposedHeaderConfigurationElementCollection ExposedHeaders => (ExposedHeaderConfigurationElementCollection)base[ExposedHeadersTagName];

        [ConfigurationProperty(HeadersTagName)]
        public HeaderConfigurationElementCollection Headers => (HeaderConfigurationElementCollection)base[HeadersTagName];

        [ConfigurationProperty(MethodsTagName)]
        public MethodConfigurationElementCollection Methods => (MethodConfigurationElementCollection)base[MethodsTagName];

        [ConfigurationProperty(OriginsTagName)]
        public OriginConfigurationElementCollection Origins => (OriginConfigurationElementCollection)base[OriginsTagName];
    }
}