﻿using System.Configuration;

namespace Framework.WebApi.Cors
{
    public sealed class HeaderConfigurationElement : ConfigurationElement
    {
        private const string NameAttributeName = "name";
        private const string UrlAttributeName = "url";

        [ConfigurationProperty(NameAttributeName)]
        public string Name
        {
            get => (string)this[NameAttributeName];
            set => this[NameAttributeName] = value;
        }

        [ConfigurationProperty(UrlAttributeName)]
        public string Url
        {
            get => (string)this[UrlAttributeName];
            set => this[UrlAttributeName] = value;
        }
    }
}