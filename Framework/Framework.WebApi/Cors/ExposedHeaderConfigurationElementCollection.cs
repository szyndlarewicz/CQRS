﻿using System;
using System.Configuration;

namespace Framework.WebApi.Cors
{
    [ConfigurationCollection(typeof(ExposedHeaderConfigurationElement))]
    public sealed class ExposedHeaderConfigurationElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "exposedHeader";

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMapAlternate;
        protected override string ElementName => PropertyName;

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ExposedHeaderConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ExposedHeaderConfigurationElement)element).Name;
        }

        public ExposedHeaderConfigurationElement this[int index] => (ExposedHeaderConfigurationElement)BaseGet(index);
    }
}