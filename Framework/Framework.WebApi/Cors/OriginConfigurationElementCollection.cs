﻿using System;
using System.Configuration;

namespace Framework.WebApi.Cors
{
    [ConfigurationCollection(typeof(OriginConfigurationElement))]
    public sealed class OriginConfigurationElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "origin";

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMapAlternate;
        protected override string ElementName => PropertyName;

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new OriginConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((OriginConfigurationElement)element).Name;
        }

        public OriginConfigurationElement this[int index] => (OriginConfigurationElement)BaseGet(index);
    }
}