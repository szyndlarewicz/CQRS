﻿using System;
using System.Configuration;

namespace Framework.WebApi.Cors
{
    [ConfigurationCollection(typeof(HeaderConfigurationElement))]
    public sealed class HeaderConfigurationElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "header";

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMapAlternate;
        protected override string ElementName => PropertyName;

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName, StringComparison.InvariantCultureIgnoreCase);
        }
        
        public override bool IsReadOnly()
        {
            return false;
        }
        
        protected override ConfigurationElement CreateNewElement()
        {
            return new HeaderConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((HeaderConfigurationElement)element).Name;
        }

        public HeaderConfigurationElement this[int index] => (HeaderConfigurationElement)BaseGet(index);
    }
}