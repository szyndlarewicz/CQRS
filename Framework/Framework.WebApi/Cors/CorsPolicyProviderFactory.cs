﻿using System.Net.Http;
using System.Web.Http.Cors;

namespace Framework.WebApi.Cors
{
    public sealed class CorsPolicyProviderFactory : ICorsPolicyProviderFactory
    {
        public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
        {
            return new WebConfigCorsPolicyProvider();
        }
    }
}