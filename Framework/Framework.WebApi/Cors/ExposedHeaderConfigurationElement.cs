﻿using System.Configuration;

namespace Framework.WebApi.Cors
{
    public sealed class ExposedHeaderConfigurationElement : ConfigurationElement
    {
        private const string NameAttributeName = "name";
        private const string ValueAttributeName = "value";

        [ConfigurationProperty(NameAttributeName)]
        public string Name
        {
            get => (string)this[NameAttributeName];
            set => this[NameAttributeName] = value;
        }

        [ConfigurationProperty(ValueAttributeName)]
        public string Value
        {
            get => (string)this[ValueAttributeName];
            set => this[ValueAttributeName] = value;
        }
    }
}