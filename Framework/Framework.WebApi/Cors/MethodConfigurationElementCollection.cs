﻿using System;
using System.Configuration;

namespace Framework.WebApi.Cors
{
    [ConfigurationCollection(typeof(MethodConfigurationElement))]
    public sealed class MethodConfigurationElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "method";

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMapAlternate;
        protected override string ElementName => PropertyName;

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName, StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new MethodConfigurationElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((MethodConfigurationElement)element).Name;
        }

        public MethodConfigurationElement this[int index] => (MethodConfigurationElement)BaseGet(index);
    }
}