﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web;
using Framework.Core.Authentication;

namespace Framework.WebApi.Authentication.Principal
{
    public sealed class HttpCurrentUser : ICurrentUser
    {
        public Guid UserId 
        {
            get
            {
                ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
                Claim nameIdentifier = principal?.FindFirst(c => c.Type == ClaimTypes.NameIdentifier);
                if (nameIdentifier == null)
                    return Guid.Empty;

                return new Guid(nameIdentifier.Value);
            }
        }

        public bool IsInRole(string roleName)
        {
            ClaimsPrincipal principal = HttpContext.Current.User as ClaimsPrincipal;
            if (principal == null)
                return false;

            return principal.FindAll(c => c.Type == ClaimTypes.Role).Any(c => c.Value == roleName);
        }
    }
}