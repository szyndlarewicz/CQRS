﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.Filters;
using Framework.Cqrs.Commands;

namespace Framework.WebApi.Cqrs
{
    public sealed class SyncCqrsProcessingAttribute : ActionFilterAttribute
    {
        private readonly HttpMethod[] _httpMethods = { HttpMethod.Post, HttpMethod.Put, HttpMethod.Delete };

        public override void OnActionExecuted(HttpActionExecutedContext context)
        {
            if (!_httpMethods.Contains(context.Request.Method))
                return;

            IDependencyScope dependencyScope = context.Request.GetDependencyScope();
            var commandProcessor = (ICommandProcessor)dependencyScope.GetService(typeof(ICommandProcessor));
            commandProcessor.Process();

            var errorAggregator = (IRequestScopeExceptionAggregator)dependencyScope.GetService(typeof(IRequestScopeExceptionAggregator));
            if (!errorAggregator.Exceptions.Any())
                return;

            context.Response = context.Request.CreateResponse(
                HttpStatusCode.BadRequest,
                errorAggregator.Exceptions,
                context.Request.GetConfiguration()
            );
        }
    }
}