﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Framework.Cqrs.Commands;
using Framework.Cqrs.Queries;

namespace Framework.WebApi.Cqrs
{
    [SyncCqrsProcessing]
    public abstract class SyncCqrsApiController : ApiController
    {
        protected readonly ICommandPublisher CommandPublisher;
        protected readonly IQueryDispatcher QueryDispatcher;

        protected SyncCqrsApiController(ICommandPublisher commandPublisher, IQueryDispatcher queryDispatcher)
        {
            CommandPublisher = commandPublisher ?? throw new ArgumentNullException(nameof(commandPublisher));
            QueryDispatcher = queryDispatcher ?? throw new ArgumentNullException(nameof(queryDispatcher));
        }

        protected virtual HttpResponseMessage Info(INotification notification)
        {
            return notification == null || notification.IsValid
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateResponse(HttpStatusCode.BadRequest, notification.Errors);
        }

        protected virtual HttpResponseMessage View<T>(T result)
        {
            return Request.CreateResponse<T>(HttpStatusCode.OK, result);
        }
    }
}