﻿using System;
using Framework.Core.Logging;
using Framework.Cqrs;

namespace Framework.WebApi.Cqrs
{
    public sealed class LoggingExceptionHandler : IExceptionHandler
    {
        private readonly ILogger _logger;

        public LoggingExceptionHandler(ILogger logger)
        {
            _logger = logger ?? throw new ArgumentException($"{nameof(logger)} cannot be Empty");
        }

        public void Handle(Exception exception)
        {
            _logger.Log((LogLevel)LogLevel.Error, exception);
        }
    }
}