using System;
using Framework.Cqrs;

namespace Framework.WebApi.Cqrs
{
    public sealed class CqrsExceptionHandler : IExceptionHandler
    {
        private readonly IRequestScopeExceptionAggregator _exceptionAggregator;

        public CqrsExceptionHandler(IRequestScopeExceptionAggregator exceptionAggregator)
        {
            _exceptionAggregator = exceptionAggregator ?? throw new ArgumentNullException(nameof(exceptionAggregator));
        }

        public void Handle(Exception exception)
        {
            _exceptionAggregator.Add(exception);
        }
    }
}