using System;
using System.Collections.Generic;

namespace Framework.WebApi.Cqrs
{
    public interface IRequestScopeExceptionAggregator
    {
        IReadOnlyCollection<Exception> Exceptions { get; }

        void Add(Exception exception);

        void Clear();
    }
}