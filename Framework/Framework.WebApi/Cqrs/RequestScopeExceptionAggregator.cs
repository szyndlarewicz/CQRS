﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Framework.WebApi.Cqrs
{
    public sealed class RequestScopeExceptionAggregator : IRequestScopeExceptionAggregator
    {
        private readonly IList<Exception> _exceptions = new List<Exception>();

        public IReadOnlyCollection<Exception> Exceptions => new ReadOnlyCollection<Exception>(_exceptions);

        public void Add(Exception exception)
        {
            _exceptions.Add(exception);
        }

        public void Clear()
        {
            _exceptions.Clear();
        }
    }
}