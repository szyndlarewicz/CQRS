﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Framework.EventSourcing;
using Framework.Testing;
using Framework.Testing.Data.Readers;

namespace Framework.Tests.EventSourcing.Data
{
    public sealed class EventStoreDataReader : DataReader
    {
        public EventStoreDataReader()
            : base(new WriteDbConnection("name=eventstore"))
        {
        }

        public static EventStoreDataReader Instance => new EventStoreDataReader();

        public ICollection<EventRecord> GetEvents(Guid aggregateId)
        {
            const string sql = "SELECT * FROM dbo.Events WHERE AggregateId = @AggregateId ORDER BY AggregateVersion ASC";
            var parameters = new { AggregateId = aggregateId };

            return DbConnection.Connection.Query<EventRecord>(sql, parameters).ToList();
        }
    }
}