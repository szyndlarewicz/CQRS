﻿using Framework.Testing;
using Framework.Testing.Data.Builders;

namespace Framework.Tests.EventSourcing.Data.Builders
{
    public sealed class EventStoreDataBuilder : DataBuilder<EventStoreDataBuilder, EventStoreContextRouter>
    {
        public EventStoreDataBuilder()
            : base(new WriteDbConnection("name=eventstore"))
        {
        }

        public override EventStoreDataBuilder CleanUp()
        {
            Actions.Enqueue(new BuilderCommand("delete from dbo.Events"));
            return this;
        }
    }
}