﻿using System.Collections.Generic;
using Framework.Testing.Data.Builders;

namespace Framework.Tests.EventSourcing.Data.Builders
{
    public sealed class EventStoreContextRouter : ContextRouter<EventStoreDataBuilder>
    {
        public EventStoreContextRouter(Queue<BuilderCommand> actions, EventStoreDataBuilder builder) 
            : base(actions, builder)
        {
        }
    }
}