﻿using System;
using Framework.Domain;
using Framework.EventSourcing;
using Framework.Testing;
using Framework.Testing.Data.Builders;

namespace Framework.Tests.EventSourcing.Data
{
    public sealed class EventStoreDataBuilder : DataBuilder<EventStoreDataBuilder>
    {
        public EventStoreDataBuilder()
            : base(new WriteDbConnection("name=eventstore"))
        {
        }

        public EventStoreDataBuilder(WriteDbConnection dbConnection)
            : base(dbConnection)
        {
        }

        public override EventStoreDataBuilder CleanUp()
        {
            Actions.Enqueue(new BuilderCommand("DELETE FROM dbo.Events"));
            return this;
        }

        public EventStoreDataBuilder CreateEvent(DateTime createdAt, Guid createdBy, Guid aggregateId, long version, Guid aggregateHash, IEvent @event)
        {
            const string sql = "INSERT INTO dbo.Events (AggregateId, AggregateVersion, IsSnapshot, CreatedAt, CreatedBy, AggregateHash, EventData) " +
                               "VALUES (@AggregateId, @AggregateVersion, @IsSnapshot, @CreatedAt, @CreatedBy, @AggregateHash, @EventData)";

            var parameters = new
            {
                CreatedAt = createdAt,
                CreatedBy = createdBy,
                AggregateId = aggregateId,
                AggregateVersion = version,
                IsSnapshot = @event is ISnapshot,
                AggregateHash = aggregateHash,
                EventData = new JsonEventSerializer().Serialize(@event)
            };

            Actions.Enqueue(new BuilderCommand(sql, parameters));
            return this;
        }
    }
}