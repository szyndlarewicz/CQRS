﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using Dapper;
using Framework.Testing;
using Framework.Testing.Data.Readers;

namespace Framework.Tests.EventSourcing.Data.Readers
{
    public sealed class EventStoreDataReader : DataReader
    {
        public EventStoreDataReader()
            : base(new WriteDbConnection("name=eventstore"))
        {
        }

        public ICollection<EventRecord> GetEvents(Guid aggregateId, long aggregateVersion)
        {
            const string sql = "SELECT * FROM dbo.Events WHERE AggregateId = @AggregateId AND AggregateVersion <= @AggregateVersion";
            var parameters = new { AggregateId = aggregateId, AggregateVersion = aggregateVersion };

            return DbConnection.Connection.Query<EventRecord>(sql, parameters).ToList();
        }
    }
}
