﻿using System;
using System.Linq;
using Framework.Core.Authentication;
using Framework.Core.Providers;
using Framework.Domain;
using Framework.EventSourcing;
using Framework.EventSourcing.EntityFramework;
using Framework.Testing.Mocks;
using Framework.Testing.Providers;
using Framework.Tests.EventSourcing.Data;
using Framework.Tests.EventSourcing.Models;
using Framework.Tests.EventSourcing.Models.Events;
using Framework.Tests.EventSourcing.Models.Types;
using NUnit.Framework;

namespace Framework.Tests.EventSourcing
{
    [TestFixture]
    public class When_storing_event
    {
        private StaticDateTimeProvider _timeProvider;
        private IEventSerializer _eventSerializer;
        private IEventStore _eventStore;
        private ICurrentUser _currentUser;
        private EventRecord _eventRecord;

        [SetUp]
        public void SetUp()
        {
            DateTimeProvider.Current = _timeProvider = new StaticDateTimeProvider();

            _eventStore = EventStoreBuilder.Instance
                .Use(_currentUser = new FakeCurrentUser(Guid.NewGuid()))
                .Use(_eventSerializer = new JsonEventSerializer())
                .UseEntityFramework()
                .CreateDatabase()
                .Build();

            EventStoreDataBuilder.Instance
                .CleanUp()
                .Build();

            _timeProvider.Change("2017-01-01 10:00:00");
            var task = new TaskEntity((EntityIdentity)"9f8e7539-3167-4369-b669-aab9a4bf7d4f", (TaskName)"Name A", (TaskDesription)"Desription A");
            _eventStore.Save(task);

            _eventRecord = EventStoreDataReader.Instance
                .GetEvents((EntityIdentity)"9f8e7539-3167-4369-b669-aab9a4bf7d4f").First();
        }

        [TestCase]
        public void It_should_set_aggregate_id()
        {
            Assert.That(_eventRecord.AggregateId, Is.EqualTo(new Guid("9f8e7539-3167-4369-b669-aab9a4bf7d4f")));
        }

        [TestCase]
        public void It_should_set_aggregate_hash()
        {
            Assert.That(_eventRecord.AggregateHash, Is.EqualTo((Guid)AggregateHash.New<TaskEntity>()));
        }

        [TestCase]
        public void It_should_set_new_aggregate_version()
        {
            Assert.That(_eventRecord.AggregateVersion, Is.EqualTo(1));
        }

        [TestCase]
        public void It_should_set_created_date()
        {
            Assert.That(_eventRecord.CreatedAt, Is.EqualTo(DateTime.Parse("2017-01-01 10:00:00")));
        }

        [TestCase]
        public void It_should_set_event_as_type()
        {
            Assert.That(_eventRecord.IsSnapshot, Is.EqualTo(false));
        }

        [TestCase]
        public void It_should_store_event_data()
        {
            Assert.That(_eventSerializer.Deserialize(_eventRecord.EventData).GetType(), Is.EqualTo(typeof(TaskCreatedEvent)));
        }

        [TestCase]
        public void It_should_store_current_user_id()
        {
            Assert.That(_eventRecord.CreatedBy, Is.EqualTo(_currentUser.UserId));
        }
    }
}