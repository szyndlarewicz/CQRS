﻿using System;
using Framework.Core.Validation;
using Framework.Domain;

namespace Framework.Tests.EventSourcing.Models.Types
{
    public sealed class TaskDesription : IEquatable<TaskDesription>, IValueObject
    {
        private readonly string _value;

        public TaskDesription(string taskDesription)
        {
            Guard.NotNullOrEmpty(() => taskDesription, taskDesription);

            _value = taskDesription;
        }

        public static explicit operator TaskDesription(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new TaskDesription(instance);
        }

        public static implicit operator string(TaskDesription instance)
        {
            return instance?._value;
        }

        public static bool operator ==(TaskDesription x, TaskDesription y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(TaskDesription x, TaskDesription y)
        {
            return !(x == y);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TaskDesription);
        }

        public bool Equals(TaskDesription other)
        {
            return !ReferenceEquals(null, other) && string.Equals(_value, other._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}
