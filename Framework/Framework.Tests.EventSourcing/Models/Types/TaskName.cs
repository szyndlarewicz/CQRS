﻿using System;
using Framework.Core.Validation;
using Framework.Domain;

namespace Framework.Tests.EventSourcing.Models.Types
{
    public sealed class TaskName : IEquatable<TaskName>, IValueObject
    {
        private readonly string _value;

        public TaskName(string taskDesription)
        {
            Guard.NotNullOrEmpty(() => taskDesription, taskDesription);

            _value = taskDesription;
        }

        public static explicit operator TaskName(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new TaskName(instance);
        }

        public static implicit operator string(TaskName instance)
        {
            return instance?._value;
        }

        public static bool operator ==(TaskName x, TaskName y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(TaskName x, TaskName y)
        {
            return !(x == y);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TaskName);
        }

        public bool Equals(TaskName other)
        {
            return !ReferenceEquals(null, other) && string.Equals(_value, other._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}
