﻿using System;
using Framework.Domain;

namespace Framework.Tests.EventSourcing.Models.Events
{
    public sealed class TaskNameChangedEvent : Event
    {
        public TaskNameChangedEvent(Guid aggregateId, string name) 
            : base(aggregateId)
        {
            Name = name;
        }

        public string Name { get; }
    }
}