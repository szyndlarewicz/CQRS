﻿using System;
using Framework.Domain;
using Framework.EventSourcing;

namespace Framework.Tests.EventSourcing.Models.Events
{
    public sealed class TaskSnapshotCreatedEvent : Event, ISnapshot
    {
        public TaskSnapshotCreatedEvent(Guid aggregateId, long version, string name, string description) 
            : base(aggregateId)
        {
            Version = version;
            Name = name;
            Description = description;
        }

        public string Name { get; }

        public string Description { get; }

        public long Version { get; }
    }
}
