﻿using System;
using Framework.Domain;

namespace Framework.Tests.EventSourcing.Models.Events
{
    public sealed class TaskDescriptionChangedEvent : Event
    {
        public TaskDescriptionChangedEvent(Guid aggregateId, string description)
            : base(aggregateId)
        {
            Description = description;
        }

        public string Description { get; }
    }
}