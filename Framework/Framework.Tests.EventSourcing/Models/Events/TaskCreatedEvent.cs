﻿using System;
using Framework.Domain;

namespace Framework.Tests.EventSourcing.Models.Events
{
    public sealed class TaskCreatedEvent : Event
    {
        public TaskCreatedEvent(Guid aggregateId, string name, string description) 
            : base(aggregateId)
        {
            Name = name;
            Description = description;
        }

        public string Name { get; }

        public string Description { get; }
    }
}