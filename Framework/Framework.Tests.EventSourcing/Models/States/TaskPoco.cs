﻿using System;

namespace Framework.Tests.EventSourcing.Models.States
{
    public class TaskPoco
    {
        public Guid Id { get; set; }

        public long Version { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}