﻿using System.Collections.Generic;
using Framework.Core.Validation;
using Framework.Domain;
using Framework.Tests.EventSourcing.Models.Events;
using Framework.Tests.EventSourcing.Models.States;
using Framework.Tests.EventSourcing.Models.Types;

namespace Framework.Tests.EventSourcing.Models
{
    public sealed class TaskEntity : Framework.EventSourcing.AggregateRoot<TaskPoco>, IAggregateRoot
    {
        public TaskEntity(TaskPoco state) 
            : base(state)
        {
        }

        public TaskEntity(IEnumerable<IEvent> events) 
            : base(new TaskPoco())
        {
            Apply(events);
        }

        public TaskEntity(EntityIdentity id, TaskName name, TaskDesription description)
            : base(new TaskPoco())
        {
            State.Id = id;
            State.Version = 0;

            State.Name = name;
            State.Description = description;

            Enqueue(new TaskCreatedEvent(State.Id, State.Name, State.Description));
        }

        public override EntityIdentity Id
        {
            get => (EntityIdentity) State.Id;
            protected set => State.Id = value;
        }

        public override AggregateVersion Version
        {
            get => (AggregateVersion) State.Version;
            protected set => State.Version = value;
        }

        public TaskName Name => (TaskName) State.Name;

        public TaskDesription Desription =>  (TaskDesription) State.Description;

        public void ChangeName(TaskName name)
        {
            Guard.NotNull(() => name, name);

            State.Name = (string) name;
            Enqueue(new TaskNameChangedEvent(State.Id, State.Name));
        }

        public void ChangeDescription(TaskDesription description)
        {
            State.Description = description == null ? null : (string)description;
            Enqueue(new TaskDescriptionChangedEvent(State.Id, State.Description));
        }

        private void ApplyChange(TaskCreatedEvent @event)
        {
            State = new TaskPoco
            {
                Id = @event.AggregateId,
                Version = 1,
                Name = @event.Name,
                Description = @event.Description
            };
        }

        private void ApplyChange(TaskNameChangedEvent @event)
        {
            State.Name = @event.Name;
            State.Version += 1;
        }

        private void ApplyChange(TaskDescriptionChangedEvent @event)
        {
            State.Description = @event.Description;
            State.Version += 1;
        }

        private void ApplyChange(TaskSnapshotCreatedEvent @event)
        {
            State = new TaskPoco
            {
                Id = @event.AggregateId,
                Version = @event.Version,
                Name = @event.Name,
                Description = @event.Description
            };
        }

        public override void Snapshot()
        {
            Enqueue(new TaskSnapshotCreatedEvent(State.Id, State.Version, State.Name, State.Description));
        }
    }
}