﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Core.Authentication;
using Framework.Domain;
using Framework.EventSourcing;
using Framework.EventSourcing.EntityFramework;
using Framework.Testing.Mocks;
using Framework.Tests.EventSourcing.Data;
using Framework.Tests.EventSourcing.Models;
using Framework.Tests.EventSourcing.Models.Events;
using NUnit.Framework;

namespace Framework.Tests.EventSourcing
{
    public sealed class When_retrieving_events_with_snapshot
    {
        private readonly Guid _taskId = EntityIdentity.New;
        private IEventStore _eventStore;
        private ICurrentUser _currentUser;

        [SetUp]
        public void SetUp()
        {
            _eventStore = EventStoreBuilder.Instance
                .Use(_currentUser = new FakeCurrentUser(Guid.NewGuid()))
                .UseJsonSerializer()
                .UseEntityFramework()
                .CreateDatabase()
                .Build();

            Guid otherTaskId = Guid.NewGuid();
            Guid taskHash = AggregateHash.New<TaskEntity>();

            IList<IEvent> events = new List<IEvent>
            {
                new TaskCreatedEvent(_taskId, "Name A", "Description A"),
                new TaskNameChangedEvent(_taskId, "Name B"),
                new TaskSnapshotCreatedEvent(_taskId, 2, "Name B", "Description A"),
                new TaskDescriptionChangedEvent(_taskId, "Description B"),
                new TaskNameChangedEvent(_taskId, "Name C"),
                new TaskSnapshotCreatedEvent(_taskId, 4, "Name C", "Description B"),
                new TaskNameChangedEvent(_taskId, "Name D"),
                new TaskCreatedEvent(otherTaskId, "Name X", "Description X"),
                new TaskSnapshotCreatedEvent(otherTaskId, 1, "Name X", "Description X")
            };

            EventStoreDataBuilder.Instance
                .CleanUp()
                .CreateEvent(DateTime.Parse("2017-01-01 10:05:00"), _currentUser.UserId, _taskId, 1, taskHash, events[0])
                .CreateEvent(DateTime.Parse("2017-01-01 10:10:00"), _currentUser.UserId, _taskId, 2, taskHash, events[1])
                .CreateEvent(DateTime.Parse("2017-01-01 10:15:00"), _currentUser.UserId, _taskId, 2, taskHash, events[2])
                .CreateEvent(DateTime.Parse("2017-01-01 10:20:00"), _currentUser.UserId, _taskId, 3, taskHash, events[3])
                .CreateEvent(DateTime.Parse("2017-01-01 10:25:00"), _currentUser.UserId, _taskId, 4, taskHash, events[4])
                .CreateEvent(DateTime.Parse("2017-01-01 10:30:00"), _currentUser.UserId, _taskId, 4, taskHash, events[5])
                .CreateEvent(DateTime.Parse("2017-01-01 10:35:00"), _currentUser.UserId, _taskId, 5, taskHash, events[6])
                .CreateEvent(DateTime.Parse("2017-01-01 10:15:00"), _currentUser.UserId, otherTaskId, 1, taskHash, events[7])
                .CreateEvent(DateTime.Parse("2017-01-01 10:20:00"), _currentUser.UserId, otherTaskId, 1, taskHash, events[8])
                .Build();
        }

        [TestCase]
        public void It_should_retrieve_snapshot_and_events_for_required_aggregate()
        {
            IList<IEvent> events = _eventStore.GetEvents((EntityIdentity)_taskId).ToList();

            Assert.That(events.Count, Is.EqualTo(2));
            Assert.That(events[0], Is.TypeOf(typeof(TaskSnapshotCreatedEvent)));
            Assert.That(events[1], Is.TypeOf(typeof(TaskNameChangedEvent)));
        }

        [TestCase]
        public void It_should_retrieve_snapshot_and_events_for_required_datetime()
        {
            IList<IEvent> events = _eventStore.GetEvents((EntityIdentity)_taskId, DateTime.Parse("2017-01-01 10:25:00")).ToList();

            Assert.That(events.Count, Is.EqualTo(3));
            Assert.That(events[0], Is.TypeOf(typeof(TaskSnapshotCreatedEvent)));
            Assert.That(events[1], Is.TypeOf(typeof(TaskDescriptionChangedEvent)));
            Assert.That(events[2], Is.TypeOf(typeof(TaskNameChangedEvent)));
        }

        [TestCase]
        public void It_should_retrive_snapshot_and_events_for_required_varsion()
        {
            IList<IEvent> events = _eventStore.GetEvents((EntityIdentity)_taskId, new AggregateVersion(3)).ToList();

            Assert.That(events.Count, Is.EqualTo(2));
            Assert.That(events[0], Is.TypeOf(typeof(TaskSnapshotCreatedEvent)));
            Assert.That(events[1], Is.TypeOf(typeof(TaskDescriptionChangedEvent)));
        }
    }
}