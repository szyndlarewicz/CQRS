﻿using System;
using System.Linq;
using Framework.Core.Authentication;
using Framework.Core.Providers;
using Framework.Domain;
using Framework.EventSourcing;
using Framework.EventSourcing.EntityFramework;
using Framework.Testing.Mocks;
using Framework.Testing.Providers;
using Framework.Tests.EventSourcing.Data;
using Framework.Tests.EventSourcing.Models;
using Framework.Tests.EventSourcing.Models.Events;
using Framework.Tests.EventSourcing.Models.Types;
using NUnit.Framework;

namespace Framework.Tests.EventSourcing
{
    [TestFixture]
    public class When_storing_snapshot
    {
        private StaticDateTimeProvider _timeProvider;
        private IEventSerializer _eventSerializer;
        private IEventStore _eventStore;
        private ICurrentUser _currentUser;
        private EventRecord _snapshotRecord;

        [SetUp]
        public void SetUp()
        {
            DateTimeProvider.Current = _timeProvider = new StaticDateTimeProvider();

            _eventStore = EventStoreBuilder.Instance
                .Use(_currentUser = new FakeCurrentUser(Guid.NewGuid()))
                .Use(_eventSerializer = new JsonEventSerializer())
                .UseEntityFramework()
                .CreateDatabase()
                .Build();

            EventStoreDataBuilder.Instance
                .CleanUp()
                .Build();

            _timeProvider.Change("2017-01-01 10:00:00");
            var task = new TaskEntity((EntityIdentity)"d27c44a9-9a77-46e7-8c54-b47310bffacf", (TaskName)"Name A", (TaskDesription)"Desription A");
            _eventStore.Save(task);

            _timeProvider.Change("2017-01-01 11:00:00");
            task.Snapshot();
            _eventStore.Save(task);

            _snapshotRecord = EventStoreDataReader.Instance
                .GetEvents((EntityIdentity)"d27c44a9-9a77-46e7-8c54-b47310bffacf")
                .ToList()
                .Last();
        }

        [TestCase]
        public void It_should_set_aggregate_id()
        {
            Assert.That(_snapshotRecord.AggregateId, Is.EqualTo(new Guid("d27c44a9-9a77-46e7-8c54-b47310bffacf")));
        }

        [TestCase]
        public void It_should_set_aggregate_hash()
        {
            Assert.That(_snapshotRecord.AggregateHash, Is.EqualTo((Guid)AggregateHash.New<TaskEntity>()));
        }

        [TestCase]
        public void It_should_set_version_equal_to_last_event_version()
        {
            Assert.That(_snapshotRecord.AggregateVersion, Is.EqualTo(1));
        }

        [TestCase]
        public void It_should_set_created_date()
        {
            Assert.That(_snapshotRecord.CreatedAt, Is.EqualTo(DateTime.Parse("2017-01-01 11:00:00")));
        }

        [TestCase]
        public void It_should_set_snapshot_as_type()
        {
            Assert.That(_snapshotRecord.IsSnapshot, Is.EqualTo(true));
        }

        [TestCase]
        public void It_should_store_event_data()
        {
            Assert.That(_eventSerializer.Deserialize(_snapshotRecord.EventData).GetType(), Is.EqualTo(typeof(TaskSnapshotCreatedEvent)));
        }

        [TestCase]
        public void It_should_store_current_user_id()
        {
            Assert.That(_snapshotRecord.CreatedBy, Is.EqualTo(_currentUser.UserId));
        }
    }
}