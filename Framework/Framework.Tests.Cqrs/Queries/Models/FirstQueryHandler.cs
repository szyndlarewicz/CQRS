﻿using Framework.Cqrs.Queries;

namespace Framework.Tests.Cqrs.Queries.Models
{
    public sealed class FirstQueryHandler : IQueryHandler<FirstQuery, FirstQueryView>
    {
        public FirstQueryView Execute(FirstQuery query)
        {
            return new FirstQueryView();
        }
    }
}