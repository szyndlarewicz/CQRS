﻿using System;
using Framework.Cqrs.Queries;
using Framework.IoC;
using Framework.Tests.Cqrs.Queries.Models;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Queries
{
    [TestFixture]
    public sealed class Tests_QueryDispatcher
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<IQueryHandlerRegistrar, QueryHandlerRegistrar>()
                .RegisterSingleton<IQueryDispatcher, QueryDispatcher>()
                .RegisterSingleton<IQueryHandler<FirstQuery, FirstQueryView>, FirstQueryHandler>()
                .Done();

            _resolver.Get<IQueryHandlerRegistrar>()
                .Register<FirstQuery, FirstQueryView, FirstQueryHandler>();
        }

        [TestCase]
        public void It_should_execute_query()
        {
            var query = new FirstQuery();

            var queryDispatcher = _resolver.Get<IQueryDispatcher>();
            FirstQueryView result = queryDispatcher.Execute<FirstQueryView>(query);
            Assert.IsNotNull(result);
        }

        [TestCase]
        public void It_should_throw_exception_when_query_is_undefined()
        {
            var queryDispatcher = _resolver.Get<IQueryDispatcher>();
            Assert.Throws<ArgumentNullException>(() => queryDispatcher.Execute<FirstQueryView>(null));
        }
    }
}