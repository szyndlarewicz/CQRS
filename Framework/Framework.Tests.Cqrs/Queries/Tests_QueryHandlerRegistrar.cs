﻿using System;
using Framework.Cqrs.Queries;
using Framework.IoC;
using Framework.Tests.Cqrs.Queries.Models;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Queries
{
    [TestFixture]
    public sealed class Tests_QueryHandlerRegistrar
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<IQueryHandlerRegistrar, QueryHandlerRegistrar>()
                .Done();

            _resolver.Get<IQueryHandlerRegistrar>()
                .Register<FirstQuery, FirstQueryView, FirstQueryHandler>();
        }

        [TestCase]
        public void It_should_register_query_handler()
        {
            var queryHandlerRegistrar = _resolver.Get<IQueryHandlerRegistrar>();
            Type queryHandlerType = queryHandlerRegistrar.GetQueryHandler(new FirstQuery());
            Assert.AreEqual(queryHandlerType, typeof(FirstQueryHandler));
        }

        [TestCase]
        public void It_should_throw_exception_when_re_registering_query()
        {
            var queryHandlerRegistrar = _resolver.Get<IQueryHandlerRegistrar>();
            Assert.Throws<QueryHandlerRegisteredException>(() => queryHandlerRegistrar.Register<FirstQuery, FirstQueryView, FirstQueryHandler>());
        }
        
        [TestCase]
        public void It_should_throw_exception_when_query_handler_is_not_registered_yet()
        {
            var queryHandlerRegistrar = _resolver.Get<IQueryHandlerRegistrar>();
            Assert.Throws<QueryHandlerNotFoundException>(() => queryHandlerRegistrar.GetQueryHandler(new SecondQuery()));
        }
    }
}