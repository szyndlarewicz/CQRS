﻿using Framework.IoC;
using Framework.IoC.Ninject;
using Framework.Testing.DI;

namespace Framework.Tests.Cqrs
{
    public sealed class TestDependencyScope : NinjectDependencyScope<TestDependencyScope>
    {
        public TestDependencyScope RegisterDependencyResolver()
        {
            RegisterSingleton<IDependencyResolver, NinjectDependencyResolver>();
            return this;
        }
    }
}