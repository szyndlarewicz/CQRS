﻿using System.Linq;
using Framework.Cqrs;
using Framework.Cqrs.Commands;
using Framework.IoC;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_CommandDispatcher
    {
        private IDependencyRegister _register;

        [SetUp]
        public void SetUp()
        {
            _register = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<IAmbientScopeFactory, FakeAmbientScopeFactory>()
                .RegisterSingleton<IExceptionHandler, NullExceptionHandler>()
                .RegisterSingleton<ICommandHandlerRegistrar, CommandHandlerRegistrar>()
                .RegisterSingleton<ICommandDispatcher, CommandDispatcher>()
                .RegisterSingleton<ExecuteCommandCollector>();
        }

        [TestCase]
        public void It_should_execute_command()
        {
            IDependencyResolver resolver = _register
                .RegisterSingleton<ICommandHandler<FirstCommand>, PositiveFirstCommandHandler>()
                .Done();

            resolver.Get<ICommandHandlerRegistrar>()
                .Register<FirstCommand, PositiveFirstCommandHandler>();

            var command = new FirstCommand();
            var commandDispatcher = resolver.Get<ICommandDispatcher>();
            commandDispatcher.Execute(command);

            var executeCommandCollector = resolver.Get<ExecuteCommandCollector>();
            Assert.IsTrue(executeCommandCollector.ExecutedCommands.Contains(command));
        }

        [TestCase]
        public void It_should_suppress_all_exceptions()
        {
            IDependencyResolver resolver = _register
                .RegisterSingleton<ICommandHandler<FirstCommand>, NegativeFirstCommandHandler>()
                .Done();

            resolver.Get<ICommandHandlerRegistrar>()
                .Register<FirstCommand, PositiveFirstCommandHandler>();

            var command = new FirstCommand();
            var commandDispatcher = resolver.Get<ICommandDispatcher>();
            Assert.DoesNotThrow(() => commandDispatcher.Execute(command));

            var executeCommandCollector = resolver.Get<ExecuteCommandCollector>();
            Assert.IsTrue(executeCommandCollector.ExecutedCommands.Contains(command));
        }
    }
}