﻿using System;
using Framework.Cqrs.Commands;
using Framework.IoC;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_CommandHandlerRegistrar
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<ICommandHandlerRegistrar, CommandHandlerRegistrar>()
                .Done();

            _resolver.Get<ICommandHandlerRegistrar>()
                .Register<FirstCommand, PositiveFirstCommandHandler>();
        }

        [TestCase]
        public void It_should_register_command_handler()
        {
            var commandHandlerRegistrar = _resolver.Get<ICommandHandlerRegistrar>();
            Type commandHandlerType = commandHandlerRegistrar.GetCommandHandler(new FirstCommand());
            Assert.AreEqual(commandHandlerType, typeof(PositiveFirstCommandHandler));
        }

        [TestCase]
        public void It_should_throw_exception_when_re_registering_command()
        {
            var commandHandlerRegistrar = _resolver.Get<ICommandHandlerRegistrar>();
            Assert.Throws<CommandHandlerRegisteredException>(() => commandHandlerRegistrar.Register<FirstCommand, PositiveFirstCommandHandler>());
        }

        [TestCase]
        public void It_should_throw_exception_when_command_handler_is_not_registered_yet()
        {
            var commandHandlerRegistrar = _resolver.Get<ICommandHandlerRegistrar>();
            Assert.Throws<CommandHandlerNotFoundException>(() => commandHandlerRegistrar.GetCommandHandler(new SecondCommand()));
        }
    }
}