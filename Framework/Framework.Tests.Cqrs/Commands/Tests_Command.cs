﻿using System;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_Command
    {
        [TestCase]
        public void It_should_create_command_with_new_id()
        {
            var command = new FirstCommand();
            Assert.AreNotEqual(Guid.Empty, command.CommandId);
        }
    }
}