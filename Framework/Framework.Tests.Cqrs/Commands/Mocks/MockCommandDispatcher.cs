﻿using System.Collections.Generic;
using Framework.Cqrs.Commands;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class MockCommandDispatcher : ICommandDispatcher
    {
        public MockCommandDispatcher()
        {
            ExecutedCommands = new List<ICommand>();
        }

        public ICollection<ICommand> ExecutedCommands { get; }

        public void Execute(ICommand command)
        {
            ExecutedCommands.Add(command);
        }
    }
}