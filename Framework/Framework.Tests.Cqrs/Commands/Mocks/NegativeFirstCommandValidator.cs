﻿using Framework.Cqrs.Commands;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class NegativeFirstCommandValidator : ICommandValidator<FirstCommand>
    {
        public INotification Validate(FirstCommand command)
        {
            var notification = new Notification();
            notification.Add("Error message");
            return notification;
        }
    }
}