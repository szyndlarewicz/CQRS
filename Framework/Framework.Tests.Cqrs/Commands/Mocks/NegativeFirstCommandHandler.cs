﻿using System;
using Framework.Cqrs.Commands;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class NegativeFirstCommandHandler : ICommandHandler<FirstCommand>
    {
        private readonly ExecuteCommandCollector _executedCommandCollection;

        public NegativeFirstCommandHandler(ExecuteCommandCollector executedCommandCollection)
        {
            _executedCommandCollection = executedCommandCollection;
        }

        public void Execute(FirstCommand command)
        {
            _executedCommandCollection.Add(command);
            throw new Exception("Something went wrong");
        }
    }
}