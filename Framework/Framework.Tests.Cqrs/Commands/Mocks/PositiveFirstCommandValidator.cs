﻿using Framework.Cqrs.Commands;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class PositiveFirstCommandValidator : ICommandValidator<FirstCommand>
    {
        public INotification Validate(FirstCommand command)
        {
           return new Notification();
        }
    }
}