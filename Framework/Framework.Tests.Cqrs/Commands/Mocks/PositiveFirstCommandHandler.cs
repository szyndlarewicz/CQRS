﻿using Framework.Cqrs.Commands;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class PositiveFirstCommandHandler : ICommandHandler<FirstCommand>
    {
        private readonly ExecuteCommandCollector _executedCommandCollection;

        public PositiveFirstCommandHandler(ExecuteCommandCollector executedCommandCollection)
        {
            _executedCommandCollection = executedCommandCollection;
        }

        public void Execute(FirstCommand command)
        {
            _executedCommandCollection.Add(command);
        }
    }
}