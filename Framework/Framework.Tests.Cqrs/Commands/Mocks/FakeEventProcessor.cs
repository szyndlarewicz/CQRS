﻿using Framework.Cqrs.Events;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class FakeEventProcessor : IEventProcessor
    {
        public void Process()
        {
        }
    }
}