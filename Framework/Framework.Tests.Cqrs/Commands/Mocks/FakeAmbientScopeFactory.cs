﻿using Framework.IoC;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class FakeAmbientScopeFactory : IAmbientScopeFactory
    {
        public AmbientScope Create()
        {
            return new AmbientScope();
        }
    }
}