﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.Cqrs.Commands;

namespace Framework.Tests.Cqrs.Commands.Mocks
{
    public sealed class ExecuteCommandCollector
    {
        private readonly IList<ICommand> _commands = new List<ICommand>();

        public IReadOnlyCollection<ICommand> ExecutedCommands => new ReadOnlyCollection<ICommand>(_commands);

        public void Add(ICommand command)
        {
            _commands.Add(command);
        }
    }
}