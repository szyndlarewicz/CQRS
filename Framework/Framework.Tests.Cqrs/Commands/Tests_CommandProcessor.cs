﻿using System.Collections.Generic;
using Framework.Cqrs.Commands;
using Framework.Cqrs.Events;
using Framework.IoC;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_CommandProcessor
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterSingleton<IEventProcessor, FakeEventProcessor>()
                .RegisterSingleton<ICommandQueue, InMemmoryCommandQueue>()
                .RegisterSingleton<ICommandDispatcher, MockCommandDispatcher>()
                .RegisterSingleton<ICommandProcessor, CommandProcessor>()
                .Done();
        }

        [TestCase]
        public void It_should_process_all_commands()
        {
            var commandA = new FirstCommand();
            var commandB = new FirstCommand();

            var commandQueue = _resolver.Get<ICommandQueue>();
            commandQueue.Enqueue(commandA);
            commandQueue.Enqueue(commandB);

            var commandProcessor = _resolver.Get<ICommandProcessor>();
            commandProcessor.Process();

            ICollection<ICommand> executedCommands = ((MockCommandDispatcher)_resolver.Get<ICommandDispatcher>()).ExecutedCommands;
            Assert.IsTrue(executedCommands.Contains(commandA));
            Assert.IsTrue(executedCommands.Contains(commandB));
        }
    }
}