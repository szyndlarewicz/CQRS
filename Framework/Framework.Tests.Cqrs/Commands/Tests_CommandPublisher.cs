﻿using System;
using Framework.Cqrs.Commands;
using Framework.IoC;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_CommandPublisher
    {
        private IDependencyRegister _register;

        [SetUp]
        public void SetUp()
        {
            _register = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<ICommandValidatorProvider, CommandValidatorProvider>()
                .RegisterSingleton<ICommandQueue, InMemmoryCommandQueue>()
                .RegisterSingleton<ICommandPublisher, CommandPublisher>();
        }

        [TestCase]
        public void It_should_send_command()
        {
            IDependencyResolver resolver = _register
                .RegisterTransient<ICommandValidator<FirstCommand>, PositiveFirstCommandValidator>()
                .Done();

            var commandQueue = resolver.Get<ICommandQueue>();
            var commandPublisher = resolver.Get<ICommandPublisher>();

            var command = new FirstCommand();
            commandPublisher.Send(command);

            Assert.AreSame(command, commandQueue.Dequeue());
        }

        [TestCase]
        public void It_should_skip_validation_and_send_command_when_command_validator_is_undefined()
        {
            IDependencyResolver resolver = _register
                .Done();

            var commandQueue = resolver.Get<ICommandQueue>();
            var commandPublisher = resolver.Get<ICommandPublisher>();

            var command = new FirstCommand();
            commandPublisher.Send(command);

            Assert.AreSame(command, commandQueue.Dequeue());
        }

        [TestCase]
        public void It_should_throw_exception_for_undefined_command()
        {
            IDependencyResolver resolver = _register
                .Done();

            var commandPublisher = resolver.Get<ICommandPublisher>();
            Assert.Throws<ArgumentNullException>(() => commandPublisher.Send<FirstCommand>(null));
        }

        [TestCase]
        public void It_should_return_validation_errors_if_command_is_not_valid()
        {
            IDependencyResolver resolver = _register
                .RegisterTransient<ICommandValidator<FirstCommand>, NegativeFirstCommandValidator>()
                .Done();

            var commandQueue = resolver.Get<ICommandQueue>();
            var commandPublisher = resolver.Get<ICommandPublisher>();

            var command = new FirstCommand();
            INotification notification = commandPublisher.Send(command);

            Assert.False(notification.IsValid);
            Assert.AreEqual(1, notification.Errors.Count);
            Assert.False(commandQueue.HasCommands);
        }
    }
}