﻿using Framework.Cqrs.Commands;
using Framework.IoC;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_CommandValidatorProvider
    {
        private IDependencyResolver _register;

        [SetUp]
        public void SetUp()
        {
            _register = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<ICommandValidatorProvider, CommandValidatorProvider>()
                .RegisterTransient<ICommandValidator<FirstCommand>, PositiveFirstCommandValidator>()
                .Done();
        }

        [TestCase]
        public void It_should_return_command_validator()
        {
            ICommandValidatorProvider commandValidatorProvider = _register.Get<ICommandValidatorProvider>();
            ICommandValidator<FirstCommand> commandValidator = commandValidatorProvider.Get<FirstCommand>();
            Assert.IsNotNull(commandValidator);
        }

        [TestCase]
        public void It_should_return_null_if_command_validatior_is_not_registered()
        {
            ICommandValidatorProvider commandValidatorProvider = _register.Get<ICommandValidatorProvider>();
            ICommandValidator<SecondCommand> commandValidator = commandValidatorProvider.Get<SecondCommand>();
            Assert.IsNull(commandValidator);
        }
    }
}