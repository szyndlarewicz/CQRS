﻿using System;
using Framework.Cqrs.Commands;
using Framework.Tests.Cqrs.Commands.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Commands
{
    [TestFixture]
    public sealed class Tests_InMemmoryCommandQueue
    {
        private ICommandQueue _commandQueue;

        [SetUp]
        public void SetUp()
        {
            _commandQueue = new InMemmoryCommandQueue();
        }

        [TestCase]
        public void It_should_enqueue_command()
        {
            var command = new FirstCommand();
            _commandQueue.Enqueue(command);
            Assert.True(_commandQueue.HasCommands);
        }

        [TestCase]
        public void It_should_dequeue_command()
        {
            var command = new FirstCommand();
            _commandQueue.Enqueue(command);
            Assert.AreSame(command, _commandQueue.Dequeue());
        }

        [TestCase]
        public void It_should_throw_exception_when_trying_get_command_from_empty_queue()
        {
            Assert.False(_commandQueue.HasCommands);
            Assert.Throws<InvalidOperationException>(() => _commandQueue.Dequeue());
        }
    }
}