﻿using System;
using Framework.Cqrs.Events;
using Framework.Tests.Cqrs.Events.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Events
{
    [TestFixture]
    public sealed class Tests_InMemmoryEventQueue
    {
        private IEventQueue _eventQueue;

        [SetUp]
        public void SetUp()
        {
            _eventQueue = new InMemmoryEventQueue();
        }

        [TestCase]
        public void It_should_enqueue_event()
        {
            var @event = new FirstEvent();
            _eventQueue.Enqueue(@event);
            Assert.True(_eventQueue.HasEvents);
        }

        [TestCase]
        public void It_should_dequeue_event()
        {
            var @event = new FirstEvent();
            _eventQueue.Enqueue(@event);
            Assert.AreSame(@event, _eventQueue.Dequeue());
        }

        [TestCase]
        public void It_should_throw_exception_when_getting_event_from_empty_queue()
        {
            Assert.False(_eventQueue.HasEvents);
            Assert.Throws<InvalidOperationException>(() => _eventQueue.Dequeue());
        }
    }
}