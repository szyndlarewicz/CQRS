﻿using System;
using Framework.Domain;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class FirstEvent : Event
    {
        public FirstEvent() 
            : this(Guid.NewGuid())
        {
        }

        public FirstEvent(Guid aggregateId) 
            : base(aggregateId)
        {
        }
    }
}