﻿using System.Collections.Generic;
using Framework.Cqrs.Events;
using Framework.Domain;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class MockEventDispatcher : IEventDispatcher
    {
        public MockEventDispatcher()
        {
            ExecutedEvents = new List<IEvent>();
        }

        public ICollection<IEvent> ExecutedEvents { get; }

        public void Execute(IEvent @event)
        {
            ExecutedEvents.Add(@event);
        }
    }
}