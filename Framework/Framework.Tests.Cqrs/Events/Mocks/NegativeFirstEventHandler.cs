﻿using Framework.Cqrs.Events;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class NegativeFirstEventHandler : IEventHandler<FirstEvent>
    {
        private readonly ExecuteEventCollector _executeEventCollector;

        public NegativeFirstEventHandler(ExecuteEventCollector executeEventCollector)
        {
            _executeEventCollector = executeEventCollector;
        }

        public void Execute(FirstEvent @event)
        {
            _executeEventCollector.Add(@event);
        }
    }
}