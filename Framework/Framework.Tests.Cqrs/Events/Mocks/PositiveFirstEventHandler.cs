﻿using Framework.Cqrs.Events;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class PositiveFirstEventHandler : IEventHandler<FirstEvent>
    {
        private readonly ExecuteEventCollector _executeEventCollector;

        public PositiveFirstEventHandler(ExecuteEventCollector executeEventCollector)
        {
            _executeEventCollector = executeEventCollector;
        }

        public void Execute(FirstEvent @event)
        {
            _executeEventCollector.Add(@event);
        }
    }
}