﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Framework.Domain;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class ExecuteEventCollector
    {
        private readonly IList<IEvent> _events = new List<IEvent>();

        public IReadOnlyCollection<IEvent> ExecutedEvents => new ReadOnlyCollection<IEvent>(_events);

        public void Add(IEvent @event)
        {
            _events.Add(@event);
        }
    }
}