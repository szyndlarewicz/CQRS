﻿using Framework.IoC;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class FakeAmbientScopeFactory : IAmbientScopeFactory
    {
        public AmbientScope Create()
        {
            return new AmbientScope();
        }
    }
}