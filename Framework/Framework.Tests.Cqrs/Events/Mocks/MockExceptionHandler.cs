﻿using System;
using Framework.Cqrs;

namespace Framework.Tests.Cqrs.Events.Mocks
{
    public sealed class MockExceptionHandler : IExceptionHandler
    {
        public void Handle(Exception exception)
        {
        }
    }
}