﻿using System;
using Framework.Domain;
using Framework.Tests.Cqrs.Events.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Events
{
    [TestFixture]
    public sealed class Tests_Event
    {
        private Guid _aggreageteId;
        private Event _testEvent;

        [SetUp]
        public void SetUp()
        {
            _aggreageteId = Guid.NewGuid();
            _testEvent = new FirstEvent(_aggreageteId);
        }

        [TestCase]
        public void It_should_set_aggregate_id()
        {
            Assert.AreEqual(_aggreageteId, _testEvent.AggregateId);
        }
    }
}