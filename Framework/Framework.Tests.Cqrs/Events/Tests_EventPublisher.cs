﻿using System;
using Framework.Cqrs.Events;
using Framework.IoC;
using Framework.Tests.Cqrs.Events.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Events
{
    [TestFixture]
    public sealed class Tests_EventPublisher
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterSingleton<IEventQueue, InMemmoryEventQueue>()
                .RegisterSingleton<IEventPublisher, EventPublisher>()
                .Done();
        }

        [TestCase]
        public void It_should_send_event()
        {
            var eventQueue = _resolver.Get<IEventQueue>();
            var eventPublisher = _resolver.Get<IEventPublisher>();

            var @event = new FirstEvent();
            eventPublisher.Send(@event);

            Assert.AreSame(@event, eventQueue.Dequeue());
        }

        [TestCase]
        public void It_should_throw_exception_for_undefined_event()
        {
            var eventPublisher = _resolver.Get<IEventPublisher>();

            Assert.Throws<ArgumentNullException>(() => eventPublisher.Send((FirstEvent)null));
        }
    }
}