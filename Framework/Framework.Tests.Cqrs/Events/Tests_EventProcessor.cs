﻿using Framework.Cqrs.Events;
using Framework.IoC;
using Framework.Tests.Cqrs.Events.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Events
{
    [TestFixture]
    public sealed class Tests_EventProcessor
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterSingleton<IEventProcessor, EventProcessor>()
                .RegisterSingleton<IEventQueue, InMemmoryEventQueue>()
                .RegisterSingleton<IEventDispatcher, MockEventDispatcher>()
                .Done();
        }

        [TestCase]
        public void It_should_process_all_events()
        {
            var eventA = new FirstEvent();
            var eventB = new FirstEvent();

            var eventQueue = _resolver.Get<IEventQueue>();
            eventQueue.Enqueue(eventA);
            eventQueue.Enqueue(eventB);

            var eventProcessor = _resolver.Get<IEventProcessor>();
            eventProcessor.Process();

            var executedEvents = ((MockEventDispatcher)_resolver.Get<IEventDispatcher>()).ExecutedEvents;
            Assert.IsTrue(executedEvents.Contains(eventA));
            Assert.IsTrue(executedEvents.Contains(eventB));
        }
    }
}