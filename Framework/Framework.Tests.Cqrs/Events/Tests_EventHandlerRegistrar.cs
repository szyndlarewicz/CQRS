﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Cqrs.Events;
using Framework.IoC;
using Framework.Tests.Cqrs.Events.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Events
{
    [TestFixture]
    public sealed class Tests_EventHandlerRegistrar
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<IEventHandlerRegistrar, EventHandlerRegistrar>()
                .Done();
        }

        [TestCase]
        public void It_should_register_event_handler_correctly()
        {
            var eventHandlerRegistrar = _resolver.Get<IEventHandlerRegistrar>();
            eventHandlerRegistrar.Register<FirstEvent, PositiveFirstEventHandler>();
            IEnumerable<Type> eventHandlerTypes = eventHandlerRegistrar.GetEventHandlers(new FirstEvent());
            Assert.IsTrue(eventHandlerTypes.Contains(typeof(PositiveFirstEventHandler)));
        }

        [TestCase]
        public void It_should_register_all_event_handlers_for_required_eventy()
        {
            var eventHandlerRegistrar = _resolver.Get<IEventHandlerRegistrar>();
            eventHandlerRegistrar.Register<FirstEvent, PositiveFirstEventHandler>();
            eventHandlerRegistrar.Register<FirstEvent, NegativeFirstEventHandler>();

            IList<Type> eventHandlerTypes = eventHandlerRegistrar.GetEventHandlers(new FirstEvent()).ToList();
            Assert.IsTrue(eventHandlerTypes.Contains(typeof(PositiveFirstEventHandler)));
            Assert.IsTrue(eventHandlerTypes.Contains(typeof(NegativeFirstEventHandler)));
        }

        [TestCase]
        public void It_should_give_empty_list_when_no_event_handlers_has_been_registered()
        {
            var eventHandlerRegistrar = _resolver.Get<IEventHandlerRegistrar>();
            IEnumerable<Type> eventHandlerTypes = eventHandlerRegistrar.GetEventHandlers(new FirstEvent());
            Assert.IsEmpty(eventHandlerTypes);
        }
    }
}