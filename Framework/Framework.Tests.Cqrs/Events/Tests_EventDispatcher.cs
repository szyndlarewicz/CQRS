﻿using System.Linq;
using Framework.Cqrs;
using Framework.Cqrs.Events;
using Framework.IoC;
using Framework.Tests.Cqrs.Events.Mocks;
using NUnit.Framework;

namespace Framework.Tests.Cqrs.Events
{
    [TestFixture]
    public sealed class Tests_EventDispatcher
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = TestDependencyScope.Instance
                .RegisterDependencyResolver()
                .RegisterSingleton<IAmbientScopeFactory, FakeAmbientScopeFactory>()
                .RegisterSingleton<IEventHandlerRegistrar, EventHandlerRegistrar>()
                .RegisterSingleton<IEventDispatcher, EventDispatcher>()
                .RegisterSingleton<IEventHandler<FirstEvent>, PositiveFirstEventHandler>()
                .RegisterSingleton<IEventHandler<FirstEvent>, NegativeFirstEventHandler>()
                .RegisterSingleton<IExceptionHandler, MockExceptionHandler>()
                .RegisterSingleton<ExecuteEventCollector>()
                .Done();

            _resolver.Get<IEventHandlerRegistrar>()
                .Register<FirstEvent, PositiveFirstEventHandler>()
                .Register<FirstEvent, NegativeFirstEventHandler>();
        }

        [TestCase]
        public void It_should_execute_event()
        {
            var @event = new FirstEvent();

            var eventDispatcher = _resolver.Get<IEventDispatcher>();
            eventDispatcher.Execute(@event);

            var executeEventCollector = _resolver.Get<ExecuteEventCollector>();
            Assert.IsTrue(executeEventCollector.ExecutedEvents.Contains(@event));
        }

        [TestCase]
        public void It_should_suppress_all_exceptions()
        {
            var @event = new FirstEvent();

            var eventDispatcher = _resolver.Get<IEventDispatcher>();
            eventDispatcher.Execute(@event);

            var executeEventCollector = _resolver.Get<ExecuteEventCollector>();
            Assert.AreEqual(2, executeEventCollector.ExecutedEvents.Count(e => e == @event));
        }

        [TestCase]
        public void It_should_do_nothing_when_event_handlers_are_not_registerd()
        {
            var eventDispatcher = _resolver.Get<IEventDispatcher>();
            Assert.DoesNotThrow(() => eventDispatcher.Execute(new SecondEvent()));
        }
    }
}