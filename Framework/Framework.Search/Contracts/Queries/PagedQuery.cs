﻿using System;
using Framework.Search.Contracts.Types;

namespace Framework.Search.Contracts.Queries
{
    public abstract class PagedQuery
    {
        protected PagedQuery(OrderDirection orderDirection, OrderExpression orderExpression, PageSize pageSize, PageIndex pageIndex)
            : this(new SortOrder(orderDirection, orderExpression), pageSize, pageIndex)
        {
        }

        protected PagedQuery(SortOrder sortOrder, PageSize pageSize, PageIndex pageIndex)
        {
            SortOrder = sortOrder ?? throw new ArgumentNullException(nameof(sortOrder));
            PageSize = pageSize ?? throw new ArgumentNullException(nameof(pageSize));
            PageIndex = pageIndex ?? throw new ArgumentNullException(nameof(pageIndex));
        }

        public SortOrder SortOrder { get; protected set; }

        public PageSize PageSize { get; protected set; }

        public PageIndex PageIndex { get; protected set; }
    }
}