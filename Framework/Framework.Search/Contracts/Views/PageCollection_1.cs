﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework.Search.Contracts.Views
{
    public class PageCollection<TType> : View
    {
        public PageCollection(IEnumerable<TType> items)
        {
            Items = items?.ToList() ?? throw new ArgumentNullException(nameof(items));
            Count = Items.Count;
        }

        public PageCollection(IEnumerable<TType> items, long count)
            : this(items)
        {
            if (count < 0)
                throw new ArgumentException($"{nameof(count)} cannot be less than 0");

            Count = count;
        }

        public static PageCollection<TType> Empty => new PageCollection<TType>(new List<TType>());

        public IList<TType> Items { get; protected set; }

        public long Count { get; protected set; }
    }
}