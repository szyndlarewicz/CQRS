﻿namespace Framework.Search.Contracts.Views
{
    public abstract class LookUpView<TKey> : View
    {
        protected LookUpView()
        {
        }

        protected LookUpView(TKey id, string name)
        {
            Id = id;
            Name = name;
        }

        public TKey Id { get; set; }

        public string Name { get; set; }
    }
}