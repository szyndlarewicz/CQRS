﻿using System;

namespace Framework.Search.Contracts.Types
{
    public abstract class OrderExpression : IEquatable<OrderExpression>
    {
        protected readonly string Value;

        protected OrderExpression(string orderExpression)
        {
            if (orderExpression == null)
                throw new ArgumentNullException(nameof(orderExpression));

            if (orderExpression.Length == 0)
                throw new ArgumentException($"{nameof(orderExpression)} cannot be Empty");

            Value = orderExpression;
        }

        public static implicit operator string(OrderExpression instance)
        {
            return instance?.Value;
        }

        public static bool operator ==(OrderExpression x, OrderExpression y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(OrderExpression x, OrderExpression y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as OrderExpression);
        }

        public bool Equals(OrderExpression value)
        {
            return !ReferenceEquals(null, value) && string.Equals(Value, value.Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return Value;
        }
    }
}