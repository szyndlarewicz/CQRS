﻿using System;

namespace Framework.Search.Contracts.Types
{
    [Serializable]
    public sealed class UnrecognizedOrderExpressionException : Exception
    {
        public UnrecognizedOrderExpressionException(string orderExpression)
            : base("Unrecognized order expression.")
        {
            OrderExpression = orderExpression;
        }

        public string OrderExpression { get; }
    }
}