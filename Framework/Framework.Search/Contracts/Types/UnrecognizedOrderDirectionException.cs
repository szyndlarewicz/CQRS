﻿using System;

namespace Framework.Search.Contracts.Types
{
    [Serializable]
    public sealed class UnrecognizedOrderDirectionException : Exception
    {
        public UnrecognizedOrderDirectionException(string orderDirection)
            : base("Unrecognized order direction.")
        {
            OrderDirection = orderDirection;
        }

        public string OrderDirection { get; }
    }
}