﻿using System;

namespace Framework.Search.Contracts.Types
{
    public sealed class PageSize : IEquatable<PageSize>
    {
        private readonly int _value;

        public PageSize(int pageSize)
        {
            if (pageSize < 1)
                throw new ArgumentException($"{nameof(pageSize)} must be greater or equal 1");

            _value = pageSize;
        }

        public static explicit operator PageSize(int instance)
        {
            return instance == 0 ? null : new PageSize(instance);
        }

        public static explicit operator PageSize(int? instance)
        {
            return (instance == null || instance == 0) ? null : new PageSize(instance.Value);
        }

        public static implicit operator int(PageSize instance)
        {
            return instance?._value ?? 0;
        }

        public static implicit operator int? (PageSize instance)
        {
            return instance?._value;
        }

        public static bool operator ==(PageSize x, PageSize y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(PageSize x, PageSize y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as PageSize);
        }

        public bool Equals(PageSize value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}