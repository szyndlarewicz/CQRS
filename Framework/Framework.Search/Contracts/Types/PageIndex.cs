﻿using System;

namespace Framework.Search.Contracts.Types
{
    public sealed class PageIndex : IEquatable<PageIndex>
    {
        private readonly int _value;

        public PageIndex(int pageIndex)
        {
            if (pageIndex < 0)
                throw new ArgumentException($"{nameof(pageIndex)} must be greater or equal 0");

            _value = pageIndex;
        }

        public static explicit operator PageIndex(int instance)
        {
            return instance == 0 ? null : new PageIndex(instance);
        }

        public static explicit operator PageIndex(int? instance)
        {
            return (instance == null || instance == 0) ? null : new PageIndex(instance.Value);
        }

        public static implicit operator int(PageIndex instance)
        {
            return instance?._value ?? 0;
        }

        public static implicit operator int?(PageIndex instance)
        {
            return instance?._value;
        }

        public static bool operator ==(PageIndex x, PageIndex y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(PageIndex x, PageIndex y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as PageIndex);
        }

        public bool Equals(PageIndex value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value.ToString();
        }
    }
}