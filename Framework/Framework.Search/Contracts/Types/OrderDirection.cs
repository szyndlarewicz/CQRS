﻿using System;
using System.Linq;

namespace Framework.Search.Contracts.Types
{
    public sealed class OrderDirection : IEquatable<OrderDirection>
    {
        private static readonly string[] AvailableOrderDirections = { "Ascending", "Descending" };

        private readonly string _value;

        public OrderDirection(string orderDirection)
        {
            if (orderDirection == null)
                throw new ArgumentNullException(nameof(orderDirection));

            if (orderDirection.Length == 0)
                throw new ArgumentException($"{nameof(orderDirection)} cannot be Empty");

            if (!AvailableOrderDirections.Contains(orderDirection))
                throw new UnrecognizedOrderDirectionException(orderDirection);

            _value = orderDirection;
        }

        public static OrderDirection Ascending => new OrderDirection("Ascending");
        public static OrderDirection Descending => new OrderDirection("Descending");

        public static explicit operator OrderDirection(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new OrderDirection(instance);
        }

        public static implicit operator string(OrderDirection instance)
        {
            return instance?._value;
        }

        public static bool operator ==(OrderDirection x, OrderDirection y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(OrderDirection x, OrderDirection y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as OrderDirection);
        }

        public bool Equals(OrderDirection value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_value, value._value);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }

        public override string ToString()
        {
            return _value;
        }
    }
}