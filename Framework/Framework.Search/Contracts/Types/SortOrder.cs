﻿using System;

namespace Framework.Search.Contracts.Types
{
    public sealed class SortOrder : IEquatable<SortOrder>
    {
        public SortOrder(OrderDirection orderDirection, OrderExpression orderExpression)
        {
            OrderDirection = orderDirection ?? throw new ArgumentNullException(nameof(orderDirection));
            OrderExpression = orderExpression ?? throw new ArgumentNullException(nameof(orderExpression));
        }

        public OrderDirection OrderDirection { get; }

        public OrderExpression OrderExpression { get; }

        public static implicit operator string(SortOrder instance)
        {
            return instance?.ToString();
        }

        public static bool operator ==(SortOrder x, SortOrder y)
        {
            return ReferenceEquals(x, y) || (!ReferenceEquals(null, x) && x.Equals(y));
        }

        public static bool operator !=(SortOrder x, SortOrder y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as SortOrder);
        }

        public bool Equals(SortOrder value)
        {
            return !ReferenceEquals(null, value) && DateTime.Equals(OrderDirection, value.OrderDirection) && DateTime.Equals(OrderExpression, value.OrderExpression);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;
                hash = (hash * HashingMultiplier) ^ OrderDirection.GetHashCode();
                hash = (hash * HashingMultiplier) ^ OrderExpression.GetHashCode();
                return hash;
            }
        }

        public override string ToString()
        {
            return $"({OrderExpression} {OrderDirection})";
        }
    }
}