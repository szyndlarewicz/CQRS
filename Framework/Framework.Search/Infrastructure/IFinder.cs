﻿namespace Framework.Search.Infrastructure
{
    /// <summary>
    /// The finder interface. Finder is a repository pattern for queries only.
    /// </summary>
    public interface IFinder
    {
    }
}