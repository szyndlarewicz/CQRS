﻿using Framework.Search.Contracts.Types;

namespace Framework.Search.Infrastructure
{
    public interface ISqlQueryPagination
    {
        string Build(SqlQuery sqlQuery, PageIndex pageIndex, PageSize pageSize, SortOrder sortOrder);
    }
}