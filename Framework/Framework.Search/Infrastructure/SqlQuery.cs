﻿using System;

namespace Framework.Search.Infrastructure
{
    public class SqlQuery
    {
        public SqlQuery(string sql)
        {
            if (sql == null)
                throw new ArgumentNullException(nameof(sql));

            if (sql.Length == 0)
                throw new ArgumentException($"{nameof(sql)} cannot be Empty");

            Value = sql;
        }

        public string Value { get; } 
    }
}