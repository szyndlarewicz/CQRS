﻿using System;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public sealed class AuditModel
    {
        public AuditModel(DateTime createdAt, Guid createdBy, IEvent @evnet)
        {
            CreatedAt = createdAt;
            CreatedBy = createdBy;
            Evnet = @evnet ?? throw new ArgumentNullException(nameof(evnet));
        }

        public DateTime CreatedAt { get; }

        public Guid CreatedBy { get; }

        public IEvent Evnet { get; }
    }
}