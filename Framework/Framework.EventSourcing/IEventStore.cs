﻿using System;
using System.Collections.Generic;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public interface IEventStore
    {
        void Save<TIdentity, TVersion>(IEventsProviderAggregateRoot<TIdentity, TVersion> aggregateRoot)
            where TIdentity : EntityIdentity
            where TVersion : AggregateVersion;

        IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId);

        IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, AggregateVersion version);

        IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, DateTime dateTime);

        IEnumerable<AuditModel> GetAuditLog(EntityIdentity aggregateId);
    }
}