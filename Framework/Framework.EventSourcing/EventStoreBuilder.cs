﻿using System;
using Framework.Core.Authentication;

namespace Framework.EventSourcing
{
    public class EventStoreBuilder
    {
        public static EventStoreBuilder Instance => new EventStoreBuilder();

        public ICurrentUser CurrentUser { get; protected set; }

        public IEventSerializer EventSerializer { get; protected set; }

        public IEventStoreUnitOfWorkFactory EventStoreUnitOfWorkFactory { get; protected set; }

        public EventStoreBuilder Use(ICurrentUser currentUser)
        {
            CurrentUser = currentUser ?? throw new ArgumentNullException(nameof(currentUser));
            return this;
        }

        public EventStoreBuilder Use(IEventSerializer eventSerializer)
        {
            EventSerializer = eventSerializer ?? throw new ArgumentNullException(nameof(eventSerializer));
            return this;
        }

        public EventStoreBuilder Use(IEventStoreUnitOfWorkFactory unitOfWorkFactory)
        {
            EventStoreUnitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
            return this;
        }

        public IEventStore Build()
        {
            IEventRecordFactory eventRecordFactory = new EventRecordFactory(EventSerializer, CurrentUser);
            return new EventStore(EventStoreUnitOfWorkFactory, eventRecordFactory);
        }
    }
}