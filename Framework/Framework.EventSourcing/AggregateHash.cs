﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public sealed class AggregateHash : IEquatable<AggregateHash>
    {
        private readonly Guid _hash;

        public AggregateHash(Type type)
        {
            if (type == null)
                throw new ArgumentException(nameof(type));

            if (type.GetInterfaces().All(c => c != typeof(IAggregateRoot)))
                throw  new ArgumentException($"Cannot generate hash because required type '{type}' does not implement IAggregateRoot");

            _hash = ComputeHash(type);
        }

        public AggregateHash(Guid computedHash)
        {
            if (computedHash == Guid.Empty)
                throw new ArgumentException("Hash cannot be empty");

            _hash = computedHash;
        }

        public static AggregateHash New<T>() => new AggregateHash(typeof(T));

        public static explicit operator AggregateHash(Guid instance)
        {
            return new AggregateHash(instance);
        }

        public static explicit operator AggregateHash(Guid? instance)
        {
            return instance == null ? null : new AggregateHash(instance.Value);
        }

        public static explicit operator AggregateHash(Type instance)
        {
            return new AggregateHash(instance);
        }

        public static implicit operator Guid?(AggregateHash instance)
        {
            return instance == null ? null : (Guid?) instance._hash;
        }

        public static implicit operator Guid(AggregateHash instance)
        {
            return instance?._hash ?? Guid.Empty;
        }

        public static bool operator ==(AggregateHash x, AggregateHash y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(AggregateHash x, AggregateHash y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as AggregateHash);
        }

        public bool Equals(AggregateHash value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_hash, value._hash);
        }

        public override int GetHashCode()
        {
            return _hash.GetHashCode();
        }

        public override string ToString()
        {
            return _hash.ToString();
        }

        private Guid ComputeHash(Type type)
        {
            var cryptoServiceProvider = new SHA1CryptoServiceProvider();
            byte[] buffer = Encoding.UTF8.GetBytes(type.FullName);
            byte[] hash = cryptoServiceProvider.ComputeHash(buffer);
            Array.Resize(ref hash, 16);

            return new Guid(hash);
        }
    }
}