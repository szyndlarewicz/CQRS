﻿using System;
using System.Runtime.Serialization;

namespace Framework.EventSourcing
{
    [Serializable]
    public sealed class AggregateConcurrencyException : Exception
    {
        public AggregateConcurrencyException(Guid aggregateId, long aggregateVersion) 
            : base($"Current version ({aggregateVersion}) of the aggregate '{aggregateId}' is lower than version in event store. To solve this problem you need to reload aggregate.")
        {
            AggregateId = aggregateId;
            AggregateVersion = aggregateVersion;
        }

        public Guid AggregateId { get; }

        public long AggregateVersion { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("AggregateId", AggregateId);
            info.AddValue("AggregateVersion", AggregateVersion);
#if DEBUG
            base.GetObjectData(info, context);
#else
            info.AddValue("ClassName", GetType().FullName);
            info.AddValue("Message", Message);
#endif
        }
    }
}