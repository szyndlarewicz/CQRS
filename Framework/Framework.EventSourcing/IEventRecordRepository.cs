﻿using System;
using System.Collections.Generic;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public interface IEventRecordRepository
    {
        AggregateVersion GetAggregateVersion(EntityIdentity aggregateId);

        void AddEvent(EventRecord eventRecord);

        void AddEvent(IEnumerable<EventRecord> eventRecords);

        IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId);

        IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, AggregateVersion version);

        IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, DateTime dateTime);

        IEnumerable<AuditModel> GetAuditLog(EntityIdentity aggregateId);
    }
}