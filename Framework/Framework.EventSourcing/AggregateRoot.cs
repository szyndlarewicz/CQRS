﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.ExceptionServices;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public abstract class AggregateRoot : IEventDrivenAggregateRoot<EntityIdentity, AggregateVersion>, IAggregateRoot
    {
        private readonly Queue<IEvent> _events = new Queue<IEvent>();

        protected AggregateRoot(IEnumerable<IEvent> events)
        {
            Apply(events);
        }

        public EntityIdentity Id { get; protected set; }

        public AggregateVersion Version { get; protected set; }

        public IEnumerable<IEvent> Events => _events;

        public abstract void Snapshot();

        public void Flush()
        {
            int eventsCount = _events.Count(c => !(c is ISnapshot));

            Version = Version == null
                ? new AggregateVersion(eventsCount)
                : new AggregateVersion(Version + eventsCount);

            _events.Clear();
        }

        public void Apply(IEnumerable<IEvent> events)
        {
            foreach (IEvent @event in events)
                Apply(@event);
        }

        protected void Apply(IEvent @event)
        {
            try
            {
                var bindingFlags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod;
                GetType().InvokeMember("ApplyChange", bindingFlags, Type.DefaultBinder, this, new object[] { @event });
                Version = Version == null ? AggregateVersion.First : Version.Next;
            }
            catch (TargetInvocationException exception)
            {
                ExceptionDispatchInfo.Capture(exception.InnerException).Throw();
            }
        }

        protected void Enqueue(IEvent @event)
        {
            _events.Enqueue(@event);
        }
    }
}