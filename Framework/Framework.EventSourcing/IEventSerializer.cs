﻿using Framework.Domain;

namespace Framework.EventSourcing
{
    public interface IEventSerializer
    {
        string Serialize(IEvent @event);

        IEvent Deserialize(string json);
    }
}