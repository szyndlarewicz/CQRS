﻿using Framework.Core.Persistence;

namespace Framework.EventSourcing
{
    public interface IEventStoreUnitOfWork : IUnitOfWork
    {
        IEventRecordRepository EventRepository { get; }
    }
}