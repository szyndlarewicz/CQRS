﻿using System;
using System.Collections.Generic;
using Framework.Domain;
using Framework.Core.Persistence;

namespace Framework.EventSourcing
{
    public sealed class EventStore : IEventStore
    {
        private readonly IEventStoreUnitOfWorkFactory _unitOfWorkFactory;

        private readonly IEventRecordFactory _eventFactory;

        public EventStore(IEventStoreUnitOfWorkFactory unitOfWorkFactory, IEventRecordFactory eventFactory)
        {
            _unitOfWorkFactory = unitOfWorkFactory ?? throw new ArgumentNullException(nameof(unitOfWorkFactory));
            _eventFactory = eventFactory ?? throw new ArgumentNullException(nameof(eventFactory));
        }

        public void Save<TIdentity, TVersion>(IEventsProviderAggregateRoot<TIdentity, TVersion> aggregateRoot)
            where TIdentity : EntityIdentity
            where TVersion : AggregateVersion
        {
            if (aggregateRoot == null)
                throw new ArgumentNullException(nameof(aggregateRoot));

            using (IEventStoreUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                var currentVersion = unitOfWork.EventRepository.GetAggregateVersion(aggregateRoot.Id);
                if (currentVersion != aggregateRoot.Version)
                    throw new AggregateConcurrencyException((Guid) aggregateRoot.Id, (long) aggregateRoot.Version);

                using (ITransaction transaction = unitOfWork.BeginTransaction())
                {
                    AggregateHash aggregateHash = new AggregateHash(aggregateRoot.GetType());
                    EntityIdentity aggregateId = aggregateRoot.Id;
                    AggregateVersion aggregateVersion = currentVersion;

                    foreach (IEvent @event in aggregateRoot.Events)
                    {
                        aggregateVersion = GetNextVesrion(aggregateVersion, @event);
                        EventRecord evnet = _eventFactory.Create(aggregateHash, aggregateId, aggregateVersion, @event);
                        unitOfWork.EventRepository.AddEvent(evnet);
                    }
                    
                    unitOfWork.Save();
                    transaction.Commit();
                    aggregateRoot.Flush();
                }
            }
        }

        public IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId)
        {
            using (IEventStoreUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                return unitOfWork.EventRepository.GetEvents(aggregateId);
            }
        }

        public IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, AggregateVersion version)
        {
            using (IEventStoreUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                return unitOfWork.EventRepository.GetEvents(aggregateId, version);
            }
        }

        public IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, DateTime dateTime)
        {
            using (IEventStoreUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                return unitOfWork.EventRepository.GetEvents(aggregateId, dateTime);
            }
        }

        public IEnumerable<AuditModel> GetAuditLog(EntityIdentity aggregateId)
        {
            using (IEventStoreUnitOfWork unitOfWork = _unitOfWorkFactory.Create())
            {
                return unitOfWork.EventRepository.GetAuditLog(aggregateId);
            }
        }

        private AggregateVersion GetNextVesrion(AggregateVersion currentVersion, IEvent @event)
        {
            return currentVersion == null
                ? AggregateVersion.First
                : (@event is ISnapshot ? currentVersion : currentVersion.Next);
        }
    }
}