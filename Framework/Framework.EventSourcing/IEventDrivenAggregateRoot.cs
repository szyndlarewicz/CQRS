﻿using System.Collections.Generic;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public interface IEventDrivenAggregateRoot<out TIdentity, out TVersion> : IEventsProviderAggregateRoot<TIdentity, TVersion>
        where TIdentity : EntityIdentity
        where TVersion : AggregateVersion
    {
        void Apply(IEnumerable<IEvent> events);

        void Snapshot();
    }
}