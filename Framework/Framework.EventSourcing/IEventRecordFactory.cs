﻿using Framework.Domain;

namespace Framework.EventSourcing
{
    public interface IEventRecordFactory
    {
        EventRecord Create(AggregateHash aggregateHash, EntityIdentity aggregateId, AggregateVersion aggregateVersion, IEvent @event);
    }
}