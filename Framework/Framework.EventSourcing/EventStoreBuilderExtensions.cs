﻿namespace Framework.EventSourcing
{
    public static class EventStoreBuilderExtensions
    {
        public static EventStoreBuilder UseJsonSerializer(this EventStoreBuilder eventStoreBuilder)
        {
            return eventStoreBuilder.Use(new JsonEventSerializer());
        }
    }
}