﻿using System;

namespace Framework.EventSourcing
{
    public sealed class EventRecord
    {
        public DateTime CreatedAt { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid AggregateHash { get; set; }

        public Guid AggregateId { get; set; }

        public long AggregateVersion { get; set; }

        public bool IsSnapshot { get; set; }

        public string EventData { get; set; }
    }
}