using System;
using Framework.Core.Authentication;
using Framework.Core.Providers;
using Framework.Domain;

namespace Framework.EventSourcing
{
    public sealed class EventRecordFactory : IEventRecordFactory
    {
        private readonly IEventSerializer _eventSerializer;
        private readonly ICurrentUser _currentUser;

        public EventRecordFactory(IEventSerializer eventSerializer, ICurrentUser currentUser)
        {
            _eventSerializer = eventSerializer ?? throw new ArgumentNullException(nameof(eventSerializer));
            _currentUser = currentUser ?? throw new ArgumentNullException(nameof(currentUser));
        }

        public EventRecord Create(AggregateHash aggregateHash, EntityIdentity aggregateId, AggregateVersion aggregateVersion, IEvent @event)
        {
            return new EventRecord
            {
                CreatedAt = DateTimeProvider.Current.Now,
                CreatedBy = _currentUser.UserId,
                AggregateId = (Guid)aggregateId,
                AggregateVersion = (long)aggregateVersion,
                AggregateHash = (Guid)aggregateHash,
                IsSnapshot = @event is ISnapshot,
                EventData = _eventSerializer.Serialize(@event)
            };
        }
    }
}