﻿using System;
using Framework.Domain;
using Newtonsoft.Json;

namespace Framework.EventSourcing
{
    public sealed class JsonEventSerializer : IEventSerializer
    {
        public JsonSerializerSettings Settings { get; } = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public string Serialize(IEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            return JsonConvert.SerializeObject(@event, Settings);
        }

        public IEvent Deserialize(string json)
        {
            if (json == null)
                throw new ArgumentNullException(nameof(json));

            if (json.Length == 0)
                throw new ArgumentException("Event json cannot be empty");

            return (IEvent)JsonConvert.DeserializeObject(json, Settings);
        }
    }
}