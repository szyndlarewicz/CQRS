﻿using Framework.Domain;

namespace Framework.EventSourcing
{
    public interface ISnapshot : IEvent
    {
    }
}