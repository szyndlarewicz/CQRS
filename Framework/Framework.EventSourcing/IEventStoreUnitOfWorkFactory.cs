﻿namespace Framework.EventSourcing
{
    public interface IEventStoreUnitOfWorkFactory
    {
        IEventStoreUnitOfWork Create();
    }
}
