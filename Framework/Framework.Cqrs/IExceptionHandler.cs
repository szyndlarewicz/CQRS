﻿using System;

namespace Framework.Cqrs
{
    public interface IExceptionHandler
    {
        void Handle(Exception exception);
    }
}