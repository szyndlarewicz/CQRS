﻿using System;

namespace Framework.Cqrs.Commands
{
    public abstract class Command : ICommand
    {
        protected Command()
        {
            CommandId = Guid.NewGuid();
        }

        public Guid CommandId { get; }
    }
}