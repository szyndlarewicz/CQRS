﻿namespace Framework.Cqrs.Commands
{
    public interface ICommandValidatorProvider
    {
        ICommandValidator<TCommand> Get<TCommand>()
            where TCommand : class, ICommand;
    }
}