﻿using System.Collections.Generic;

namespace Framework.Cqrs.Commands
{
    public interface INotification
    {
        bool IsValid { get; }

        IList<string> Errors { get; }
    }
}