﻿using System;
using System.Runtime.Serialization;

namespace Framework.Cqrs.Commands
{
    [Serializable]
    public sealed class CommandHandlerNotFoundException : Exception
    {
        public CommandHandlerNotFoundException(Type commandType)
            : base($"Command handler not found for given command '{commandType.FullName}'")
        {
            CommandType = commandType;
        }

        public Type CommandType { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("CommandType", CommandType);
            base.GetObjectData(info, context);
        }
    }
}