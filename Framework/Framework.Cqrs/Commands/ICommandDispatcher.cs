﻿namespace Framework.Cqrs.Commands
{
    public interface ICommandDispatcher
    {
        void Execute(ICommand command);
    }
}