﻿using System;

namespace Framework.Cqrs.Commands
{
    public sealed class CommandPublisher : ICommandPublisher
    {
        private readonly ICommandValidatorProvider _commandValidatorProvider;
        private readonly ICommandQueue _commandQueue;

        public CommandPublisher(ICommandValidatorProvider commandValidatorProvider, ICommandQueue commandQueue)
        {
            _commandValidatorProvider = commandValidatorProvider ?? throw new ArgumentNullException(nameof(commandValidatorProvider)); 
            _commandQueue = commandQueue ?? throw new ArgumentNullException(nameof(commandQueue));
        }

        public INotification Send<TCommand>(TCommand command)
            where TCommand : class, ICommand
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            ICommandValidator<TCommand> commandValidator = _commandValidatorProvider.Get<TCommand>();
            if (commandValidator != null)
            {
                INotification validationResult = commandValidator.Validate(command);
                if (!validationResult.IsValid)
                    return validationResult;
            }

            _commandQueue.Enqueue(command);
            return Notification.Empty;
        }
    }
}