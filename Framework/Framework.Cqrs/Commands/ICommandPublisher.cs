﻿namespace Framework.Cqrs.Commands
{
    public interface ICommandPublisher
    {
        INotification Send<TCommand>(TCommand command)
            where TCommand : class, ICommand;
    }
}