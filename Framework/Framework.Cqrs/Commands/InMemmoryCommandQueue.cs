﻿using System.Collections.Generic;

namespace Framework.Cqrs.Commands
{
    public sealed class InMemmoryCommandQueue : Queue<ICommand>, ICommandQueue
    {
        public bool HasCommands => Count > 0;
    }
}