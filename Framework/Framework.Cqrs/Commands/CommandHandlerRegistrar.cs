﻿using System;
using System.Collections.Generic;

namespace Framework.Cqrs.Commands
{
    public sealed class CommandHandlerRegistrar : ICommandHandlerRegistrar
    {
        private readonly IDictionary<Type, Type> _container = new Dictionary<Type, Type>();

        public ICommandHandlerRegistrar Register<TCommand, TCommandHandler>()
            where TCommand : class, ICommand
            where TCommandHandler : class, ICommandHandler<TCommand>
        {
            Type commandType = typeof(TCommand);

            if (_container.ContainsKey(commandType))
                throw new CommandHandlerRegisteredException(commandType);

            _container.Add(commandType, typeof(TCommandHandler));
            return this;
        }

        public ICommandHandlerRegistrar Register(Type commandType, Type commandHandlerType)
        {
            if (commandType == null)
                throw new ArgumentNullException(nameof(commandType));

            if (!typeof(ICommand).IsAssignableFrom(commandType))
                throw new ArgumentException($"Command '{commandType.FullName}' does not implement interface ICommand");

            if (commandHandlerType == null)
                throw new ArgumentNullException(nameof(commandHandlerType));

            if (!typeof(ICommandHandler<>).MakeGenericType(commandType).IsAssignableFrom(commandHandlerType))
                throw new ArgumentException($"Command handler '{commandHandlerType.FullName}' does not implement interface ICommandHandler<{commandType.FullName}>");

            if (_container.ContainsKey(commandType))
                throw new CommandHandlerRegisteredException(commandType);

            _container.Add(commandType, commandHandlerType);
            return this;
        }

        public Type GetCommandHandler(ICommand command)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command));

            Type commandType = command.GetType();
            if (!_container.ContainsKey(commandType))
                throw new CommandHandlerNotFoundException(commandType);

            return _container[commandType];
        }
    }
}