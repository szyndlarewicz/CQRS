﻿namespace Framework.Cqrs.Commands
{
    public interface ICommandValidator<in TCommand> : ICommandValidator
        where TCommand : class, ICommand
    {
        INotification Validate(TCommand command);
    }
}