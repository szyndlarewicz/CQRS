﻿using System;
using System.Reflection;
using Framework.IoC;

namespace Framework.Cqrs.Commands
{
    public sealed class CommandDispatcher : ICommandDispatcher
    {
        private readonly IDependencyResolver _dependencyResolver;
        private readonly ICommandHandlerRegistrar _commandHandlerRegistrar;
        private readonly IAmbientScopeFactory _ambientScopeFactory;
        private readonly IExceptionHandler _exceptionHandler;

        public CommandDispatcher(
            IDependencyResolver dependencyResolver,
            ICommandHandlerRegistrar commandHandlerRegistrar,
            IAmbientScopeFactory ambinetScopeFactory,
            IExceptionHandler exceptionHandler)
        {
            _dependencyResolver = dependencyResolver ?? throw new ArgumentNullException(nameof(dependencyResolver));
            _commandHandlerRegistrar = commandHandlerRegistrar ?? throw new ArgumentNullException(nameof(commandHandlerRegistrar));
            _ambientScopeFactory = ambinetScopeFactory ?? throw new ArgumentNullException(nameof(ambinetScopeFactory));
            _exceptionHandler = exceptionHandler ?? throw new ArgumentNullException(nameof(exceptionHandler));
        }

        public void Execute(ICommand command)
        {
            Type commandHandlerType = _commandHandlerRegistrar.GetCommandHandler(command);
            MethodInfo executeMethod = commandHandlerType.GetMethod(nameof(ICommandHandler<ICommand>.Execute));

            using (_ambientScopeFactory.Create())
            {
                try
                {
                    object commandHandler = _dependencyResolver.Get(commandHandlerType);
                    executeMethod.Invoke(commandHandler, new object[] { command });
                }
                catch (TargetInvocationException exception)
                {
                    _exceptionHandler.Handle(exception.InnerException);
                }
            }
        }
    }
}