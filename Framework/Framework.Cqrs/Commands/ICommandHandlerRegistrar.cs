﻿using System;

namespace Framework.Cqrs.Commands
{
    public interface ICommandHandlerRegistrar
    {
        ICommandHandlerRegistrar Register<TCommand, TCommandHandler>()
            where TCommand : class, ICommand
            where TCommandHandler : class, ICommandHandler<TCommand>;

        ICommandHandlerRegistrar Register(Type commandType, Type commandHandlerType);

        Type GetCommandHandler(ICommand command);
    }
}