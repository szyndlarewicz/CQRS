﻿using System;
using Framework.Cqrs.Events;

namespace Framework.Cqrs.Commands
{
    public sealed class CommandProcessor : ICommandProcessor
    {
        private readonly ICommandQueue _commandQueue;
        private readonly ICommandDispatcher _commandDispatcher;
        private readonly IEventProcessor _eventProcessor;

        public CommandProcessor(
            ICommandQueue commandQueue,
            ICommandDispatcher commandDispatcher,
            IEventProcessor eventProcessor)
        {
            _commandQueue = commandQueue ?? throw new ArgumentNullException(nameof(commandQueue));
            _commandDispatcher = commandDispatcher ?? throw new ArgumentNullException(nameof(commandDispatcher));
            _eventProcessor = eventProcessor ?? throw new ArgumentNullException(nameof(eventProcessor));
        }

        public void Process()
        {
            while (_commandQueue.HasCommands)
            {
                ICommand command = _commandQueue.Dequeue();
                _commandDispatcher.Execute(command);
                _eventProcessor.Process();
            }
        }
    }
}