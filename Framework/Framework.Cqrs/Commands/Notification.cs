﻿using System;
using System.Collections.Generic;

namespace Framework.Cqrs.Commands
{
    public sealed class Notification : INotification
    {
        private readonly IList<string> _errors = new List<string>();

        public bool IsValid => _errors.Count == 0;

        public IList<string> Errors => new List<string>(_errors);

        public static Notification Empty => new Notification();

        public INotification Add(string error)
        {
            if (error == null)
                throw new ArgumentNullException(nameof(error));

            if (error.Length == 0)
                throw new ArgumentException("Error message cannot be empty");

            _errors.Add(error);
            return this;
        }

        public void Clear()
        {
            _errors.Clear();
        }
    }
}