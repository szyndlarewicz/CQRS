﻿using System;
using Framework.IoC;

namespace Framework.Cqrs.Commands
{
    public sealed class CommandValidatorProvider : ICommandValidatorProvider
    {
        private readonly IDependencyResolver _dependencyResolver;

        public CommandValidatorProvider(IDependencyResolver dependencyResolver)
        {
            _dependencyResolver = dependencyResolver ?? throw new ArgumentNullException(nameof(dependencyResolver));
        }

        public ICommandValidator<TCommand> Get<TCommand>()
            where TCommand : class, ICommand
        {
            return _dependencyResolver.TryGet<ICommandValidator<TCommand>>();
        }
    }
}