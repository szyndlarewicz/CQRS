﻿using System;
using System.Runtime.Serialization;

namespace Framework.Cqrs.Commands
{
    [Serializable]
    public sealed class CommandHandlerRegisteredException : Exception
    {
        public CommandHandlerRegisteredException(Type commandType)
            : base($"Command handler has already been registered for given command '{commandType.FullName}'")
        {
            CommandType = commandType;
        }

        public Type CommandType { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("CommandType", CommandType);
            base.GetObjectData(info, context);
        }
    }
}