﻿namespace Framework.Cqrs.Commands
{
    public interface ICommandProcessor
    {
        void Process();
    }
}