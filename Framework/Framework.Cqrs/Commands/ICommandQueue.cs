﻿namespace Framework.Cqrs.Commands
{
    public interface ICommandQueue
    {
        bool HasCommands { get; }

        void Enqueue(ICommand command);

        ICommand Dequeue();
    }
}