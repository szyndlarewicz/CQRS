﻿namespace Framework.Cqrs.Queries
{
    public interface IQueryHandler<in TQuery, out TResult> : IQueryHandler
        where TQuery : IQuery
    {
        TResult Execute(TQuery query);
    }
}