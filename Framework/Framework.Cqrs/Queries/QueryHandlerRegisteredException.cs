﻿using System;
using System.Runtime.Serialization;

namespace Framework.Cqrs.Queries
{
    [Serializable]
    public sealed class QueryHandlerRegisteredException : Exception
    {
        public QueryHandlerRegisteredException(Type queryType)
            : base($"Query handler has already been registered for given query '{queryType.FullName}'")
        {
            QueryType = queryType;
        }

        public Type QueryType { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("QueryType", QueryType);
            base.GetObjectData(info, context);
        }
    }
}