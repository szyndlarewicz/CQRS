﻿namespace Framework.Cqrs.Queries
{
    public interface IQueryDispatcher
    {
        TResult Execute<TResult>(IQuery query);
    }
}