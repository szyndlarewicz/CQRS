﻿using System;
using System.Reflection;
using System.Runtime.ExceptionServices;
using Framework.IoC;

namespace Framework.Cqrs.Queries
{
    public sealed class QueryDispatcher : IQueryDispatcher
    {
        private readonly IDependencyResolver _dependencyResolver;
        private readonly IQueryHandlerRegistrar _queryHandlerRegistrar;

        public QueryDispatcher(IDependencyResolver dependencyResolver, IQueryHandlerRegistrar queryHandlerRegistrar)
        {
            _dependencyResolver = dependencyResolver ?? throw new ArgumentNullException(nameof(dependencyResolver));
            _queryHandlerRegistrar = queryHandlerRegistrar ?? throw new ArgumentNullException(nameof(queryHandlerRegistrar));
        }

        public TResult Execute<TResult>(IQuery query)
        {
            Type queryHandlerType = _queryHandlerRegistrar.GetQueryHandler(query);
            MethodInfo executeMethod = queryHandlerType.GetMethod(nameof(IQueryHandler<IQuery, object>.Execute));

            try
            {
                object queryHandler = _dependencyResolver.Get(queryHandlerType);
                return (TResult) executeMethod.Invoke(queryHandler, new object[] { query });
            }
            catch (TargetInvocationException exception)
            {
                ExceptionDispatchInfo.Capture(exception.InnerException).Throw();
                throw;
            }
        }
    }
}