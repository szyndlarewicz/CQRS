﻿using System;
using System.Runtime.Serialization;

namespace Framework.Cqrs.Queries
{
    [Serializable]
    public sealed class QueryHandlerNotFoundException : Exception
    {
        public QueryHandlerNotFoundException(Type queryType)
            : base($"Query handler not found for given query '{queryType.FullName}'")
        {
            QueryType = queryType;
        }

        public Type QueryType { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("QueryType", QueryType);
            base.GetObjectData(info, context);
        }
    }
}