﻿using System;

namespace Framework.Cqrs.Queries
{
    public interface IQueryHandlerRegistrar
    {
        IQueryHandlerRegistrar Register<TQuery, TResult, TQueryHandler>()
            where TQuery : class, IQuery
            where TQueryHandler : class, IQueryHandler<TQuery, TResult>;

        IQueryHandlerRegistrar Register(Type queryType, Type resultType, Type queryHandlerType);

        Type GetQueryHandler(IQuery query);
    }
}