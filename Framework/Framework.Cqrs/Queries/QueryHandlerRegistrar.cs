﻿using System;
using System.Collections.Generic;

namespace Framework.Cqrs.Queries
{
    public sealed class QueryHandlerRegistrar : IQueryHandlerRegistrar
    {
        private readonly IDictionary<Type, Type> _container = new Dictionary<Type, Type>();

        public IQueryHandlerRegistrar Register<TQuery, TResult, TQueryHandler>() 
            where TQuery : class, IQuery
            where TQueryHandler : class, IQueryHandler<TQuery, TResult>
        {
            Type query = typeof(TQuery);

            if (_container.ContainsKey(query))
                throw new QueryHandlerRegisteredException(query);

            _container.Add(query, typeof(TQueryHandler));
            return this;
        }

        public IQueryHandlerRegistrar Register(Type queryType, Type resultType, Type queryHandlerType)
        {
            if (queryType == null)
                throw new ArgumentNullException(nameof(queryType));

            if (!typeof(IQuery).IsAssignableFrom(queryType))
                throw new ArgumentException($"Query '{queryType.FullName}' does not implement interface IQuery");

            if (resultType == null)
                throw new ArgumentNullException(nameof(queryHandlerType));

            if (queryHandlerType == null)
                throw new ArgumentNullException(nameof(queryHandlerType));

            if (!typeof(IQueryHandler<,>).MakeGenericType(queryType, resultType).IsAssignableFrom(queryHandlerType))
                throw new ArgumentException($"Query handler '{queryHandlerType.FullName}' does not implement interface IQueryHandler<{queryType.FullName}, {resultType.FullName}>");

            if (_container.ContainsKey(queryType))
                throw new QueryHandlerRegisteredException(queryType);

            _container.Add(queryType, queryHandlerType);
            return this;
        }

        public Type GetQueryHandler(IQuery query)
        {
            if (query == null)
                throw new ArgumentNullException(nameof(query));

            Type queryType = query.GetType();
            if (!_container.ContainsKey(queryType))
                throw new QueryHandlerNotFoundException(queryType);

            return _container[queryType];
        }
    }
}