﻿using System.Collections.Generic;
using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public interface IEventPublisher
    {
        void Send(IEvent @event);

        void Send(Queue<IEvent> events);

        void Send(IEnumerable<IEvent> events);
    }
}