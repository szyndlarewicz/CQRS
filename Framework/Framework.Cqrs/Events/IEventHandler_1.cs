﻿using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public interface IEventHandler<in TEvent> : IEventHandler
        where TEvent : IEvent
    {
        void Execute(TEvent @event);
    }
}