﻿using System;
using System.Collections.Generic;
using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public interface IEventHandlerRegistrar
    {
        IEventHandlerRegistrar Register<TEvent, TEventHandler>()
            where TEvent : class, IEvent
            where TEventHandler : class, IEventHandler<TEvent>;

        IEventHandlerRegistrar Register(Type eventType, Type eventHandlerType);

        IEnumerable<Type> GetEventHandlers(IEvent @event);
    }
}