﻿using System;
using System.Collections.Generic;
using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public sealed class EventPublisher : IEventPublisher
    {
        private readonly IEventQueue _eventQueue;

        public EventPublisher(IEventQueue eventQueue)
        {
            _eventQueue = eventQueue ?? throw new ArgumentNullException(nameof(eventQueue));
        }

        public void Send(IEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            _eventQueue.Enqueue(@event);
        }

        public void Send(Queue<IEvent> events)
        {
            if (events == null)
                throw new ArgumentNullException(nameof(events));

            while (events.Count > 0)
                _eventQueue.Enqueue(events.Dequeue());
        }

        public void Send(IEnumerable<IEvent> events)
        {
            foreach (IEvent @event in events)
                _eventQueue.Enqueue(@event);
        }
    }
}