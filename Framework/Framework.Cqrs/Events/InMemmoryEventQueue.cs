﻿using System.Collections.Generic;
using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public sealed class InMemmoryEventQueue : Queue<IEvent>, IEventQueue
    {
        public bool HasEvents => Count > 0;
    }
}