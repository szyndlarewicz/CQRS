﻿namespace Framework.Cqrs.Events
{
    public interface IEventProcessor
    {
        void Process();
    }
}