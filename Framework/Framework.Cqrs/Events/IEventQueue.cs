﻿using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public interface IEventQueue
    {
        bool HasEvents { get; }

        void Enqueue(IEvent @event);

        IEvent Dequeue();
    }
}