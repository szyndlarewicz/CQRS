﻿using System;
using System.Collections.Generic;
using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public sealed class EventHandlerRegistrar : IEventHandlerRegistrar
    {
        private readonly IDictionary<Type, IList<Type>> _container = new Dictionary<Type, IList<Type>>();

        public IEventHandlerRegistrar Register<TEvent, TEventHandler>()
            where TEvent : class, IEvent
            where TEventHandler : class, IEventHandler<TEvent>
        {
            Type eventType = typeof(TEvent);

            IList<Type> eventHandlerList;
            if (_container.ContainsKey(eventType))
                eventHandlerList = _container[eventType];
            else
                _container.Add(eventType, eventHandlerList = new List<Type>());

            eventHandlerList.Add(typeof(TEventHandler));
            return this;
        }

        public IEventHandlerRegistrar Register(Type eventType, Type eventHandlerType)
        {
            if (eventType == null)
                throw new ArgumentNullException(nameof(eventType));

            if (!typeof(IEvent).IsAssignableFrom(eventType))
                throw new ArgumentException($"Event '{eventType.FullName}' does not implement interface IEvent");

            if (eventHandlerType == null)
                throw new ArgumentNullException(nameof(eventHandlerType));

            if (!typeof(IEventHandler<>).MakeGenericType(eventType).IsAssignableFrom(eventHandlerType))
                throw new ArgumentException($"Event handler '{eventHandlerType.FullName}' does not implement interface IEventHandler<{eventType.FullName}>");

            IList<Type> eventHandlerList;
            if (_container.ContainsKey(eventType))
                eventHandlerList = _container[eventType];
            else
                _container.Add(eventType, eventHandlerList = new List<Type>());

            eventHandlerList.Add(eventHandlerType);
            return this;
        }

        public IEnumerable<Type> GetEventHandlers(IEvent @event)
        {
            if (@event == null)
                throw new ArgumentNullException(nameof(@event));

            Type eventType = @event.GetType();

            return _container.ContainsKey(eventType)
                ? _container[eventType]
                : new List<Type>();
        }
    }
}