﻿using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public interface IEventDispatcher
    {
        void Execute(IEvent @event);
    }
}
