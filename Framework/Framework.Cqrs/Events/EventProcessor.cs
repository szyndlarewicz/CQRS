﻿using System;
using Framework.Domain;

namespace Framework.Cqrs.Events
{
    public sealed class EventProcessor : IEventProcessor
    {
        private readonly IEventQueue _eventQueue;
        private readonly IEventDispatcher _eventDispather;

        public EventProcessor(IEventQueue eventQueue, IEventDispatcher eventDispather)
        {
            _eventQueue = eventQueue ?? throw new ArgumentNullException(nameof(eventQueue));
            _eventDispather = eventDispather ?? throw new ArgumentNullException(nameof(eventDispather));
        }

        public void Process()
        {
            while (_eventQueue.HasEvents)
            {
                IEvent @event = _eventQueue.Dequeue();
                _eventDispather.Execute(@event);
            }
        }
    }
}