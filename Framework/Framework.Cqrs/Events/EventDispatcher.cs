﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Framework.Domain;
using Framework.IoC;

namespace Framework.Cqrs.Events
{
    public sealed class EventDispatcher : IEventDispatcher
    {
        private readonly IDependencyResolver _dependencyResolver;
        private readonly IEventHandlerRegistrar _eventHandlerRegistrar;
        private readonly IAmbientScopeFactory _ambientScopeFactory;
        private readonly IExceptionHandler _exceptionHandler;

        public EventDispatcher(
            IDependencyResolver dependencyResolver, 
            IEventHandlerRegistrar eventHandlerRegistrar, 
            IAmbientScopeFactory ambinetScopeFactory,
            IExceptionHandler exceptionHandler)
        {
            _dependencyResolver = dependencyResolver ?? throw new ArgumentNullException(nameof(dependencyResolver));
            _eventHandlerRegistrar = eventHandlerRegistrar ?? throw new ArgumentNullException(nameof(eventHandlerRegistrar));
            _ambientScopeFactory = ambinetScopeFactory ?? throw new ArgumentNullException(nameof(ambinetScopeFactory));
            _exceptionHandler = exceptionHandler ?? throw new ArgumentNullException(nameof(exceptionHandler));
        }

        public void Execute(IEvent @event)
        {
            IEnumerable<Type> eventHandlers = _eventHandlerRegistrar.GetEventHandlers(@event);
            foreach (Type eventHandlerType in eventHandlers)
            {
                MethodInfo executeMethod = eventHandlerType.GetMethod(nameof(IEventHandler<IEvent>.Execute));
                using (_ambientScopeFactory.Create())
                {
                    try
                    {
                        object eventHandler = _dependencyResolver.Get(eventHandlerType);
                        executeMethod.Invoke(eventHandler, new object[] { @event });
                    }
                    catch (TargetInvocationException exception)
                    {
                        _exceptionHandler.Handle(exception.InnerException);
                    }
                }
            }
        }
    }
}