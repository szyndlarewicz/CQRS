﻿using System;

namespace Framework.Cqrs
{
    public sealed class NullExceptionHandler : IExceptionHandler
    {
        public void Handle(Exception exception)
        {
        }
    }
}