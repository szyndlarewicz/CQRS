﻿using System;

namespace Framework.IoC
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class AutoBindAttribute : Attribute
    {
    }
}