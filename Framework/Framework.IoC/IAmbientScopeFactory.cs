﻿namespace Framework.IoC
{
    public interface IAmbientScopeFactory
    {
        AmbientScope Create();
    }
}