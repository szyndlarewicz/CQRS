﻿using System;

namespace Framework.IoC
{
    public interface IDependencyRegister
    {
        #region Transient
        IDependencyRegister RegisterTransient(Type definition, Type implementation);

        IDependencyRegister RegisterTransient<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition;

        IDependencyRegister RegisterTransient(Type implementation);

        IDependencyRegister RegisterTransient<TImplementation>()
            where TImplementation : class;
        #endregion

        #region AmbientScope
        IDependencyRegister RegisterAmbientScope(Type definition, Type implementation);

        IDependencyRegister RegisterAmbientScope<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition;

        IDependencyRegister RegisterAmbientScope(Type implementation);

        IDependencyRegister RegisterAmbientScope<TImplementation>()
            where TImplementation : class;

        IDependencyRegister RegisterAmbientScope(Type implementation, object value);

        IDependencyRegister RegisterAmbientScope<TImplementation>(TImplementation value)
            where TImplementation : class;

        IDependencyRegister RegisterAmbientScope(Type definition, Type implementation, object value);

        IDependencyRegister RegisterAmbientScope<TDefinition, TImplementation>(TImplementation value)
            where TDefinition : class
            where TImplementation : class, TDefinition;
        #endregion

        #region RequestScope
        IDependencyRegister RegisterRequestScope(Type definition, Type implementation);

        IDependencyRegister RegisterRequestScope<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition;

        IDependencyRegister RegisterRequestScope(Type implementation);

        IDependencyRegister RegisterRequestScope<TImplementation>()
            where TImplementation : class;

        IDependencyRegister RegisterRequestScope(Type implementation, object value);

        IDependencyRegister RegisterRequestScope<TImplementation>(TImplementation value)
            where TImplementation : class;

        IDependencyRegister RegisterRequestScope(Type definition, Type implementation, object value);

        IDependencyRegister RegisterRequestScope<TDefinition, TImplementation>(TImplementation value)
            where TDefinition : class
            where TImplementation : class, TDefinition;
        #endregion

        #region Singleton
        IDependencyRegister RegisterSingleton(Type definition, Type implementation);

        IDependencyRegister RegisterSingleton<TDefinition, TImplementation>()
            where TDefinition : class
            where TImplementation : class, TDefinition;

        IDependencyRegister RegisterSingleton(Type implementation);

        IDependencyRegister RegisterSingleton<TImplementation>()
            where TImplementation : class;

        IDependencyRegister RegisterSingleton(Type implementation, object value);

        IDependencyRegister RegisterSingleton<TImplementation>(TImplementation value)
            where TImplementation : class;

        IDependencyRegister RegisterSingleton(Type definition, Type implementation, object value);

        IDependencyRegister RegisterSingleton<TDefinition, TImplementation>(TImplementation value)
            where TDefinition : class
            where TImplementation : class, TDefinition;
        #endregion

        IDependencyResolver Done();
    }
}