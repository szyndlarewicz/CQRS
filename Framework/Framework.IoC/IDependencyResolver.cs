﻿using System;

namespace Framework.IoC
{
    public interface IDependencyResolver
    {
        T Get<T>();

        T TryGet<T>();

        object Get(Type type);

        object TryGet(Type type);
    }
}