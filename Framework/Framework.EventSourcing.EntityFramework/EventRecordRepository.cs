﻿using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Domain;

namespace Framework.EventSourcing.EntityFramework
{
    public sealed class EventRecordRepository : IEventRecordRepository
    {
        private readonly EventStoreDbContext _context;

        private readonly IEventSerializer _eventSerializer;

        public EventRecordRepository(EventStoreDbContext context, IEventSerializer eventSerializer)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _eventSerializer = eventSerializer ?? throw new ArgumentNullException(nameof(eventSerializer));
        }

        public void AddEvent(EventRecord eventRecord)
        {
            if (eventRecord == null)
                throw new ArgumentNullException(nameof(eventRecord));

            _context.Events.Add(eventRecord);
        }

        public void AddEvent(IEnumerable<EventRecord> eventRecords)
        {
            if (eventRecords == null)
                throw new ArgumentNullException(nameof(eventRecords));

            _context.Events.AddRange(eventRecords);
        }

        public AggregateVersion GetAggregateVersion(EntityIdentity aggregateId)
        {
            if (aggregateId == null)
                throw new ArgumentNullException(nameof(aggregateId));

            return (AggregateVersion)_context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Select(e => (long?)e.AggregateVersion)
                .Max();
        }

        public IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId)
        {
            if (aggregateId == null)
                throw new ArgumentNullException(nameof(aggregateId));

            EventRecord snapshot = FindSnapshot(aggregateId);

            var query = _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => !e.IsSnapshot);

            if (snapshot != null)
                query = query.Where(e => e.AggregateVersion > snapshot.AggregateVersion);

            IList<string> serializedEvents = query
                .OrderBy(e => e.CreatedAt)
                .Select(e => e.EventData)
                .ToList();

            if (snapshot != null)
                serializedEvents.Insert(0, snapshot.EventData);

            return serializedEvents.Select(e => _eventSerializer.Deserialize(e));
        }

        public IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, AggregateVersion version)
        {
            if (aggregateId == null)
                throw new ArgumentNullException(nameof(aggregateId));

            if (version == null)
                throw new ArgumentNullException(nameof(version));

            EventRecord snapshot = FindSnapshot(aggregateId, version);

            var query = _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => !e.IsSnapshot);

            if (snapshot != null)
                query = query.Where(e => e.AggregateVersion > snapshot.AggregateVersion);

            IList<string> serializedEvents = query
                .Where(e => e.AggregateVersion <= version)
                .OrderBy(e => e.CreatedAt)
                .Select(e => e.EventData)
                .ToList();

            if (snapshot != null)
                serializedEvents.Insert(0, snapshot.EventData);

            return serializedEvents.Select(e => _eventSerializer.Deserialize(e));
        }

        public IEnumerable<IEvent> GetEvents(EntityIdentity aggregateId, DateTime dateTime)
        {
            if (aggregateId == null)
                throw new ArgumentNullException(nameof(aggregateId));

            EventRecord snapshot = FindSnapshot(aggregateId, dateTime);

            var query = _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => !e.IsSnapshot);

            if (snapshot != null)
                query = query.Where(e => e.AggregateVersion > snapshot.AggregateVersion);

            IList<string> serializedEvents = query
                .Where(e => e.CreatedAt <= dateTime)
                .OrderBy(e => e.CreatedAt)
                .Select(e => e.EventData)
                .ToList();

            if (snapshot != null)
                serializedEvents.Insert(0, snapshot.EventData);

            return serializedEvents.Select(e => _eventSerializer.Deserialize(e));
        }

        public IEnumerable<AuditModel> GetAuditLog(EntityIdentity aggregateId)
        {
            if (aggregateId == null)
                throw new ArgumentNullException(nameof(aggregateId));

            return _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => !e.IsSnapshot)
                .OrderBy(e => e.CreatedAt)
                .ToList()
                .Select(e => new AuditModel(e.CreatedAt, e.CreatedBy, _eventSerializer.Deserialize(e.EventData)));
        }

        private EventRecord FindSnapshot(EntityIdentity aggregateId)
        {
            return _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => e.IsSnapshot)
                .OrderByDescending(c => c.CreatedAt)
                .FirstOrDefault();
        }

        private EventRecord FindSnapshot(EntityIdentity aggregateId, DateTime dateTime)
        {
            return _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => e.IsSnapshot)
                .Where(e => e.CreatedAt <= dateTime)
                .OrderByDescending(c => c.CreatedAt)
                .FirstOrDefault();
        }

        private EventRecord FindSnapshot(EntityIdentity aggregateId, long version)
        {
            return _context.Events
                .Where(e => e.AggregateId == aggregateId)
                .Where(e => e.IsSnapshot)
                .Where(e => e.AggregateVersion <= version)
                .OrderByDescending(c => c.CreatedAt)
                .FirstOrDefault();
        }
    }
}