﻿using System.Data.Entity.ModelConfiguration;

namespace Framework.EventSourcing.EntityFramework
{
    public class EventRecordMap : EntityTypeConfiguration<EventRecord>
    {
        public EventRecordMap()
        {
            ToTable("Events", "dbo");
            HasKey(c => new { c.AggregateId, c.AggregateVersion, c.IsSnapshot });
            Property(c => c.CreatedAt).IsRequired();

            Property(c => c.AggregateId).IsRequired();
            Property(c => c.AggregateVersion).IsRequired();
            Property(c => c.AggregateHash).IsRequired();

            Property(c => c.IsSnapshot).IsRequired();

            Property(c => c.EventData).IsRequired().HasMaxLength(null);
        }
    }
}