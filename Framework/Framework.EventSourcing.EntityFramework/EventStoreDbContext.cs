﻿using System.Data.Entity;

namespace Framework.EventSourcing.EntityFramework
{
    public class EventStoreDbContext : DbContext
    {
        public const string DefaultDbName = "name=eventstore";

        public EventStoreDbContext()
            : this(DefaultDbName)
        {
        }

        public EventStoreDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<EventStoreDbContext>());

            if (!Database.Exists())
                Database.Initialize(true);
        }

        public DbSet<EventRecord> Events { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new EventRecordMap());
        }
    }
}