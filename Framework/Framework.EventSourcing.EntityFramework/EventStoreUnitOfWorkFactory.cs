﻿using System;

namespace Framework.EventSourcing.EntityFramework
{
    public sealed class EventStoreUnitOfWorkFactory : IEventStoreUnitOfWorkFactory
    {
        private readonly IEventSerializer _eventSerializer;

        public EventStoreUnitOfWorkFactory(IEventSerializer eventSerializer)
        {
            _eventSerializer = eventSerializer ?? throw new ArgumentNullException(nameof(eventSerializer));
        }

        public IEventStoreUnitOfWork Create()
        {
            return new EventStoreUnitOfWork(new EventStoreDbContext(), _eventSerializer);
        }
    }
}