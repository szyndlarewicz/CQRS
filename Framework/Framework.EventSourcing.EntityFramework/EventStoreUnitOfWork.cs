﻿using System;
using Framework.Core.Persistence;

namespace Framework.EventSourcing.EntityFramework
{
    public sealed class EventStoreUnitOfWork : IEventStoreUnitOfWork
    {
        private readonly EventStoreDbContext _context;

        public EventStoreUnitOfWork(EventStoreDbContext context, IEventSerializer eventSerializer)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));

            if (eventSerializer == null)
                throw new ArgumentNullException(nameof(eventSerializer));

            EventRepository = new EventRecordRepository(context, eventSerializer);
        }

        public IEventRecordRepository EventRepository { get; }

        public ITransaction BeginTransaction()
        {
            return new DbTransaction(_context);
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}