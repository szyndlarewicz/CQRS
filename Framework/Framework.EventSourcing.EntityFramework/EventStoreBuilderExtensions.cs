namespace Framework.EventSourcing.EntityFramework
{
    public static class EventStoreBuilderExtensions
    {
        public static EventStoreBuilder UseEntityFramework(this EventStoreBuilder eventStoreBuilder)
        {
            return eventStoreBuilder.Use(new EventStoreUnitOfWorkFactory(eventStoreBuilder.EventSerializer));
        }

        public static EventStoreBuilder CreateDatabase(this EventStoreBuilder eventStoreBuilder)
        {
            eventStoreBuilder.EventStoreUnitOfWorkFactory.Create();
            return eventStoreBuilder;
        }
    }
}