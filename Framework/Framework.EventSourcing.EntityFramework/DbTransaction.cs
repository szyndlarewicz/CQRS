﻿using System;
using System.Data.Entity;
using Framework.Core.Persistence;

namespace Framework.EventSourcing.EntityFramework
{
    public sealed class DbTransaction : ITransaction
    {
        private readonly DbContextTransaction _transaction;

        public DbTransaction(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            _transaction = context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }
    }
}