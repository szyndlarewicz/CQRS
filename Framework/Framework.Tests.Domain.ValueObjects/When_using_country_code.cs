﻿using System;
using Framework.Domain.ValueObjects;
using NUnit.Framework;

namespace Framework.Tests.Domain.ValueObjects
{
    [TestFixture]
    public sealed class When_using_country_code
    {
        [TestCase]
        public void It_should_create_country_code_via_constructor()
        {
            Assert.NotNull(new CountryCode("POL"));
        }

        [TestCase]
        public void It_should_create_country_code_via_property()
        {
            Assert.NotNull(CountryCode.Pol);
        }

        [TestCase]
        public void It_should_throw_exception_when_given_unknown_country_name()
        {
            string unknown = "XXX";
            Assert.Throws<ArgumentException>(() => new CountryCode(unknown));
        }

        [TestCase]
        public void It_should_be_reference_equals()
        {
            CountryCode poland = new CountryCode("POL");
            CountryCode polska = poland;
            Assert.AreEqual(poland, polska);
        }

        [TestCase]
        public void It_should_be_equals_when_given_no_value()
        {
            CountryCode poland = null;
            CountryCode polska = null;
            Assert.AreEqual(poland, polska);
        }

        [TestCase]
        public void It_should_not_be_when_one_of_given_value_is_null()
        {
            CountryCode poland = new CountryCode("POL");
            CountryCode polska = null;
            Assert.AreNotEqual(poland, polska);
        }

        [TestCase]
        public void It_should_cast_country_to_string()
        {
            CountryCode poland = CountryCode.Pol;
            string polska = poland;
            Assert.AreEqual("POL", polska);
        }

        [TestCase]
        public void It_should_cast_string_to_country()
        {
            CountryCode polska = (CountryCode)"POL";
            Assert.AreEqual(CountryCode.Pol, polska);
        }

        [TestCase]
        public void It_should_be_converted_to_country_code()
        {
            CountryCode code = CountryCode.Pol;
            CountryName name = code.ToCountryName();
            Assert.AreEqual(CountryName.Poland, name);
        }
    }
}