﻿using System;
using Framework.Domain.ValueObjects;
using NUnit.Framework;

namespace Framework.Tests.Domain.ValueObjects
{
    [TestFixture]
    public sealed class When_using_country_name
    {
        [TestCase]
        public void It_should_create_country_name_via_constructor()
        {
            Assert.NotNull(new CountryName("Poland"));
        }

        [TestCase]
        public void It_should_create_country_name_via_property()
        {
            Assert.NotNull(CountryName.Poland);
        }

        [TestCase]
        public void It_should_throw_exception_when_given_unknown_country_name()
        {
            string germany = "Deutschland";
            Assert.Throws<ArgumentException>(() => new CountryName(germany));
        }

        [TestCase]
        public void It_should_be_reference_equals()
        {
            CountryName poland = new CountryName("Poland");
            CountryName polska = poland;
            Assert.AreEqual(poland, polska);
        }

        [TestCase]
        public void It_should_be_equals_when_given_no_value()
        {
            CountryName poland = null;
            CountryName polska = null;
            Assert.AreEqual(poland, polska);
        }

        [TestCase]
        public void It_should_not_be_when_one_of_given_value_is_null()
        {
            CountryName poland = new CountryName("Poland");
            CountryName polska = null;
            Assert.AreNotEqual(poland, polska);
        }

        [TestCase]
        public void It_should_cast_country_to_string()
        {
            CountryName poland = CountryName.Poland;
            string polska = poland;
            Assert.AreEqual("Poland", polska);
        }

        [TestCase]
        public void It_should_cast_string_to_country()
        {
            CountryName polska = (CountryName)"Poland";
            Assert.AreEqual(CountryName.Poland, polska);
        }

        [TestCase]
        public void It_should_be_converted_to_country_code()
        {
            CountryName polska = CountryName.Poland;
            CountryCode pln = polska.ToCountryCode();
            Assert.AreEqual(CountryCode.Pol, pln);
        }
    }
}