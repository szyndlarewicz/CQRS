﻿using $ext_safeprojectname$.Domain.Events;
using Framework.Core.IoC;
using Framework.Cqrs.Events;

namespace $ext_safeprojectname$.Handlers.Events
{
    [AutoBind]
    public sealed class ProjectNameChangedEventHandler : IEventHandler<ProjectNameChangedEvent>
    {
        public void Execute(ProjectNameChangedEvent @event)
        {
        }
    }
}