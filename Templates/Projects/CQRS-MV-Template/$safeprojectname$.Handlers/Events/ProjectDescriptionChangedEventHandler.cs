﻿using $ext_safeprojectname$.Domain.Events;
using Framework.Core.IoC;
using Framework.Cqrs.Events;

namespace $ext_safeprojectname$.Handlers.Events
{
    [AutoBind]
    public sealed class ProjectDescriptionChangedEventHandler : IEventHandler<ProjectDescriptionChangedEvent>
    {
        public void Execute(ProjectDescriptionChangedEvent @event)
        {
        }
    }
}