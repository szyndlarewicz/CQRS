﻿using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain.Factories;
using $ext_safeprojectname$.Domain.Repositories;
using Framework.Core.IoC;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Handlers.Commands
{
    [AutoBind]
    public sealed class AddProjectCommandHandler : ICommandHandler<AddProjectCommand>
    {
        private readonly IProjectRepository _projectRepository;
        private readonly IProjectFactory _projectFactory;

        public AddProjectCommandHandler(IProjectRepository projectRepository, IProjectFactory projectFactory)
        {
            _projectRepository = projectRepository;
            _projectFactory = projectFactory;
        }

        public void Execute(AddProjectCommand command)
        {
            var project = _projectFactory.Create(command.CommandId, command.ProjectName, command.ProjectDescription);
            _projectRepository.Add(project);
        }
    }
}