﻿using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.ValueObjects;
using Framework.Core.IoC;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Handlers.Commands
{
    [AutoBind]
    public sealed class ChangeProjectDescriptionCommandHandler : ICommandHandler<ChangeProjectDescriptionCommand>
    {
        private readonly IProjectRepository _projectRepository;

        public ChangeProjectDescriptionCommandHandler(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public void Execute(ChangeProjectDescriptionCommand command)
        {
            ProjectEntity project = _projectRepository.Get((ProjectId)command.ProjectId);
            project.ChangeDescription((ProjectDescription) command.ProjectDescription);
            _projectRepository.Change(project);
        }
    }
}
