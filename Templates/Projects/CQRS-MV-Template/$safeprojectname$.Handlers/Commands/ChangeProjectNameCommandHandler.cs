﻿using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.ValueObjects;
using Framework.Core.IoC;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Handlers.Commands
{
    [AutoBind]
    public sealed class ChangeProjectNameCommandHandler : ICommandHandler<ChangeProjectNameCommand>
    {
        private readonly IProjectRepository _projectRepository;

        public ChangeProjectNameCommandHandler(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public void Execute(ChangeProjectNameCommand command)
        {
            ProjectEntity project = _projectRepository.Get((ProjectId) command.ProjectId);
            project.ChangeName((ProjectName) command.ProjectName);
            _projectRepository.Change(project);
        }
    }
}