﻿using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.ValueObjects;
using Framework.Core.IoC;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Handlers.Commands
{
    [AutoBind]
    public sealed class RemoveProjectCommandHandler : ICommandHandler<RemoveProjectCommand>
    {
        private readonly IProjectRepository _projectRepository;

        public RemoveProjectCommandHandler(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public void Execute(RemoveProjectCommand command)
        {
            ProjectEntity project = _projectRepository.Get((ProjectId)command.ProjectId);
            project.Remove();
            _projectRepository.Remove(project);
        }
    }
}