﻿using $ext_safeprojectname$.Contracts.Queries;
using $ext_safeprojectname$.Contracts.Views;
using $ext_safeprojectname$.Search;
using Framework.Core.IoC;
using Framework.Cqrs.Queries;
using Framework.Search.Contracts.Views;

namespace $ext_safeprojectname$.Handlers.Queries
{
    [AutoBind]
    public sealed class FindProjectsQueryHandler : IQueryHandler<FindProjectsQuery, PageCollection<ProjectListItemView>>
    {
        private readonly IProjectFinder _projectFinder;

        public FindProjectsQueryHandler(IProjectFinder projectFinder)
        {
            _projectFinder = projectFinder;
        }

        public PageCollection<ProjectListItemView> Execute(FindProjectsQuery query)
        {
            return _projectFinder.FindBy(query.SearchText, query.SortOrder, query.PageIndex, query.PageSize);
        }
    }
}