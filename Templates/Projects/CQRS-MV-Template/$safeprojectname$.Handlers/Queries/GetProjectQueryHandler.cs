﻿using $ext_safeprojectname$.Contracts.Queries;
using $ext_safeprojectname$.Contracts.Views;
using $ext_safeprojectname$.Search;
using Framework.Core.IoC;
using Framework.Cqrs.Queries;

namespace $ext_safeprojectname$.Handlers.Queries
{
    [AutoBind]
    public sealed class GetProjectQueryHandler : IQueryHandler<GetProjectQuery, ProjectDetailsView>
    {
        private readonly IProjectFinder _projectFinder;

        public GetProjectQueryHandler(IProjectFinder projectFinder)
        {
            _projectFinder = projectFinder;
        }

        public ProjectDetailsView Execute(GetProjectQuery query)
        {
            return _projectFinder.GetProject(query.ProjectId);
        }
    }
}