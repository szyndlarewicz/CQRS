﻿using System;
using Framework.Search.Contracts.Views;

namespace $ext_safeprojectname$.Contracts.Views
{
    public sealed class ProjectDetailsView : View
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}