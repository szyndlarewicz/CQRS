﻿using System;
using Framework.Search.Contracts.Views;

namespace $ext_safeprojectname$.Contracts.Views
{
    public sealed class ProjectListItemView : View
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}