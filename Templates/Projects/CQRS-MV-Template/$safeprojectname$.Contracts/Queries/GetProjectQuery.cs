﻿using System;
using Framework.Cqrs.Queries;

namespace $ext_safeprojectname$.Contracts.Queries
{
    public sealed class GetProjectQuery : IQuery
    {
        public GetProjectQuery(Guid projectId)
        {
            ProjectId = projectId;
        }

        public Guid ProjectId { get; }
    }
}