﻿using $ext_safeprojectname$.Contracts.Types;
using Framework.Cqrs.Queries;
using Framework.Search.Contracts.Queries;
using Framework.Search.Contracts.Types;

namespace $ext_safeprojectname$.Contracts.Queries
{
    public sealed class FindProjectsQuery : PagedQuery, IQuery
    {
        public FindProjectsQuery(string searchText, string orderDirection, string orderExpression, int pageSize, int pageIndex)
            : base(new OrderDirection(orderDirection), new ProjectOrderExpression(orderExpression), new PageSize(pageSize), new PageIndex(pageIndex))
        {
            SearchText = searchText;
        }

        public string SearchText { get; }
    }
}