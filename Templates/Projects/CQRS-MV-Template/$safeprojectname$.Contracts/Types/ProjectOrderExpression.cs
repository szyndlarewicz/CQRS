﻿using System.Linq;
using Framework.Search.Contracts.Types;

namespace $ext_safeprojectname$.Contracts.Types
{
    public sealed class ProjectOrderExpression : OrderExpression
    {
        public const string Name = "Name";

        private static readonly string[] AvailableExpressions = { Name };

        public ProjectOrderExpression(string sortExpression)
            : base(sortExpression)
        {
            if (!AvailableExpressions.Contains(sortExpression))
                throw new UnrecognizedOrderExpressionException(sortExpression);
        }
    }
}