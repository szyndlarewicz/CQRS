﻿using System;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Contracts.Commands
{
    public sealed class ChangeProjectNameCommand : Command
    {
        public ChangeProjectNameCommand(Guid projectId, string projectName)
        {
            ProjectId = projectId;
            ProjectName = projectName;
        }

        public Guid ProjectId { get; }

        public string ProjectName { get; }
    }
}