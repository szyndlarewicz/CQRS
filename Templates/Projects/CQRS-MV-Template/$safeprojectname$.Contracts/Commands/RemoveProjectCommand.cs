﻿using System;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Contracts.Commands
{
    public sealed class RemoveProjectCommand : Command
    {
        public RemoveProjectCommand(Guid projectId)
        {
            ProjectId = projectId;
        }

        public Guid ProjectId { get; }
    }
}