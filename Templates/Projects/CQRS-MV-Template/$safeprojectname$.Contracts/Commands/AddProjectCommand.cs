﻿using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Contracts.Commands
{
    public sealed class AddProjectCommand : Command
    {
        public AddProjectCommand(string projectName, string projectDescription)
        {
            ProjectName = projectName;
            ProjectDescription = projectDescription;
        }

        public string ProjectName { get; }

        public string ProjectDescription { get; }
    }
}