﻿using System;
using Framework.Cqrs.Commands;

namespace $ext_safeprojectname$.Contracts.Commands
{
    public sealed class ChangeProjectDescriptionCommand : Command
    {
        public ChangeProjectDescriptionCommand(Guid projectId, string projectDescription)
        {
            ProjectId = projectId;
            ProjectDescription = projectDescription;
        }

        public Guid ProjectId { get; }

        public string ProjectDescription { get; }
    }
}