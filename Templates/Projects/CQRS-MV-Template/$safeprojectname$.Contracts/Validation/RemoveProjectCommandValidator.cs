﻿using $ext_safeprojectname$.Contracts.Commands;
using FluentValidation;
using Framework.Core.IoC;
using Framework.Cqrs.FluentValidation;

namespace $ext_safeprojectname$.Contracts.Validation
{
    [AutoBind]
    public sealed class RemoveProjectCommandValidator : AbstractCommandValidator<RemoveProjectCommand>
    {
        public RemoveProjectCommandValidator()
        {
            RuleFor(v => v.ProjectId).NotEmpty();
        }
    }
}