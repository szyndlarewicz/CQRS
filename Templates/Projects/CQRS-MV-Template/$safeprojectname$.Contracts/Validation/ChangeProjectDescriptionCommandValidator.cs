﻿using $ext_safeprojectname$.Contracts.Commands;
using FluentValidation;
using Framework.Core.IoC;
using Framework.Cqrs.FluentValidation;

namespace $ext_safeprojectname$.Contracts.Validation
{
    [AutoBind]
    public sealed class ChangeProjectDescriptionCommandValidator : AbstractCommandValidator<ChangeProjectDescriptionCommand>
    {
        public ChangeProjectDescriptionCommandValidator()
        {
            RuleFor(v => v.ProjectId).NotEmpty();
        }
    }
}