﻿using $ext_safeprojectname$.Contracts.Commands;
using FluentValidation;
using Framework.Core.IoC;
using Framework.Cqrs.FluentValidation;

namespace $ext_safeprojectname$.Contracts.Validation
{
    [AutoBind]
    public sealed class ChangeProjectNameCommandValidator : AbstractCommandValidator<ChangeProjectNameCommand>
    {
        public ChangeProjectNameCommandValidator()
        {
            RuleFor(v => v.ProjectId).NotEmpty();
            RuleFor(v => v.ProjectName).NotEmpty();
            RuleFor(v => v.ProjectName).MaximumLength(100);
        }
    }
}