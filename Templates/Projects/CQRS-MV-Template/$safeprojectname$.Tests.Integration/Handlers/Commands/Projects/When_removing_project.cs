﻿using System;
using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.States;
using $ext_safeprojectname$.Handlers.Commands;
using $ext_safeprojectname$.Infrastructure.Persistence;
using $ext_safeprojectname$.Infrastructure.Repositories;
using $ext_safeprojectname$.Tests.Integration.Data.Builders;
using $ext_safeprojectname$.Tests.Integration.Data.Readers;
using $ext_safeprojectname$.Tests.Integration.Dependencies;
using Framework.Core.IoC;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Integration.Handlers.Commands.Projects
{
    [TestFixture]
    public class When_removing_project
    {
        private readonly Guid _projectId = Guid.NewGuid();
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = AppDependencyScope.Instance
                .ConfigureCore()
                .RegisterSingleton<ChangeProjectNameCommandHandler>()
                .RegisterTransient<IProjectRepository, ProjectRepository>()
                .RegisterTransient<AppDbContext>()
                .Done();

            AppDataBuilder.Instance
                    .CleanUp()
                .InContext.App
                    .CreateProject(_projectId, "Project A", "Description A")
                .InContext.Database
                    .Build();
        }

        [TestCase]
        public void It_should_remove_project()
        {
            var command = new RemoveProjectCommand(_projectId);
            var commandHandler = _resolver.Get<RemoveProjectCommandHandler>();
            commandHandler.Execute(command);

            var reader = new AppContextDataReader();
            ProjectPoco project = reader.GetProjectBy(_projectId);
            Assert.IsNull(project);
        }
    }
}