﻿using System;
using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.States;
using $ext_safeprojectname$.Handlers.Commands;
using $ext_safeprojectname$.Infrastructure.Persistence;
using $ext_safeprojectname$.Infrastructure.Repositories;
using $ext_safeprojectname$.Tests.Integration.Data.Builders;
using $ext_safeprojectname$.Tests.Integration.Data.Readers;
using $ext_safeprojectname$.Tests.Integration.Dependencies;
using Framework.Core.IoC;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Integration.Handlers.Commands.Projects
{
    [TestFixture]
    public class When_changing_project_description
    {
        private readonly Guid _projectId = Guid.NewGuid();
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = AppDependencyScope.Instance
                .ConfigureCore()
                .RegisterSingleton<ChangeProjectDescriptionCommandHandler>()
                .RegisterTransient<IProjectRepository, ProjectRepository>()
                .RegisterTransient<AppDbContext>()
                .Done();

            AppDataBuilder.Instance
                    .CleanUp()
                .InContext.App
                    .CreateProject(_projectId, "Project A", "Description A")
                .InContext.Database
                    .Build();
        }

        [TestCase]
        public void It_should_change_project_description()
        {
            var command = new ChangeProjectDescriptionCommand(_projectId, "Description B");
            var commandHandler = _resolver.Get<ChangeProjectDescriptionCommandHandler>();
            commandHandler.Execute(command);

            var reader = new AppContextDataReader();
            ProjectPoco project = reader.GetProjectBy(_projectId);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(project);
                Assert.AreEqual("Description B", project.Description); 
            });
        }
    }
}