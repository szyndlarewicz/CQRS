﻿using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Domain.Factories;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.States;
using $ext_safeprojectname$.Handlers.Commands;
using $ext_safeprojectname$.Infrastructure.Factories;
using $ext_safeprojectname$.Infrastructure.Persistence;
using $ext_safeprojectname$.Infrastructure.Repositories;
using $ext_safeprojectname$.Tests.Integration.Data.Builders;
using $ext_safeprojectname$.Tests.Integration.Data.Readers;
using $ext_safeprojectname$.Tests.Integration.Dependencies;
using Framework.Core.IoC;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Integration.Handlers.Commands.Projects
{
    [TestFixture]
    public class When_adding_project
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = AppDependencyScope.Instance
                .ConfigureCore()
                .RegisterSingleton<AddProjectCommandHandler>()
                .RegisterTransient<IProjectFactory, ProjectFactory>()
                .RegisterTransient<IProjectRepository, ProjectRepository>()
				.RegisterSingleton<AppDbContext>()
                .Done();

            AppDataBuilder.Instance
                .CleanUp();
        }

        [TestCase]
        public void It_should_create_project()
        {
            var command = new AddProjectCommand("Project A", "Description A");
            var commandHandler = _resolver.Get<AddProjectCommandHandler>();
            commandHandler.Execute(command);

            var reader = new AppContextDataReader();
            ProjectPoco project = reader.GetProjectBy(command.CommandId);

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(project);
                Assert.AreEqual("Project A", project.Name);
                Assert.AreEqual("Description A", project.Description);
            });
        }
    }
}