﻿using System;
using $ext_safeprojectname$.Contracts.Queries;
using $ext_safeprojectname$.Contracts.Views;
using $ext_safeprojectname$.Handlers.Queries;
using $ext_safeprojectname$.Search;
using $ext_safeprojectname$.Tests.Integration.Data.Builders;
using $ext_safeprojectname$.Tests.Integration.Dependencies;
using Framework.Core.IoC;
using Framework.Search.Dapper;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Integration.Handlers.Queries
{
    [TestFixture]
    public class When_getting_project
    {
        private readonly Guid _projectId = Guid.NewGuid();
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = AppDependencyScope.Instance
                .ConfigureCore()
                .RegisterSingleton<ReadDbConnection>()
                .RegisterTransient<IProjectFinder, ProjectFinder>()
                .RegisterTransient<GetProjectQueryHandler>()
                .Done();

            AppDataBuilder.Instance
                    .CleanUp()
                .InContext.App
                    .CreateProject(Guid.NewGuid(), "Project A", "Description A")
                    .CreateProject(_projectId, "Project B", "Description B")
                    .CreateProject(Guid.NewGuid(), "Project C", "Description C")
                .InContext.Database
                    .Build();
        }

        [TestCase]
        public void It_should_return_project_details()
        {
            var query = new GetProjectQuery(_projectId);
            var queryHandler = _resolver.Get<GetProjectQueryHandler>();
            ProjectDetailsView result = queryHandler.Execute(query);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(_projectId, result.Id);
                Assert.AreEqual("Project B", result.Name);
                Assert.AreEqual("Description B", result.Description);
            });
        }
    }
}