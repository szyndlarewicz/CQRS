﻿using System;
using System.Linq;
using $ext_safeprojectname$.Contracts.Queries;
using $ext_safeprojectname$.Contracts.Views;
using $ext_safeprojectname$.Handlers.Queries;
using $ext_safeprojectname$.Search;
using $ext_safeprojectname$.Tests.Integration.Data.Builders;
using $ext_safeprojectname$.Tests.Integration.Dependencies;
using Framework.Core.IoC;
using Framework.Search.Contracts.Views;
using Framework.Search.Dapper;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Integration.Handlers.Queries
{
    [TestFixture]
    public class When_finding_projects
    {
        private IDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _resolver = AppDependencyScope.Instance
                .ConfigureCore()
                .RegisterSingleton<ReadDbConnection>()
                .RegisterTransient<IProjectFinder, ProjectFinder>()
                .RegisterTransient<FindProjectsQueryHandler>()
                .Done();

            AppDataBuilder.Instance
                    .CleanUp()
                .InContext.App
                    .CreateProject(Guid.NewGuid(), "Project A", "Description A")
                    .CreateProject(Guid.NewGuid(), "Project B", "Description B")
                    .CreateProject(Guid.NewGuid(), "Project C", "Description C")
                .InContext.Database
                    .Build();
        }

        [TestCase]
        public void It_should_return_projects_with_pagination()
        {
            var query = new FindProjectsQuery(searchText: "Project", orderDirection: "Ascending", orderExpression: "Name", pageIndex: 0, pageSize: 25);
            var queryHandler = _resolver.Get<FindProjectsQueryHandler>();
            PageCollection<ProjectListItemView> results = queryHandler.Execute(query);

            Assert.Multiple(() =>
            {
                Assert.AreEqual(3, results.Count);
                Assert.IsTrue(results.Items.Any(c => c.Name == "Project A"));
                Assert.IsTrue(results.Items.Any(c => c.Name == "Project B"));
                Assert.IsTrue(results.Items.Any(c => c.Name == "Project C"));
            });
        }
    }
}