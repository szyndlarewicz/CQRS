﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("$ext_safeprojectname$.Tests.Integration")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("$ext_safeprojectname$.Tests.Integration")]
[assembly: AssemblyCopyright("Copyright © $username$ $year$")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("$ext_guid8$")]
[assembly: AssemblyVersion("1.0.0.0")]