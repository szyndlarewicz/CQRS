﻿using System.Collections.Generic;
using Framework.Testing.Data.Builders;

namespace $ext_safeprojectname$.Tests.Integration.Data.Builders
{
    public sealed class AppContextRouter : ContextRouter<AppDataBuilder>
    {
        public AppContextRouter(Queue<BuilderCommand> actions, AppDataBuilder builder)
            : base(actions, builder)
        {
            App = new AppDataContextBuilder(actions, this);
        }

        public AppDataContextBuilder App { get; }
    }
}