﻿using Framework.Testing;
using Framework.Testing.Data.Builders;

namespace $ext_safeprojectname$.Tests.Integration.Data.Builders
{
    public sealed class AppDataBuilder : DataBuilder<AppDataBuilder, AppContextRouter>
    {
        public AppDataBuilder()
            : base(new WriteDbConnection("name=database"))
        {
        }

        public override AppDataBuilder CleanUp()
        {
            Actions.Enqueue(new BuilderCommand("delete from dbo.Projects"));
            return this;
        }
    }
}