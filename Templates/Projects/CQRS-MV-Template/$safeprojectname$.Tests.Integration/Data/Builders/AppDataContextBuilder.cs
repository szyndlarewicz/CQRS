﻿using System;
using System.Collections.Generic;
using Framework.Testing.Data.Builders;

namespace $ext_safeprojectname$.Tests.Integration.Data.Builders
{
    public sealed class AppDataContextBuilder : DataContextBuilder<AppContextRouter>
    {
        public AppDataContextBuilder(Queue<BuilderCommand> actions, AppContextRouter builder) 
            : base(actions, builder)
        {
        }

        public AppDataContextBuilder CreateProject(Guid projectId, string projectName, string projectDescription)
        {
            const string sql = "INSERT INTO dbo.Projects(Id, Name, Description) VALUES (@Id, @Name, @Description)";
            var parameters = new { Id = projectId, Name = projectName, Description = projectDescription };
            Actions.Enqueue(new BuilderCommand(sql, parameters));
            return this;
        }
    }
}