﻿using System;
using System.Linq;
using $ext_safeprojectname$.Domain.States;
using Dapper;
using Framework.Testing.Data.Readers;

namespace $ext_safeprojectname$.Tests.Integration.Data.Readers
{
    public sealed class AppContextDataReader : DataReader
    {
        public ProjectPoco GetProjectBy(Guid projectId)
        { 
            const string sql = "SELECT * FROM dbo.Projects WHERE Id = @ProjectId";
            var parameters = new { ProjectId = projectId };
            ProjectPoco project = DbConnection.Connection.Query<ProjectPoco>(sql, parameters).SingleOrDefault();
            return project;
        }
    }
}