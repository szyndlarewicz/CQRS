﻿using System.Collections.Generic;
using Framework.Cqrs.Events;
using Framework.Domain;

namespace $ext_safeprojectname$.Tests.Integration.Dependencies
{
    public sealed class NullEventPublisher : IEventPublisher
    {
        public void Send(IEvent @event)
        {
        }

        public void Send(Queue<IEvent> events)
        {
        }

        public void Send(IEnumerable<IEvent> events)
        {
        }
    }
}