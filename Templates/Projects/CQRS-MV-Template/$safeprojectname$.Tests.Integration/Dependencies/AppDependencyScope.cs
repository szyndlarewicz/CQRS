﻿using Framework.Core.IoC;
using Framework.Cqrs.Events;
using Framework.IoC.Ninject;
using Framework.Testing.DI;

namespace $ext_safeprojectname$.Tests.Integration.Dependencies
{
    public sealed class AppDependencyScope : NinjectDependencyScope<AppDependencyScope>
    {
        public AppDependencyScope ConfigureCore()
        {
            RegisterSingleton<IDependencyResolver, NinjectDependencyResolver>();
            RegisterSingleton<IDependencyRegister, NinjectDependencyRegister>();
            RegisterTransient<IEventPublisher, NullEventPublisher>();
            return this;
        }
    }
}