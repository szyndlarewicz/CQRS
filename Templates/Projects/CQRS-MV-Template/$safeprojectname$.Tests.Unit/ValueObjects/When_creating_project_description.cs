﻿using System;
using $ext_safeprojectname$.Domain.ValueObjects;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Unit.ValueObjects
{
    [TestFixture]
    public class When_creating_project_description
    {
        [TestCase]
        public void It_should_create_project_description()
        {
            Assert.IsNotNull(new ProjectDescription("description"));
        }

        [TestCase]
        public void It_should_throw_exception_when_given_null_project_description()
        {
            Assert.Catch<ArgumentNullException>(() => new ProjectDescription(null));
        }

        [TestCase]
        public void It_should_throw_exception_when_given_empty_project_description()
        {
            Assert.Catch<ArgumentException>(() => new ProjectDescription(string.Empty));
        }
    }
}