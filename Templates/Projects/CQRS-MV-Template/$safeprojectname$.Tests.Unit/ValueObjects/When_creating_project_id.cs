﻿using System;
using $ext_safeprojectname$.Domain.ValueObjects;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Unit.ValueObjects
{
    [TestFixture]
    public class When_creating_project_id
    {
        [TestCase]
        public void It_should_create_project_id()
        {
            Assert.IsNotNull(new ProjectId(Guid.NewGuid()));
        }
    }
}
