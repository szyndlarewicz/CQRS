﻿using System;
using $ext_safeprojectname$.Domain.ValueObjects;
using NUnit.Framework;
using NUnit.Framework.Internal;

namespace $ext_safeprojectname$.Tests.Unit.ValueObjects
{
    [TestFixture]
    public class When_creating_project_name
    {
        private readonly Randomizer _randomizer = new Randomizer((int)DateTime.Now.Ticks);

        [TestCase]
        public void It_should_create_project_name()
        {
            Assert.IsNotNull(new ProjectName("name"));
        }

        [TestCase]
        public void It_should_throw_exception_when_given_null_project_name()
        {
            Assert.Catch<ArgumentNullException>(() => new ProjectName(null));
        }

        [TestCase]
        public void It_should_throw_exception_when_given_empty_project_name()
        {
            Assert.Catch<ArgumentException>(() => new ProjectName(string.Empty));
        }

        [TestCase]
        public void It_should_throw_exception_when_project_name_contains_more_than_100_chars()
        {
            Assert.Catch<ArgumentException>(() => new ProjectName(_randomizer.GetString(101)));
        }
    }
}