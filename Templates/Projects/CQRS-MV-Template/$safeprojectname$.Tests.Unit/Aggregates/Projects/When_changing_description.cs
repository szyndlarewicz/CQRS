﻿using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.ValueObjects;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Unit.Aggregates.Projects
{
    [TestFixture]
    public class When_changing_description
    {
        private ProjectEntity _project;

        [SetUp]
        public void SetUp()
        {
            var projectId = new ProjectId();
            var name = new ProjectName("Name");
            var description = new ProjectDescription("Description1");

            _project = new ProjectEntity(projectId, name, description);
        }

        [TestCase]
        public void It_should_change_project_description()
        {
            var description = new ProjectDescription("Description2");
            _project.ChangeDescription(description);

            Assert.AreEqual(description, _project.Description);
        }

        [TestCase]
        public void It_should_clear_project_description()
        {
            _project.ChangeDescription(null);

            Assert.IsNull(_project.Description);
        }
    }
}