﻿using System;
using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.ValueObjects;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Unit.Aggregates.Projects
{
    [TestFixture]
    public class When_changing_name
    {
        private ProjectEntity _project;

        [SetUp]
        public void SetUp()
        {
            var projectId = new ProjectId();
            var name = new ProjectName("Name1");
            var description = new ProjectDescription("Description1");

            _project = new ProjectEntity(projectId, name, description);
        }

        [TestCase]
        public void It_should_throw_exception_when_given_null_project_name()
        {
            Assert.Catch<ArgumentNullException>(() => _project.ChangeName(null));
        }

        [TestCase]
        public void It_should_change_project_name()
        {
            var name = new ProjectName("Name2");
            _project.ChangeName(name);
            Assert.AreEqual(name, _project.Name);
        }
    }
}