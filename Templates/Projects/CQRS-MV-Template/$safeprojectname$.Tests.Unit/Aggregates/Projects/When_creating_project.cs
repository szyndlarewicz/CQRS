﻿using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.ValueObjects;
using NUnit.Framework;

namespace $ext_safeprojectname$.Tests.Unit.Aggregates.Projects
{
    [TestFixture]
    public class When_creating_project
    {
        private ProjectId _projectId;
        private ProjectName _name;
        private ProjectDescription _description;

        private ProjectEntity _project;

        [SetUp]
        public void SetUp()
        {
            _projectId = new ProjectId();
            _name = new ProjectName("Name");
            _description = new ProjectDescription("Description");

            _project = new ProjectEntity(_projectId, _name, _description);
        }

        [TestCase]
        public void It_should_set_project_id()
        {
            Assert.AreEqual(_projectId, _project.Id);
        }

        [TestCase]
        public void It_should_set_project_name()
        {
            Assert.AreEqual(_name, _project.Name);
        }

        [TestCase]
        public void It_should_set_project_description()
        {
            Assert.AreEqual(_description, _project.Description);
        }
    }
}