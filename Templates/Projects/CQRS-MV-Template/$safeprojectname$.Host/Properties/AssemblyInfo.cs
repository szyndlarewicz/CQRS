﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("$ext_safeprojectname$.Host")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("$ext_safeprojectname$.Host")]
[assembly: AssemblyCopyright("Copyright © $username$ $year$")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("$ext_guid4$")]
[assembly: AssemblyVersion("1.0.0.0")]