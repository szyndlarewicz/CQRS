﻿using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using $ext_safeprojectname$.Host.Infrastructure.Config;

namespace $ext_safeprojectname$.Host
{
    /// <summary>
    /// The website startup class
    /// </summary>
    public class WebApiApplication : HttpApplication
    {
        /// <summary>
        /// Application Start
        /// </summary>
        protected void Application_Start()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}