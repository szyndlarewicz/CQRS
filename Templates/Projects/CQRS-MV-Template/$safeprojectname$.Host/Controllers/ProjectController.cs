﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using $ext_safeprojectname$.Contracts.Commands;
using $ext_safeprojectname$.Contracts.Queries;
using $ext_safeprojectname$.Contracts.Views;
using Framework.Cqrs.Commands;
using Framework.Cqrs.Queries;
using Framework.Search.Contracts.Views;
using Framework.WebApi.Cqrs;
using Swashbuckle.Swagger.Annotations;

namespace $ext_safeprojectname$.Host.Controllers
{   
    /// <summary>
    /// Operations about projects
    /// </summary>
    [RoutePrefix("api/projects")]
    public class ProjectController : SyncCqrsApiController
    {        
        /// <summary>
        ///  Project controller constructor
        /// </summary>
        public ProjectController(ICommandPublisher commandPublisher, IQueryDispatcher queryDispatcher)
            : base(commandPublisher, queryDispatcher)
        {
        }

        /// <summary>
        /// Find project by search criteria
        /// </summary>
        /// <param name="searchText">The search text to find</param>
        /// <param name="orderDirection">The sort direction</param>
        /// <param name="orderExpression">The sort expression</param>
        /// <param name="pageSize">The page size</param>
        /// <param name="pageIndex">The page idex</param>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [HttpGet]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(PageCollection<ProjectListItemView>))]
        public HttpResponseMessage FindProjects(string searchText = null, string orderExpression = "Name", string orderDirection = "Ascending", int pageSize = 25, int pageIndex = 0)
        {
            var query = new FindProjectsQuery(searchText, orderDirection, orderExpression, pageSize, pageIndex);
            return View(QueryDispatcher.Execute<PageCollection<ProjectListItemView>>(query));
        }

        /// <summary>
        /// Add a new project
        /// </summary>
        /// <param name="projectName">The project name</param>
        /// <param name="projectDescription">The project description</param>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [HttpPost]
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Guid))]
        public HttpResponseMessage Create(string projectName = null, string projectDescription = null)
        {
            var command = new AddProjectCommand(projectName, projectDescription);
            return Info(CommandPublisher.Send(command));
        }

        /// <summary>
        /// Get project by id
        /// </summary>
        /// <param name="projectId">The proejct Id</param>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [HttpGet]
        [Route("{projectId}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(ProjectDetailsView))]
        public HttpResponseMessage GetProject(Guid projectId)
        {
            var query = new GetProjectQuery(projectId);
            return View(QueryDispatcher.Execute<ProjectDetailsView>(query));
        }

        /// <summary>
        /// Change poject name
        /// </summary>
        /// <param name="projectId">The proejct Id</param>
        /// <param name="projectName">The project name</param>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [HttpPost]
        [Route("{projectId}/name")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Guid))]
        public HttpResponseMessage ChangeName(Guid projectId, string projectName)
        {
            var command = new ChangeProjectNameCommand(projectId, projectName);
            return Info(CommandPublisher.Send(command));
        }

        /// <summary>
        /// Change project description
        /// </summary>
        /// <param name="projectId">The proejct Id</param>
        /// <param name="projectDescription">The project description</param>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [HttpPost]
        [Route("{projectId}/description")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Guid))]
        public HttpResponseMessage ChangeDescription(Guid projectId, string projectDescription)
        {
            var command = new ChangeProjectDescriptionCommand(projectId, projectDescription);
            return Info(CommandPublisher.Send(command));
        }

        /// <summary>
        /// Remove project
        /// </summary>
        /// <param name="projectId">The proejct Id</param>
        /// <response code="400">Bad request</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="403">Forbidden</response>
        /// <response code="500">Internal server error</response>
        [HttpDelete]
        [Route("{projectId}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Guid))]
        public HttpResponseMessage Remove(Guid projectId)
        {
            var command = new RemoveProjectCommand(projectId);
            return Info(CommandPublisher.Send(command));
        }
    }
}