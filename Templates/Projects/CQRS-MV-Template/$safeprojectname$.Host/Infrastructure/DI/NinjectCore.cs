using System.Web.Http;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject.Web.Common;

namespace $ext_safeprojectname$.Host.Infrastructure.DI
{
    internal static class NinjectCore
    {
        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));

            Bootstrapper.Initialize(KernelFactory.Create);
            GlobalConfiguration.Configuration.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(Bootstrapper.Kernel);
        }

        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }
    }
}