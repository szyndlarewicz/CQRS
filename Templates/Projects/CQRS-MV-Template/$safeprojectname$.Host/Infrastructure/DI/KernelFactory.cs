﻿using System;
using System.Web;
using $ext_safeprojectname$.Host.Infrastructure.AutoWiring;
using $ext_safeprojectname$.Host.Infrastructure.Loggining;
using Framework.Core.Logging;
using Framework.Cqrs;
using Framework.Cqrs.Commands;
using Framework.Cqrs.Events;
using Framework.Cqrs.Queries;
using Framework.IoC;
using Framework.IoC.AutoWiring;
using Framework.IoC.Ninject;
using Framework.Search.Dapper;
using Framework.WebApi.Cqrs;
using Ninject;
using Ninject.Web.Common;

namespace $ext_safeprojectname$.Host.Infrastructure.DI
{
    internal static class KernelFactory
    {
        public static IKernel Create()
        {
            IKernel kernel = new StandardKernel();

            try
            {
                Configure(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void Configure(IKernel kernel)
        {
            ConfigureCore(kernel);
            ConfigureCqrs(kernel);
            ConfigureAutoWiring(kernel);

            kernel.Bind<ReadDbConnection>().ToSelf().InRequestScope();
            kernel.Get<AutoBindProcessor>().Execute();
        }
 
        private static void ConfigureCore(IKernel kernel)
        {
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            kernel.Bind<IAmbientScopeFactory>().To<NinjectAmbientScopeFactory>().InSingletonScope();
            kernel.Bind<IDependencyResolver>().To<NinjectDependencyResolver>().InSingletonScope();
            kernel.Bind<IDependencyRegister>().To<NinjectDependencyRegister>().InSingletonScope();

            kernel.Bind<ILogger>().To<NLogLoggerProxy>().InTransientScope();
        }

        private static void ConfigureCqrs(IKernel kernel)
        {
            kernel.Bind<IRequestScopeExceptionAggregator>().To<RequestScopeExceptionAggregator>().InRequestScope();
            kernel.Bind<IExceptionHandler>().To<CqrsExceptionHandler>().InTransientScope();

            kernel.Bind<ICommandHandlerRegistrar>().To<CommandHandlerRegistrar>().InSingletonScope();
            kernel.Bind<ICommandQueue>().To<InMemmoryCommandQueue>().InRequestScope();
            kernel.Bind<ICommandPublisher>().To<CommandPublisher>().InTransientScope();
            kernel.Bind<ICommandDispatcher>().To<CommandDispatcher>().InTransientScope();
            kernel.Bind<ICommandProcessor>().To<CommandProcessor>().InTransientScope();
            kernel.Bind<ICommandValidatorProvider>().To<CommandValidatorProvider>().InTransientScope();

            kernel.Bind<IQueryHandlerRegistrar>().To<QueryHandlerRegistrar>().InSingletonScope();
            kernel.Bind<IQueryDispatcher>().To<QueryDispatcher>().InTransientScope();
            
            kernel.Bind<IEventHandlerRegistrar>().To<EventHandlerRegistrar>().InSingletonScope();
            kernel.Bind<IEventQueue>().To<InMemmoryEventQueue>().InRequestScope();
            kernel.Bind<IEventPublisher>().To<EventPublisher>().InTransientScope();
            kernel.Bind<IEventDispatcher>().To<EventDispatcher>().InTransientScope();
            kernel.Bind<IEventProcessor>().To<EventProcessor>().InTransientScope();
        }

        private static void ConfigureAutoWiring(IKernel kernel)
        {
            kernel.Bind<IAssemblyProvider>().To<ContextAssemblyProvider>().InTransientScope();
            kernel.Bind<IBindingStrategyMapping>().To<DefaultBindingStrategyMaping>().InTransientScope();
            kernel.Bind<IAutoBindableTypeFinder>().To<AutoBindableTypeFinder>().InTransientScope();
            kernel.Bind<IBindingStrategyFactory>().To<BindingStrategyFactory>().InTransientScope();
            kernel.Bind<AutoBindProcessor>().ToSelf().InTransientScope();
        }
    }
}