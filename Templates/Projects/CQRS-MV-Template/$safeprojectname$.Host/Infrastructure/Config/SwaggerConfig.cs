using System;
using System.Reflection;
using System.Web.Http;
using Swashbuckle.Application;

namespace $ext_safeprojectname$.Host.Infrastructure.Config
{
    internal static class SwaggerConfig
    {
        public static void Register()
        {
            GlobalConfiguration.Configuration
                .EnableSwagger(configure =>
                {
                    configure.SingleApiVersion("v1", "$ext_safeprojectname$");
                    configure.IncludeXmlComments(GetXmlCommentsPath());
                })
                .EnableSwaggerUi(configure =>
                {
                    Assembly assembly = Assembly.GetExecutingAssembly();
                    configure.InjectStylesheet(assembly, "$ext_safeprojectname$.Host.Infrastructure.Config.Swagger.Themes.theme-flattop.css");
                });
        }

        private static string GetXmlCommentsPath()
        {
            return AppDomain.CurrentDomain.BaseDirectory + @"\Api.xml";
        }
    }
}