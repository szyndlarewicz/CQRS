﻿using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using Framework.WebApi.Cors;
using Framework.WebApi.Logging;
using Framework.WebApi.UnknownRoutes;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace $ext_safeprojectname$.Host.Infrastructure.Config
{
    internal static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional });

            config.MapUnknownRoutes();

            ICorsSectionProvider corsSectionProvider = new WebConfigCorsSectionProvider();
            CorsSection corsSection = corsSectionProvider.GetSettings();
            var cors = new EnableCorsAttribute(corsSection.Origin, corsSection.Headers, corsSection.Methods);
            config.EnableCors(cors);

            JsonSerializerSettings serializerSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            serializerSettings.Formatting = Formatting.Indented;
            serializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            config.Services.Add(typeof(IExceptionLogger), new WebApiExceptionLogger());           
        }
    }
}