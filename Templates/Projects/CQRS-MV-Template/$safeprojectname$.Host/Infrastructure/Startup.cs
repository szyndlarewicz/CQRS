﻿using $ext_safeprojectname$.Host.Infrastructure;
using $ext_safeprojectname$.Host.Infrastructure.Config;
using $ext_safeprojectname$.Host.Infrastructure.DI;
using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(Startup), "Start")]
[assembly: ApplicationShutdownMethod(typeof(Startup), "Stop")]
namespace $ext_safeprojectname$.Host.Infrastructure
{
    internal static class Startup
    {
        public static void Start()
        {
            NinjectCore.Start();
            SwaggerConfig.Register();
        }

        public static void Stop()
        {
            NinjectCore.Stop();
        }
    }
}