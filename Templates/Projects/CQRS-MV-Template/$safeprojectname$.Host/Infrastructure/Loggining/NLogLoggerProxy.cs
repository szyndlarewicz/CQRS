﻿using System;
using NLog;
using ILogger = Framework.Core.Logging.ILogger;
using LogLevel = Framework.Core.Logging.LogLevel;

namespace $ext_safeprojectname$.Host.Infrastructure.Loggining
{
    internal sealed class NLogLoggerProxy : ILogger
    {
        private readonly Logger _logger;

        public NLogLoggerProxy()
            : this(LogManager.GetLogger("Logger"))
        {
        }

        public NLogLoggerProxy(Logger logger)
        {
            _logger = logger;
        }

        public void Log(LogLevel logLevel, string message)
        {
            _logger.Log(NLog.LogLevel.FromString((string)logLevel), message);
        }

        public void Log(LogLevel logLevel, Exception exception)
        {
            _logger.Log(NLog.LogLevel.FromString((string)logLevel), exception);
        }

        public void Log(LogLevel logLevel, Exception exception,  string message)
        {
            _logger.Log(NLog.LogLevel.FromString((string)logLevel), exception, message);
        }
    }
}