﻿using Framework.Core.Logging;

namespace $ext_safeprojectname$.Host.Infrastructure.Loggining
{
    internal sealed class NLogLoggerProvider : LoggerProvider
    {
        public override ILogger GetLogger<T>()
        {
            NLog.Logger logger = NLog.LogManager.GetLogger(typeof(T).Name);
            return new NLogLoggerProxy(logger);
        }

        public override ILogger GetLogger(string name)
        {
            NLog.Logger logger = NLog.LogManager.GetLogger(name);
            return new NLogLoggerProxy(logger);
        }
    }
}