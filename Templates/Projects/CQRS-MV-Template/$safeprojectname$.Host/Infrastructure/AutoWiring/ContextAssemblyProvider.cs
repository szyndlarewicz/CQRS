﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Framework.IoC.AutoWiring;

namespace $ext_safeprojectname$.Host.Infrastructure.AutoWiring
{
    internal sealed class ContextAssemblyProvider : IAssemblyProvider
    {
        private const string RootAssemblyPrefix = "$ext_safeprojectname$";

        public IEnumerable<Assembly> GetProjectAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies().Where(c => c.FullName.StartsWith(RootAssemblyPrefix));
        }
    }
}