﻿namespace $ext_safeprojectname$.Migration.Infrastructure
{
    public interface IDatabaseMigrator
    {
        bool Upgrade();
    }
}