﻿using DbUp.Engine.Output;
using NLog;

namespace $ext_safeprojectname$.Migration.Infrastructure.Loggining
{
    public class NLogUpgradeLog : IUpgradeLog
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public void WriteInformation(string format, params object[] args)
        {
            Logger.Info(format, args);
        }

        public void WriteError(string format, params object[] args)
        {
            Logger.Error(format, args);
        }

        public void WriteWarning(string format, params object[] args)
        {
            Logger.Warn(format, args);
        }
    }
}