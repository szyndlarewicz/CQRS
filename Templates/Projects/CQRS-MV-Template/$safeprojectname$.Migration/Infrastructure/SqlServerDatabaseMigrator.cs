﻿using System.Reflection;
using $ext_safeprojectname$.Migration.Infrastructure.Loggining;
using $ext_safeprojectname$.Migration.Infrastructure.Settings;
using DbUp;
using DbUp.Engine;
using Framework.Core.Validation;

namespace $ext_safeprojectname$.Migration.Infrastructure
{
    public class SqlServerDatabaseMigrator : IDatabaseMigrator
    {
        private UpgradeEngine _upgradeEngine;

        public SqlServerDatabaseMigrator(ISettingsProvider settingsProvider)
        {
            Guard.NotNull(() => settingsProvider, settingsProvider);

            Initialize(settingsProvider);
        }

        public bool Upgrade()
        {
            DatabaseUpgradeResult result = _upgradeEngine.PerformUpgrade();
            return result.Successful;
        }

        private void Initialize(ISettingsProvider settingsProvider)
        {
            SettingsData settings = settingsProvider.GetSettings();

            _upgradeEngine = DeployChanges.To
                .SqlDatabase(settings.ConnectionString)
                .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                .LogToConsole()
                .LogTo(new NLogUpgradeLog())
                .Build();
        }
    }
}