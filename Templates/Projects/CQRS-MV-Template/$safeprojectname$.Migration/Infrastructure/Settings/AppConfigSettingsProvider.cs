﻿using System.Configuration;

namespace $ext_safeprojectname$.Migration.Infrastructure.Settings
{
    public class AppConfigSettingsProvider : ISettingsProvider
    {
        public SettingsData GetSettings()
        {
            return new SettingsData
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["database"].ConnectionString
            };
        }
    }
}