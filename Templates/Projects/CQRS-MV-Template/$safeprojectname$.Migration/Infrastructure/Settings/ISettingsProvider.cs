﻿namespace $ext_safeprojectname$.Migration.Infrastructure.Settings
{
    public interface ISettingsProvider
    {
        SettingsData GetSettings();
    }
}