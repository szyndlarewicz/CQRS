﻿namespace $ext_safeprojectname$.Migration.Infrastructure.Settings
{
    public sealed class SettingsData
    {
        public string ConnectionString { get; set; }
    }
}