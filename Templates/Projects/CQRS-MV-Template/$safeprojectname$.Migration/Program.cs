﻿using $ext_safeprojectname$.Migration.Infrastructure;
using $ext_safeprojectname$.Migration.Infrastructure.Settings;

namespace $ext_safeprojectname$.Migration
{
    public sealed class Program
    {
        public static int Main()
        {
            ISettingsProvider settingsProvider = new AppConfigSettingsProvider();
            IDatabaseMigrator databaseMigrator = new SqlServerDatabaseMigrator(settingsProvider);
            bool result = databaseMigrator.Upgrade();
            return result ? 0 : -1;
        }
    }
}