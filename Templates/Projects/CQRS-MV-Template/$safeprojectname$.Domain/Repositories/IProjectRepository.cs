﻿using $ext_safeprojectname$.Domain.ValueObjects;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Repositories
{
    public interface IProjectRepository : IRepository
    {
        ProjectEntity Get(ProjectId projectId);

        void Add(ProjectEntity project);

        void Change(ProjectEntity project);

        void Remove(ProjectEntity project);
    }
}