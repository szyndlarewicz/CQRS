﻿using System;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Factories
{
    public interface IProjectFactory : IFactory
    {
        ProjectEntity Create(Guid projectId, string name, string description);
    }
}