﻿using $ext_safeprojectname$.Domain.Events;
using $ext_safeprojectname$.Domain.States;
using $ext_safeprojectname$.Domain.ValueObjects;
using Framework.Core.Validation;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain
{
    public sealed class ProjectEntity : AggregateRoot<ProjectPoco>
    {
        public ProjectEntity(ProjectPoco state) 
            : base(state)
        {
        }

        public ProjectEntity(ProjectId id, ProjectName name, ProjectDescription description)
            : base(new ProjectPoco())
        {
            State.Id = id;
            State.Name = name;
            State.Description = description;

            var @event = new ProjectCreatedEvent(State.Id, State.Name, State.Description);
            Enqueue(@event);
        }

        public ProjectId Id => (ProjectId) State.Id;

        public ProjectName Name => (ProjectName) State.Name;

        public ProjectDescription Description => (ProjectDescription) State.Description;

        public void ChangeName(ProjectName name)
        {
            Guard.NotNull(() => name, name);

            if (State.Name == name)
                return;

            State.Name = name;

            Enqueue(new ProjectNameChangedEvent(State.Id, State.Name));
        }

        public void ChangeDescription(ProjectDescription description)
        {
            if (State.Description == description)
                return;

            State.Description = description;

            Enqueue(new ProjectDescriptionChangedEvent(State.Id, State.Description));
        }

        public void Remove()
        {
            Enqueue(new ProjectRemovedEvent(State.Id));
        }
    }
}