﻿using System;
using Framework.Core.Validation;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.ValueObjects
{
    public sealed class ProjectName : IValueObject, IEquatable<ProjectName>
    {
        private readonly string _projectName;

        public ProjectName(string projectName)
        {
            Guard.NotNullOrEmpty(() => projectName, projectName);
            Guard.IsValid(() => projectName, projectName, v => v.Length <= 100, $"{projectName} cannot be greater than 100");

            _projectName = projectName;
        }

        public static explicit operator ProjectName(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new ProjectName(instance);
        }

        public static implicit operator string(ProjectName instance)
        {
            return instance?._projectName;
        }

        public static bool operator ==(ProjectName x, ProjectName y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(ProjectName x, ProjectName y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as ProjectName);
        }

        public bool Equals(ProjectName value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_projectName, value._projectName);
        }

        public override int GetHashCode()
        {
            return _projectName.GetHashCode();
        }

        public override string ToString()
        {
            return _projectName;
        }
    }
}
