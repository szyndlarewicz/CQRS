﻿using System;
using Framework.Core.Validation;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.ValueObjects
{
    public sealed class ProjectDescription : IValueObject, IEquatable<ProjectDescription>
    {
        private readonly string _projectDescription;

        public ProjectDescription(string projectDescription)
        {
            Guard.NotNullOrEmpty(() => projectDescription, projectDescription);

            _projectDescription = projectDescription;
        }

        public static explicit operator ProjectDescription(string instance)
        {
            return string.IsNullOrEmpty(instance) ? null : new ProjectDescription(instance);
        }

        public static implicit operator string(ProjectDescription instance)
        {
            return instance?._projectDescription;
        }

        public static bool operator ==(ProjectDescription x, ProjectDescription y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(ProjectDescription x, ProjectDescription y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as ProjectDescription);
        }

        public bool Equals(ProjectDescription value)
        {
            return !ReferenceEquals(null, value) && string.Equals(_projectDescription, value._projectDescription);
        }

        public override int GetHashCode()
        {
            return _projectDescription.GetHashCode();
        }

        public override string ToString()
        {
            return _projectDescription;
        }
    }
}
