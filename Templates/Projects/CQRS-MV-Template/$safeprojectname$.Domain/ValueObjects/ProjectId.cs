﻿using System;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.ValueObjects
{
    public sealed class ProjectId : EntityIdentity
    {
        public ProjectId()
        {
        }

        public ProjectId(Guid projectId)
            : base(projectId)
        {
        }

        public static explicit operator ProjectId(Guid instance)
        {
            return instance == Guid.Empty ? null : new ProjectId(instance);
        }

        public static explicit operator ProjectId(Guid? instance)
        {
            return (instance == null || instance == Guid.Empty) ? null : new ProjectId(instance.Value);
        }

        public static implicit operator Guid?(ProjectId instance)
        {
            return instance?.Identity;
        }

        public static implicit operator Guid(ProjectId instance)
        {
            return instance?.Identity ?? Guid.Empty;
        }

        public static bool operator ==(ProjectId x, ProjectId y)
        {
            return Equals(null, x) || x.Equals(y);
        }

        public static bool operator !=(ProjectId x, ProjectId y)
        {
            return !(x == y);
        }

        public override bool Equals(object value)
        {
            return Equals(value as ProjectId);
        }

        public bool Equals(ProjectId value)
        {
            return !ReferenceEquals(null, value) && string.Equals(Identity, value.Identity);
        }

        public override int GetHashCode()
        {
            return Identity.GetHashCode();
        }

        public override string ToString()
        {
            return Identity.ToString();
        }
    }
}