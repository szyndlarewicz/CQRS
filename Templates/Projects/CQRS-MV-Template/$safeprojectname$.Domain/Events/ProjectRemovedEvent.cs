﻿using System;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Events
{
    public sealed class ProjectRemovedEvent : IEvent
    {
        public ProjectRemovedEvent(Guid projectId)
        {
            ProjectId = projectId;
        }

        public Guid ProjectId { get; }
    }
}