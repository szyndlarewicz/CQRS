﻿using System;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Events
{
    public sealed class ProjectCreatedEvent : IEvent
    {
        public ProjectCreatedEvent(Guid projectId, string projectName, string projectDescription)
        {
            ProjectId = projectId;
            ProjectName = projectName;
            ProjectDescription = projectDescription;
        }

        public Guid ProjectId { get; }

        public string ProjectName { get; }

        public string ProjectDescription { get; }
    }
}