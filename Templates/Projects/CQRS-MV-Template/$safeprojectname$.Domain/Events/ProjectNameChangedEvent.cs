﻿using System;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Events
{
    public sealed class ProjectNameChangedEvent : IEvent
    {
        public ProjectNameChangedEvent(Guid projectId, string projectName)
        {
            ProjectId = projectId;
            ProjectName = projectName;
        }

        public Guid ProjectId { get; }

        public string ProjectName { get; }
    }
}