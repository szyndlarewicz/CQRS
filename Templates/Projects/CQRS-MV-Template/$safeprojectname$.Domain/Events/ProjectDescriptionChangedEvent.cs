﻿using System;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Events
{
    public sealed class ProjectDescriptionChangedEvent : IEvent
    {
        public ProjectDescriptionChangedEvent(Guid projectId, string projectDescription)
        {
            ProjectId = projectId;
            ProjectDescription = projectDescription;
        }

        public Guid ProjectId { get; }

        public string ProjectDescription { get; }
    }
}