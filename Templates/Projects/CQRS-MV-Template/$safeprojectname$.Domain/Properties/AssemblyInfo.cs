﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("$ext_safeprojectname$.Domain")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("$ext_safeprojectname$.Domain")]
[assembly: AssemblyCopyright("Copyright © $username$ $year$")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("$ext_guid2$")]
[assembly: AssemblyVersion("1.0.0.0")]