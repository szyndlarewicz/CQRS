﻿using System;
using Framework.Core.Persistence;

namespace $ext_safeprojectname$.Domain.States
{
    public class ProjectPoco : Poco<Guid>
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
