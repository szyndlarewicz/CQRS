﻿using System;
using System.Runtime.Serialization;
using Framework.Domain;

namespace $ext_safeprojectname$.Domain.Exceptions
{
    [Serializable]
    public sealed class ProjectNotFoundException : DomainException
    {
        public ProjectNotFoundException(Guid projectId)
            : base($"Cannot find project with given id '{projectId}'")
        {
            ProjectId = projectId;
        }

        public Guid ProjectId { get; }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ProjectId", ProjectId);
            base.GetObjectData(info, context);
        }
    }
}