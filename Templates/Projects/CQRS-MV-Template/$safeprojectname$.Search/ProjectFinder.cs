﻿using System;
using System.Linq;
using System.Text;
using $ext_safeprojectname$.Contracts.Views;
using Framework.Core.IoC;
using Framework.Search.Contracts.Types;
using Framework.Search.Contracts.Views;
using Framework.Search.Dapper;
using Framework.Search.Infrastructure;

namespace $ext_safeprojectname$.Search
{
    [AutoBind]
    public sealed class ProjectFinder : DapperFinder, IProjectFinder
    {
        public ProjectFinder(ReadDbConnection connection) 
            : base(connection)
        {
            Pagination = new SqlServer2012QueryPagination();
        }

        public ProjectDetailsView GetProject(Guid projectId)
        {
            var sql = new SqlQuery("SELECT Id, Name, Description FROM qbo.Projects WHERE Id = @ProjectId");
            var parameters = new { ProjectId = projectId };

            ProjectDetailsView result = Query<ProjectDetailsView>(sql, parameters).SingleOrDefault();
            return result;
        }

        public PageCollection<ProjectListItemView> FindBy(string searchText, SortOrder sortOrder, PageIndex pageIndex, PageSize pageSize)
        {
            StringBuilder query = new StringBuilder();
            query.Append("SELECT Id, Name FROM qbo.Projects ");
            if (!string.IsNullOrEmpty(searchText))
                query.Append("WHERE Name LIKE CONCAT('%',@SearchText,'%')");

            var sql = new SqlQuery(query.ToString());
            var parameters = new { SearchText = searchText };

            PageCollection<ProjectListItemView> results = Query<ProjectListItemView>(sql, pageIndex, pageSize, sortOrder, parameters);
            return results;
        }
    }
}