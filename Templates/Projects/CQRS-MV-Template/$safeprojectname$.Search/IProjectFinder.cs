﻿using System;
using $ext_safeprojectname$.Contracts.Views;
using Framework.Search.Contracts.Types;
using Framework.Search.Contracts.Views;
using Framework.Search.Infrastructure;

namespace $ext_safeprojectname$.Search
{
    public interface IProjectFinder : IFinder
    {
        ProjectDetailsView GetProject(Guid projectId);

        PageCollection<ProjectListItemView> FindBy(string searchText, SortOrder sortOrder, PageIndex pageIndex, PageSize pageSize);
    }
}