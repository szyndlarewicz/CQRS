﻿using System.Linq;
using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.Exceptions;
using $ext_safeprojectname$.Domain.Repositories;
using $ext_safeprojectname$.Domain.States;
using $ext_safeprojectname$.Domain.ValueObjects;
using $ext_safeprojectname$.Infrastructure.Persistence;
using Framework.Core.IoC;
using Framework.Cqrs.Events;

namespace $ext_safeprojectname$.Infrastructure.Repositories
{
    [AutoBind]
    public sealed class ProjectRepository : IProjectRepository
    {
        private readonly AppDbContext _context;
        private readonly IEventPublisher _eventPublisher;

        public ProjectRepository(AppDbContext context, IEventPublisher eventPublisher)
        {
            _context = context;
            _eventPublisher = eventPublisher;
        }

        public ProjectEntity Get(ProjectId projectId)
        {
            ProjectPoco poco = _context.Projects.SingleOrDefault(c => c.Id == projectId);
            if (poco == null)
                throw new ProjectNotFoundException(projectId);

            return new ProjectEntity(poco);
        }

        public void Add(ProjectEntity project)
        {
            _context.Projects.Add(project);

            _context.SaveChanges();
            _eventPublisher.Send(project.Events);
        }

        public void Change(ProjectEntity project)
        {
            _context.SaveChanges();
            _eventPublisher.Send(project.Events);
        }

        public void Remove(ProjectEntity project)
        {
            _context.Projects.Remove(project);
            _context.SaveChanges();
            _eventPublisher.Send(project.Events);
        }
    }
}