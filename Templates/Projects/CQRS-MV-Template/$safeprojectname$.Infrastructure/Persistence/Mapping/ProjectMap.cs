﻿using System.Data.Entity.ModelConfiguration;
using $ext_safeprojectname$.Domain.States;

namespace $ext_safeprojectname$.Infrastructure.Persistence.Mapping
{
    public sealed class ProjectMap : EntityTypeConfiguration<ProjectPoco>
    {
        public ProjectMap()
        {
            ToTable("Projects", "dbo");
            HasKey(c => c.Id).Property(c => c.Id);

            Property(c => c.Name).IsRequired().HasMaxLength(100);
            Property(c => c.Description).HasMaxLength(null).IsOptional();
        }
    }
}