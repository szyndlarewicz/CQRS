﻿using System.Data.Entity;
using $ext_safeprojectname$.Domain.States;
using $ext_safeprojectname$.Infrastructure.Persistence.Mapping;
using Framework.Core.IoC;
using Framework.Infrastructure.EntityFramework;

namespace $ext_safeprojectname$.Infrastructure.Persistence
{
    [AutoBind]
    public sealed class AppDbContext : WriteDbContext
    {
        public IDbSet<ProjectPoco> Projects { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProjectMap());
        }
    }
}