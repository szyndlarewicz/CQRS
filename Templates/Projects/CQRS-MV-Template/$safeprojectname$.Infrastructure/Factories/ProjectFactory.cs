﻿using System;
using $ext_safeprojectname$.Domain;
using $ext_safeprojectname$.Domain.Factories;
using $ext_safeprojectname$.Domain.ValueObjects;
using Framework.Core.IoC;

namespace $ext_safeprojectname$.Infrastructure.Factories
{
    [AutoBind]
    public sealed class ProjectFactory : IProjectFactory
    {
        public ProjectEntity Create(Guid projectId, string name, string description)
        {
            return new ProjectEntity((ProjectId) projectId, (ProjectName) name, (ProjectDescription) description);
        }
    }
}